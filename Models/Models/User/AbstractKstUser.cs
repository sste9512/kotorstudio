using System;

namespace KSTModels.Models.User
{
    public abstract class AbstractKstUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public bool IsSignedIn { get; set; }
        private DBConfig DBconfig{ get; set; }
        public abstract bool SignIn(Guid id);
        public abstract bool SignOut(Guid id);
        
       
    }
}