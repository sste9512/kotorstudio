﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using KotorStudio.Utils;
using ResourceHandler = KSTModels.Utils.ResourceHandler;

namespace KotorStudio.Models.LegacyERF
{
    /*
     *
     *  This class represents the ERF files in the kotor directories
     *  These files hold the different textures and each subsequent ERF File holds a different resolution texture pack
     *  Is Serializable!!!!!!!!!!!!!!!!!!
     *
     */

    [Serializable]
    public class ErfKeyEntry
    {
        public readonly string _resTypeStr;
        public readonly int Index;
        public readonly int Length;
        public readonly int Offset;
        public readonly string ResourceName;
        public readonly short ResType;
        private int _resourceId;


        public ErfKeyEntry()
        {
        }


        public ErfKeyEntry(string resourceName, short resType, int resourceId, int offset, int length, int index)
        {
            ResourceName = resourceName;
            ResType = resType;
            _resTypeStr = ResourceHandler.GetRsrcTypeForId(resType);
            _resourceId = resourceId;
            Offset = offset;
            Length = length;
            Index = index;
        }


        public static void WriteToFile(ErfKeyEntry keyEntry, string destFile = "C:/Users/steve/Desktop/kotor_tpcs/")
        {
            using (var fileStream = new FileStream(destFile + keyEntry.ResourceName + "." + keyEntry._resTypeStr,
                FileMode.Create))
            {
                fileStream.Write(ByteFunctions.ObjectToByteArray(keyEntry), 0, keyEntry.Length);
            }
        }


        public async Task<Task> WriteToFileAsync(ErfKeyEntry keyEntry, string fileName)
        {
            await Task.Factory.StartNew(() =>
            {
                using (var fileStream = new FileStream(fileName + keyEntry.ResourceName + "." + keyEntry._resTypeStr,
                    FileMode.Create))
                {
                    fileStream.Write(ByteFunctions.ObjectToByteArray(keyEntry), 0, keyEntry.Length);
                }
            });
            return Task.CompletedTask;
        }

        public async Task<Task> WriteMultipleToFileAsync(IEnumerable<ErfKeyEntry> entries, string destFile)
        {
            await Task.Factory.StartNew(() =>
            {
                var erfKeyEntries = entries.ToList();
                foreach (var keyEntry in erfKeyEntries)
                {
                    using (var fileStream = new FileStream(destFile + keyEntry.ResourceName + "." + keyEntry._resTypeStr, FileMode.Create))
                    {
                        fileStream.Write(ByteFunctions.ObjectToByteArray(keyEntry), 0, keyEntry.Length);
                    }
                }
            });
            return Task.CompletedTask;
        }
    }


    /*
    BUILDER PATTERN/ FLUENT DESIGN
   */

    public class ErfKeyBuilder
    {
        public int BIndex;
        public int BLength;
        public int BOffset;
        public int BResourceId;
        public string BResourceName;
        public short BResType;
        public string BResTypeStr;

        public ErfKeyBuilder WithResName(string resourceName)
        {
            BResourceName = resourceName;
            return this;
        }

        public ErfKeyBuilder WithResType(short resType)
        {
            BResType = resType;
            return this;
        }

        public ErfKeyBuilder WithResId(int resId)
        {
            BResourceId = resId;
            return this;
        }

        public ErfKeyBuilder WithOffSet(int offSet)
        {
            BOffset = offSet;
            return this;
        }

        public ErfKeyBuilder WithLength(int length)
        {
            BLength = length;
            return this;
        }

        public static implicit operator ErfKeyEntry(ErfKeyBuilder eb)
        {
            return new ErfKeyEntry(
                eb.BResourceName,
                eb.BResType,
                eb.BResourceId,
                eb.BOffset,
                eb.BLength,
                eb.BIndex
            );
        }
    }
}