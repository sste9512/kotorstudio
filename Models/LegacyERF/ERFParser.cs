﻿using System;
using System.Collections;
using System.IO;
using System.Text;
using KSTModels.Utils;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace KotorStudio.Models.LegacyERF
{
    public class ErfParser
    {
        //  ERF HEADER DATA -> Look into making this a struct
        private readonly FileStream _fstream;
        private readonly byte[] _keyData;
        private readonly int _offKeyList;
        private readonly byte[] _resInfoData;
        public readonly int EntryCount;
        public readonly ArrayList KeyEntryList;
        private int _offsetResList;


        public ErfParser(string outputFilePath,
            string fileType,
            uint descriptionStrRef,
            ErfLocalizedString[] erfLocalizedStringList,
            string[] inputFileList)
        {
            var fileStream = new FileStream(outputFilePath, FileMode.Create, FileAccess.Write);
            var binaryWriter = new BinaryWriter(fileStream, Encoding.ASCII);
            var chars1 = new char[16];
            binaryWriter.Write((Strings.UCase(Strings.Trim(fileType)) + " V1.0").ToCharArray());
            var num1 = 0;
            if (erfLocalizedStringList != null && erfLocalizedStringList.Length > 0)
            {
                var num2 = !((StringType.StrCmp(Strings.LCase(fileType), "erf", false) == 0) | (StringType.StrCmp(Strings.LCase(fileType), "hak", false) == 0))
                    ? 0
                    : 1;
                var num3 = 0;
                var num4 = erfLocalizedStringList.Length - 1;
                var index = num3;
                while (index <= num4)
                {
                    num1 += 8 + erfLocalizedStringList[index].StringSize + num2;
                    ++index;
                }

                binaryWriter.Write(erfLocalizedStringList.Length);
            }
            else
            {
                binaryWriter.Write(0);
            }

            binaryWriter.Write(num1);
            binaryWriter.Write(inputFileList.Length);
            binaryWriter.Write(160);
            binaryWriter.Write(checked(num1 + 160));
            binaryWriter.Write(checked(num1 + inputFileList.Length * 24 + 160));
            binaryWriter.Write(checked(DateAndTime.Year(DateAndTime.Now) - 1900));
            binaryWriter.Write(checked((int) DateAndTime.DateDiff(DateInterval.DayOfYear,
                DateType.FromString("1/1/" + StringType.FromInteger(DateAndTime.Year(DateAndTime.Now))),
                DateAndTime.Now, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)));
            binaryWriter.Write(descriptionStrRef);
            var num5 = 1;
            do
            {
                binaryWriter.Write(0);
                ++num5;
            } while (num5 <= 29);

            fileStream.Seek(160L, SeekOrigin.Begin);
            if (erfLocalizedStringList != null && erfLocalizedStringList.Length > 0)
            {
                var num2 = !((StringType.StrCmp(Strings.LCase(fileType), "erf", false) == 0) |
                             (StringType.StrCmp(Strings.LCase(fileType), "hak", false) == 0))
                    ? 0
                    : 1;
                var num3 = 0;
                var num4 = erfLocalizedStringList.Length - 1;
                var index1 = num3;
                while (index1 <= num4)
                {
                    binaryWriter.Write(erfLocalizedStringList[index1].LanguageId);
                    binaryWriter.Write(erfLocalizedStringList[index1].StringSize);
                    var chars2 = new char[checked(erfLocalizedStringList[index1].StringSize + num2 - 1 + 1)];
                    Array.Clear(chars2, 0, chars2.Length);
                    var num6 = 0;
                    var num7 = checked(erfLocalizedStringList[index1].StringSize - 1);
                    var index2 = num6;
                    while (index2 <= num7)
                    {
                        chars2[index2] = erfLocalizedStringList[index1].StringText[index2];
                         ++index2;
                        
                    }

                    binaryWriter.Write(chars2);
                 ++index1;
                    
                }
            }

            fileStream.Seek(num1 + 160, SeekOrigin.Begin);
            var num8 = 0;
            var num9 = inputFileList.Length - 1;
            var index3 = num8;
            while (index3 <= num9)
            {
                var index1 = 0;
                do
                {
                    chars1[index1] = char.MinValue;
                    ++index1;
                } while (index1 <= 15);

                var withoutExtension = Path.GetFileNameWithoutExtension(inputFileList[index3]);
                var num2 = 0;
                if (withoutExtension != null)
                {
                    var num3 = withoutExtension.Length - 1;
                    var index2 = num2;
                    while (index2 <= num3)
                    {
                        chars1[index2] = withoutExtension[index2];
                        ++index2;
                    }
                }

                binaryWriter.Write(chars1);
                binaryWriter.Write(index3);
                binaryWriter.Write(ResourceHandler.GetIdForRsrcType(Strings.Mid(Path.GetExtension(inputFileList[index3]), 2)));
                binaryWriter.Write((byte) 0);
                binaryWriter.Write((byte) 0);
                ++index3;
                
            }

            var uint321 = Convert.ToUInt32(checked(fileStream.Position + inputFileList.Length * 8));
            var num10 = 0;
            var num11 = inputFileList.Length - 1;
            var index4 = num10;
            while (index4 <= num11)
            {
                binaryWriter.Write(uint321);
                var uint322 = Convert.ToUInt32(new FileInfo(inputFileList[index4]).Length);
                binaryWriter.Write(uint322);
                uint321 = Convert.ToUInt32(Convert.ToInt64(uint321) + Convert.ToInt64(uint322));
                ++index4;
            }

            var num12 = 0;
            var num13 = inputFileList.Length - 1;
            var index5 = num12;
            while (index5 <= num13)
            {
                var binaryReader = new BinaryReader(new FileStream(inputFileList[index5], FileMode.Open, FileAccess.Read));
                fileStream.Write(binaryReader.ReadBytes((int) new FileInfo(inputFileList[index5]).Length), 0, (int) new FileInfo(inputFileList[index5]).Length);
                binaryReader.Close();
                ++index5;
            }
             binaryWriter.Close();
        }


        public ErfParser(FileStream fs)
        {
            var binaryReader = new BinaryReader(fs, Encoding.ASCII);
            
                var asciiEncoding = new ASCIIEncoding();
                var stringBuilder = new StringBuilder();
                _fstream = fs;
                fs.Seek(16L, SeekOrigin.Begin);
                EntryCount = binaryReader.ReadInt32();
                fs.Seek(4L, SeekOrigin.Current);
                _offKeyList = binaryReader.ReadInt32();
                _offsetResList = binaryReader.ReadInt32();
                KeyEntryList = new ArrayList(EntryCount);
                fs.Seek(_offKeyList, SeekOrigin.Begin);
                _keyData = binaryReader.ReadBytes(checked(24 * EntryCount));
                _resInfoData = binaryReader.ReadBytes(checked(16 * EntryCount));
                var num1 = 0;
                var num2 = checked(EntryCount - 1);
                var index = num1;
                while (index <= num2)
                {
                    stringBuilder.Append(asciiEncoding.GetString(_keyData, index * 24, 16));
                    var num3 = 0;
                    while (_keyData[num3 + index * 24] != 0)
                    {
                        ++num3;
                        if (num3 > 15) break;
                    }

                    stringBuilder.Length = num3;
                    var resourceName = stringBuilder.ToString();
                    stringBuilder.Length = 0;
                    var resourceId = (int) Math.Round(_keyData[index * 24 + 16] + _keyData[index * 24 + 17] * 256.0 +
                                                      _keyData[index * 24 + 18] * 65536.0 +
                                                      _keyData[index * 24 + 19] * 16777216.0);
                    var resType = (short) Math.Round(_keyData[index * 24 + 20] + _keyData[index * 24 + 21] * 256.0);
                    var offset = (int) Math.Round(_resInfoData[index * 8] + _resInfoData[index * 8 + 1] * 256.0 +
                                                  _resInfoData[index * 8 + 2] * 65536.0 +
                                                  _resInfoData[index * 8 + 3] * 16777216.0);
                    var length = (int) Math.Round(_resInfoData[index * 8 + 4] + _resInfoData[index * 8 + 5] * 256.0 +
                                                  _resInfoData[index * 8 + 6] * 65536.0 +
                                                  _resInfoData[index * 8 + 7] * 16777216.0);
                    KeyEntryList.Add(new ErfKeyEntry(resourceName, resType, resourceId, offset, length, index));
                    ++index;
                    
                
            }
        }


        public byte[] GetErfResource(int index)
        {
            var binaryReader = new BinaryReader(_fstream, Encoding.ASCII);
            var keyEntry = (ErfKeyEntry) KeyEntryList[index];
            var numArray = new byte[keyEntry.Length - 1 + 1];
            _fstream.Seek(keyEntry.Offset, SeekOrigin.Begin);
            return binaryReader.ReadBytes(keyEntry.Length);
        }


        public byte[] GetErfResource(string resRef, int resType)
        {
            return GetErfResource(FindIndexForResRef(resRef, resType));
        }


        public int FindIndexForResRef(string resRef, int resType)
        {
            var index = 0;
            foreach (ErfKeyEntry keyEntry in KeyEntryList)
                if (StringType.StrCmp(Strings.LCase(keyEntry.ResourceName), Strings.LCase(resRef), false) == 0 &&
                    keyEntry.ResType == resType)
                {
                    index = keyEntry.Index;
                    break;
                }

            return index;
        }


        public byte[] GetErftpcResourceHeader(int index)
        {
            var binaryReader = new BinaryReader(_fstream, Encoding.ASCII);
            var keyEntry = (ErfKeyEntry) KeyEntryList[index];
            var numArray = new byte[16];
            _fstream.Seek(keyEntry.Offset, SeekOrigin.Begin);
            return binaryReader.ReadBytes(16);
        }

       
    }
}