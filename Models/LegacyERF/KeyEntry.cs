﻿using System;
using System.IO;
using System.Text;

namespace KotorStudio.Models
{
    [Serializable]
    public class KeyEntry
    {
        private int _index;
        private int _resId;
        private short _resourceType;
        private readonly string _resRef;

        public KeyEntry(string sResRef, short iResourceType, int iResId)
        {
            _resRef = sResRef;
            _resourceType = iResourceType;
            _resId = iResId;
        }

        public KeyEntry(int index, FileStream fs, int offset)
        {
            var binaryReader = new BinaryReader(fs, Encoding.ASCII);
            var chArray1 = new char[16];
            _index = index;
            fs.Seek(offset, SeekOrigin.Begin);
            var chArray2 = binaryReader.ReadChars(16);
            var index1 = 0;
            while (chArray2[index1] != 0)
            {
                checked
                {
                    ++index1;
                }

                if (index1 > 15)
                    break;
            }

            _resRef = new StringBuilder(new string(chArray2))
            {
                Length = index1
            }.ToString();
            _resourceType = binaryReader.ReadInt16();
            _resId = binaryReader.ReadInt32();
        }

        public KeyEntry(int index, byte[] data, int offset)
        {
            var numArray = new byte[16];
            var asciiEncoding = new ASCIIEncoding();
            _index = index;
            var stringBuilder = new StringBuilder(asciiEncoding.GetString(data, offset, 16));
            var index1 = 0;
            while (stringBuilder[index1] != 0)
            {
                checked
                {
                    ++index1;
                }

                if (index1 > 15)
                    break;
            }

            stringBuilder.Length = index1;
            _resRef = stringBuilder.ToString();
            _resourceType = GetInt16FromArray(data, checked(offset + 16));
            _resId = GetInt32FromArray(data, checked(offset + 18));
        }

        public object Text => _resRef;

        private short GetInt16FromArray(byte[] arr, int offset)
        {
            return checked((short) Math.Round(
                arr[offset] + arr[checked(offset + 1)] * 256.0));
        }

        private int GetInt32FromArray(byte[] arr, int offset)
        {
            return checked((int) Math.Round(arr[offset] + arr[checked(offset + 1)] * 256.0 +
                                            arr[checked(offset + 2)] * 65536.0 +
                                            arr[checked(offset + 3)] * 16777216.0));
        }

       
        
    }
}