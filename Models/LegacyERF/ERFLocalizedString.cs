﻿

using System;

namespace KotorStudio.Models.LegacyERF
{
    [Serializable]
    public abstract class ErfLocalizedString
    {
        public readonly int LanguageId;
        public readonly int StringSize;
        public readonly char[] StringText;

        protected ErfLocalizedString(int languageId, string stringText)
        {
            LanguageId = languageId;
            StringSize = stringText.Length;
            StringText = stringText.ToCharArray();
        }
    }
}