﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using KotorStudio.Models.AuroraInterfaces;

namespace KotorStudio.Models.AuroraParsers
{
    public class RimObject : IRimObject
    {

        public struct RimHeader
        {
            public char[] FileType;                 //Char 4
            public char[] FileVersion;              //Char 4
            public UInt32 Unknown1;
            public UInt32 EntryCount;
            public UInt32 OffsetToKeyList;
            public byte[] Reserved;                 //Byte 64
        }

        public struct RimKey
        {
            public char[] ResRef;             //Char 16 NULL terminated
            public UInt16 ResType;
            public UInt16 ResId;              //Resource ID
            public UInt16 Reserved;
            public UInt32 Offset;
            public UInt32 Length;
        }

        public struct RimResource
        {
            public char[] ResRef;             //Char 16 NULL terminated
            public UInt16 ResType;
            public UInt32 Length;
            public byte[] Bytes;
        }

        private readonly AuroraFile _file;
        private BinaryReader _reader;
        private byte[] _byteBuffer;

        private RimHeader _header;
        private readonly List<RimKey> _keys = new List<RimKey>();


        public RimObject(AuroraFile file)
        {
            _file = file;
        }

        public void Read()
        {
            _file.Open();
            _reader = _file.GetReader();

            _header.FileType = _reader.ReadChars(4);
            _header.FileVersion = _reader.ReadChars(4);
            _header.Unknown1 = _reader.ReadUInt32();
            _header.EntryCount = _reader.ReadUInt32();
            _header.OffsetToKeyList = _reader.ReadUInt32();
            _header.Reserved = _reader.ReadBytes(100); //Reserved bytes

            _reader.BaseStream.Position = _header.OffsetToKeyList;

            for(int i = 0; i!= _header.EntryCount; i++)
            {

                RimKey key = new RimKey();
                key.ResRef = _reader.ReadChars(16);
                key.ResType = _reader.ReadUInt16();
                key.ResId = _reader.ReadUInt16();
                key.Reserved = _reader.ReadUInt16();
                key.Offset = _reader.ReadUInt32();
                key.Length = _reader.ReadUInt32();
                _keys.Add(key);

                Debug.WriteLine(new string(key.ResRef));

            }

            _file.Close();
        }

        public byte[] GetRawResource(RimKey key)
        {
            _file.Open();
            _reader = _file.GetReader();
            _reader.BaseStream.Position = key.Offset;
            byte[] bytes = _reader.ReadBytes((int)key.Length);
            _file.Close();

            return bytes;

        }

        public RimKey GetResourceByKey(String key, int restype)
        {

            Debug.WriteLine("Searching: " + _file.GetFilename());
            foreach (RimKey rimkey in _keys)
            {
                var comp = new string(rimkey.ResRef).Replace("\0", string.Empty);
                Debug.WriteLine("Resource Name: " + new string(rimkey.ResRef));
                if (comp == key && rimkey.ResType == (ushort)restype)
                {
                    
                    return rimkey;
                }
            }
            return new RimKey();
        }

        public AuroraFile GetFile()
        {
            return _file;
        }

        public List<RimKey> GetRimKeyList()
        {
            return _keys;
        }

    }
}
