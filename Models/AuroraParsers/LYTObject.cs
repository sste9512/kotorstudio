﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using KotorStudio.Models.AuroraInterfaces;
using Vector3 = OpenTK.Vector3;

namespace KotorStudio.Models.AuroraParsers
{
    public class LytObject : ILytObject
    {
        AuroraFile _file;
        StreamReader _reader;

        public List<Room> Rooms = new List<Room>();
        public List<DoorHook> DoorHooks = new List<DoorHook>();

        public String FileDependancy = "";

        public int TrackCount = 0;
        public int ObstacleCount = 0;
        public int RoomCount = 0;
        public int DoorHookCount = 0;


        public LytObject(AuroraFile file)
        {
            _file = file;
            _file.IsText = true;
        }

        public void Read()
        {
            _file.Open();
            _reader = _file.GetStreamReader();

            String line;
            Boolean readingRooms = false;
            Boolean readingDoorHooks = false;
            while ((line = _reader.ReadLine()) != null)
            {
                line = line.Trim();
                if (line.Contains("MAXLAYOUT"))
                {
                }
                else if (line.Contains("filedependancy"))
                {
                    string[] arr = line.Split(' ');
                    FileDependancy = arr[1];
                }
                else if (line.Contains("trackcount"))
                {
                    string[] arr = line.Split(' ');
                    TrackCount = Int32.Parse(arr[1]);
                    readingRooms = false;
                    readingDoorHooks = false;
                }
                else if (readingRooms)
                {
                    Rooms.Add(Room.FromLyt(line));
                }
                else if (line.Contains("obstaclecount"))
                {
                    string[] arr = line.Split(' ');
                    ObstacleCount = Int32.Parse(arr[1]);
                    readingRooms = false;
                    readingDoorHooks = false;
                }
                else if (readingDoorHooks)
                {
                }
                else if (line.Contains("roomcount"))
                {
                    string[] arr = line.Split(' ');
                    RoomCount = Int32.Parse(arr[1]);
                    readingRooms = true;
                    readingDoorHooks = false;
                }
                else if (line.Contains("doorhookcount"))
                {
                    string[] arr = line.Split(' ');
                    DoorHookCount = Int32.Parse(arr[1]);
                    readingRooms = false;
                    readingDoorHooks = true;
                }
                else if (line.Contains("donelayout"))
                {
                    readingRooms = false;
                    readingDoorHooks = false;
                    break;
                }
            }

            Debug.WriteLine(FileDependancy);

            _file.Close();
        }


        public class Room
        {
            public String Model;
            public Vector3 Position;

            public Room(String model, Vector3 position)
            {
                Model = model;
                Position = position;
            }

            public static Room FromLyt(String lyt)
            {
                string[] arr = lyt.Trim().Split(' ');
                Debug.WriteLine(lyt);
                return new Room(arr[0].ToLower(),
                    new Vector3(float.Parse(arr[1]), float.Parse(arr[2]), float.Parse(arr[3])));
            }
        }

        public class DoorHook
        {
            Vector3 Position;

            public static DoorHook FromLyt(String lyt)
            {
                return new DoorHook();
            }
        }
    }
}