﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using KotorStudio.Models.AuroraInterfaces;

namespace KotorStudio.Models.AuroraParsers
{
   

    public class ErfObject : IErfObject
    {

        //0xFFFF

        public struct ErfHeader
        {
            public char[] FileType;                 //Char 4
            public char[] FileVersion;              //Char 4
            public Int32 LanguageCount;
            public Int32 LocalizedStringSize;
            public Int32 EntryCount;
            public Int32 OffsetToLocalizedString;
            public Int32 OffsetToKeyList;
            public Int32 OffsetToResourceList;
            public byte[] BuildYear;                //Byte 4
            public byte[] BuildDay;                 //Byte 4
            public byte[] DescriptionStrRef;        //Byte 4
            public byte[] Reserved;                 //Byte 116
        }

        public enum LanguageIDs
        {
            English = 0,
            French = 1,
            German = 2,
            Italian = 3,
            Spanish = 4,
            Polish = 5,
            Korean = 128,
            ChineseTraditional = 129,
            ChineseSimplified = 130,
            Japanese = 131
        }

        public struct LocalizedString
        {
            public Int32 LanguageId;
            public Int32 StringSize;
            public String String;
        }

        //ResRef = KeyName ex.w_Shortswrd_004
        public struct ErfKey
        {
            public char[] ResRef;             //Char 16 NULL terminated
            public UInt32 ResId;              //Resource ID
            public UInt16 ResType;
            public UInt16 Unused;
        }

        public struct ErfResource
        {
            public UInt32 OffsetToResource;
            public UInt32 ResourceSize;
        }

        private AuroraFile _file;
        private BinaryReader _reader;
        private byte[] _byteBuffer;

        public ErfHeader _header;
        static List<LocalizedString> _stringList = new List<LocalizedString>();
        static List<ErfKey> _keyList = new List<ErfKey>();
        static List<ErfResource> _resourceList = new List<ErfResource>();
       

        public ErfObject(AuroraFile file)
        {
            _file = file;
            _header = new ErfHeader();
            _stringList = new List<LocalizedString>();
            _keyList = new List<ErfKey>();
            _resourceList = new List<ErfResource>();
        }

        public ErfObject(string filePath)
        {
            _file = new AuroraFile(filePath);
            _header = new ErfHeader();
            _stringList = new List<LocalizedString>();
            _keyList = new List<ErfKey>();
            _resourceList = new List<ErfResource>();
            Read();
        }
        
        //Opens the file
        public void Open()
        {
            _file.Open();
            _reader = _file.GetReader();
            _reader.BaseStream.Position = 0;
        }

        public void Seek(int bytes)
        {
            _reader.BaseStream.Position = bytes;
        }

        public AuroraFile GetFile()
        {
            return _file;
        }

        public void Read()
        {
            Open();
            if (_reader != null)
            {
                Seek(56);
                _byteBuffer = _reader.ReadBytes((int)_reader.BaseStream.Length - 56);

                Seek(0);

                //Get HEADER Data

                _header = ReadHeader();

                //END HEADER

                /*Debug.WriteLine("[Header]");
                Debug.WriteLine(new string(Header.FileType));
                Debug.WriteLine(new string(Header.FileVersion));
                Debug.WriteLine(Header.LanguageCount);
                Debug.WriteLine(Header.LocalizedStringSize);
                Debug.WriteLine(Header.EntryCount);
                Debug.WriteLine(Header.OffsetToLocalizedString);
                Debug.WriteLine(Header.OffsetToKeyList);
                Debug.WriteLine(Header.OffsetToResourceList);
                Debug.WriteLine(BitConverter.ToInt32(Header.BuildYear, 0) + 1900);
                Debug.WriteLine(BitConverter.ToInt32(Header.BuildDay, 0));*/

                Seek(_header.OffsetToLocalizedString);

                Debug.WriteLine("[Language]");
                for (int i = 0; i!=_header.LanguageCount; i++)
                {
                    LocalizedString str = new LocalizedString();
                    str.LanguageId = _reader.ReadInt32();
                    str.StringSize = _reader.ReadInt32();
                    str.String = new string(_reader.ReadChars(str.StringSize));
                    _stringList.Add(str); 
                }

                Seek(_header.OffsetToKeyList);

                //Debug.WriteLine("[Keys]");
                for (int i = 0; i != _header.EntryCount; i++)
                {
                    ErfKey str = new ErfKey();
                    str.ResRef = _reader.ReadChars(16);
                    str.ResId = _reader.ReadUInt32();
                    str.ResType = _reader.ReadUInt16();
                    str.Unused = _reader.ReadUInt16();
                    _keyList.Add(str);
                 }

                Seek(_header.OffsetToResourceList);

                //Debug.WriteLine("[Resources]");
                for (int i = 0; i != _header.EntryCount; i++)
                {
                    ErfResource str = new ErfResource();
                    str.OffsetToResource = _reader.ReadUInt32();
                    str.ResourceSize = _reader.ReadUInt32();
                    _resourceList.Add(str);
                }

            }
            _file.Close(); //Close the file because we are done reading data...

        }

        //Reads and sets the files header data
        public ErfHeader ReadHeader()
        {

            ErfHeader tmp = new ErfHeader();
            tmp.FileType = _reader.ReadChars(4);
            tmp.FileVersion = _reader.ReadChars(4);
            tmp.LanguageCount = _reader.ReadInt32();
            tmp.LocalizedStringSize = _reader.ReadInt32();
            tmp.EntryCount = _reader.ReadInt32();
            tmp.OffsetToLocalizedString = _reader.ReadInt32();
            tmp.OffsetToKeyList = _reader.ReadInt32();
            tmp.OffsetToResourceList = _reader.ReadInt32();
            tmp.BuildYear = _reader.ReadBytes(4);                //Byte 4
            tmp.BuildDay = _reader.ReadBytes(4);                 //Byte 4
            tmp.DescriptionStrRef = _reader.ReadBytes(4);        //Byte 4
            tmp.Reserved = _reader.ReadBytes(116);                 //Byte 116

            return tmp;
        }

        public List<ErfKey> GetKeys()
        {
            return _keyList;
        }

        public List<ErfResource> GetResources()
        {
            return _resourceList;
        }

        public byte[] GetRawResource(ErfResource resource)
        {
            _file.Open();
            _reader = _file.GetReader();
            _reader.BaseStream.Position = resource.OffsetToResource;
            byte[] bytes = _reader.ReadBytes((int)resource.ResourceSize);
            _file.Close();
            return bytes;  
        }

        public async Task<byte[]> GetRawResourceAsync(ErfResource resource)
        {
            byte[] task = await Task.Factory.StartNew(() =>
            {
                _file.Open();
                _reader = _file.GetReader();
                _reader.BaseStream.Position = resource.OffsetToResource;
                byte[] bytes = _reader.ReadBytes((int)resource.ResourceSize);
                _file.Close();
                return bytes;  
            });
            return task;
        }

        public void WriteToFile(string fileName, byte[] rawResourceBytes)
        {
            File.WriteAllBytes(fileName + ".tpc", rawResourceBytes);
        }
        
        public ErfResource GetResourceByKey(String key, int restype)
        {
            foreach (ErfKey erfkey in _keyList)
            {
                if (new string(erfkey.ResRef).Replace("\0", string.Empty) == key && erfkey.ResType == (ushort) restype)
                {
                    Debug.WriteLine("Resource Type: "+erfkey.ResType+ " ID: "+ erfkey.ResId);
                    return _resourceList[(int)erfkey.ResId];
                }
            }
            return new ErfResource();
        }


    }

}
