﻿using System;
using System.Collections.Generic;
using System.IO;
using KotorStudio.Models.AuroraInterfaces;

namespace KotorStudio.Models.AuroraParsers
{
    public class BiffObject : IBiffObject
    {
        public struct BiffHeader
        {
            public char[] FileType;
            public char[] FileVersion;
            public UInt32 VariableResourceCount;
            public UInt32 FixedResourceCount;
            public UInt32 VariableTableOffset;
        }

        public struct VResourceHeader
        {
            public UInt32 Id;
            public UInt32 Offset;
            public UInt32 FileSize;
            public UInt32 ResourceType;
        }

        //Not used in spec
        public struct FResourceHeader
        {
            public UInt32 Id;
            public UInt32 Offset;
            public UInt32 PartCount;
            public UInt32 FileSize;
            public UInt32 ResourceType;
        }

        AuroraFile _file;
        BinaryReader _reader;
        BiffHeader _header;
        List<VResourceHeader> _variableResourceList = new List<VResourceHeader>();

        public BiffObject(AuroraFile file)
        {
            _file = file;
        }

        public void Read()
        {
            _file.Open();
            _reader = _file.GetReader();

            _header.FileType = _reader.ReadChars(4);
            _header.FileVersion = _reader.ReadChars(4);
            _header.VariableResourceCount = _reader.ReadUInt32();
            _header.FixedResourceCount = _reader.ReadUInt32();
            _header.VariableTableOffset = _reader.ReadUInt32();

            _reader.BaseStream.Position = _header.VariableTableOffset;
            for (int i = 0; i != _header.VariableResourceCount; i++)
            {
                VResourceHeader res = new VResourceHeader();
                res.Id = _reader.ReadUInt32();
                res.Offset = _reader.ReadUInt32();
                res.FileSize = _reader.ReadUInt32();
                res.ResourceType = _reader.ReadUInt32();
                _variableResourceList.Add(res);
            }

            _file.Close();
        }

        public VResourceHeader FindFileById(UInt32 id)
        {
            System.Diagnostics.Debug.Write("Finding: " + id);
            foreach (VResourceHeader res in _variableResourceList)
            {
                if (res.Id == id)
                {
                    return res;
                }
            }

            return new VResourceHeader();
        }

        public AuroraFile ExtractVResource(VResourceHeader fileHeader, KeyObject.KeyTable key)
        {
            byte[] bytes = {0};
            _file.Open();
            _reader = _file.GetReader();
            _reader.BaseStream.Position = fileHeader.Offset;
            bytes = _reader.ReadBytes((int) fileHeader.FileSize);

            _file.Close();

            return new AuroraFile(bytes, new string(key.ResRef), key.ResourceType);
        }

        public AuroraFile ExtractFile(String filename, UInt32 restype)
        {
            byte[] bytes = {0};
            KeyObject keyObject = new KeyObject(_file);
            keyObject.Read();
            KeyObject.KeyTable key = keyObject.FindFileKey(filename, restype);

            VResourceHeader fileHeader = FindFileById(key.ResId);
            _file.Open();
            _reader = _file.GetReader();
            _reader.BaseStream.Position = fileHeader.Offset;
            bytes = _reader.ReadBytes((int) fileHeader.FileSize);

            _file.Close();

            return new AuroraFile(bytes, new string(key.ResRef), key.ResourceType);
        }

        public String GetFilename()
        {
            return _file.GetFilename();
        }

        public List<VResourceHeader> GetResources()
        {
            return _variableResourceList;
        }
    }
}