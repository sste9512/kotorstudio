﻿using System;
using System.Collections.Generic;
using System.IO;
using KotorStudio.Models.AuroraInterfaces;

namespace KotorStudio.Models.AuroraParsers
{
    public class KeyObject : IKeyObject
    {

        public struct KeyHeader
        {
            public char[] FileType;
            public char[] FileVersion;
            public UInt32 BifCount;
            public UInt32 KeyCount;
            public UInt32 OffsetToFileTable;
            public UInt32 OffsetToKeyTable;
            public UInt32 BuildYear;
            public UInt32 BuildDay;
            public byte[] Reserved;
        }

        public struct KeyTable
        {
            public char[] ResRef;
            public UInt16 ResourceType;
            public UInt32 ResId;
        }

        private AuroraFile _file;
        private BinaryReader _reader;

        private KeyHeader _header;
        private List<KeyTable> _keysList;

        public KeyObject(AuroraFile file)
        {
            _file = file;
            _header = new KeyHeader();
            _keysList = new List<KeyTable>();
        }

        public void Read()
        {
            _file.Open();
            _reader = _file.GetReader();


            _header.FileType             = _reader.ReadChars(4);
            _header.FileVersion          = _reader.ReadChars(4);
            _header.BifCount             = _reader.ReadUInt32();
            _header.KeyCount             = _reader.ReadUInt32();
            _header.OffsetToFileTable    = _reader.ReadUInt32();
            _header.OffsetToKeyTable     = _reader.ReadUInt32();
            _header.BuildYear            = _reader.ReadUInt32();
            _header.BuildDay             = _reader.ReadUInt32();
            _header.Reserved             = _reader.ReadBytes(32);

            _reader.BaseStream.Position = _header.OffsetToKeyTable;

            for(int i = 0; i!=_header.KeyCount; i++)
            {
                KeyTable key = new KeyTable();
                key.ResRef          = _reader.ReadChars(16);
                key.ResourceType    = _reader.ReadUInt16();
                key.ResId           = _reader.ReadUInt32();

                _keysList.Add(key);
            }


            _file.Close();
        }


        public KeyTable FindFileKey(String resRef, UInt32 resourceType)
        {
            System.Diagnostics.Debug.WriteLine("Searching for: " + resRef);
            foreach (KeyTable key in _keysList)
            {
                if ( new string(key.ResRef).Replace("\0", string.Empty).Equals(resRef) && key.ResourceType == resourceType)
                {
                    System.Diagnostics.Debug.WriteLine("Found: "+new string(key.ResRef));
                    return key;
                }
            }
            return _keysList[0];
        }

    }
}
