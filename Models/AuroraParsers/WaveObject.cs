﻿using System;
using System.Diagnostics;
using System.IO;
using KotorStudio.Models.AuroraInterfaces;

namespace KotorStudio.Models.AuroraParsers
{
    public class WaveObject : IWaveObject
    {
        private byte[] _bytes;
        private readonly AudioType _audioType = AudioType.Unknown;

        private BinaryReader _br;
        private readonly AuroraFile _file;

        private readonly int Offset = 58;

        public enum AudioType
        {
            Unknown,
            Wave,
            Mp3
        }

        public WaveObject(AuroraFile file)
        {
            _file = file;
            _file.Open();
            _br = file.GetReader();

            try
            {
                //Check for real WAVE file
                _br.BaseStream.Position = 470;
                String riff = new string(_br.ReadChars(4));
                if (riff == "RIFF")
                {
                    Offset = 470;
                    _audioType = AudioType.Wave;
                    Debug.WriteLine("Found: " + riff);
                }
                else
                {
                    Debug.WriteLine(riff);
                }
            }
            catch (Exception ex)
            {
                _audioType = AudioType.Wave;
                Debug.WriteLine(ex.ToString());
            }

            _br.BaseStream.Position = 32;
            String data = new string(_br.ReadChars(4));
            if (data == "data")
            {
                Offset = 32;
                //audioType = AudioType.WAVE;
                Debug.WriteLine("Found: " + data);
            }

            //Check for real MP3 file
            _br.BaseStream.Position = 199;
            String lame = new string(_br.ReadChars(4));
            if (lame == "LAME")
            {
                Offset = 199;
                _audioType = AudioType.Mp3;
                Debug.WriteLine("Found: " + lame);
            }
            else
            {
                Debug.WriteLine(lame);
            }

            //Check for real MP3 file
            _br.BaseStream.Position = 200;
            lame = new string(_br.ReadChars(4));
            if (lame == "LAME")
            {
                Offset = 200;
                _audioType = AudioType.Mp3;
                Debug.WriteLine(lame);
            }

            file.Close(); //Close the file because we are done reading data...
        }

        public byte[] GetPlayableByteStream()
        {
            _file.Open();

            byte[] data;
            using (var br = _file.GetReader())
            {
                br.BaseStream.Position = Offset;
                data = br.ReadBytes((int) br.BaseStream.Length - Offset);
            }

            _file.Close();

            return data;
        }

        public new AudioType GetType()
        {
            return _audioType;
        }

        public static void PlayInExternalPlayer(AuroraFile file)
        {
            WaveObject audio = new WaveObject(file);

            //byte[] bytes = audio.getPlayableByteStream();

            MemoryStream dataStream = new MemoryStream(audio.GetPlayableByteStream());

            switch (audio.GetType())
            {
                case AudioType.Wave:
                    Debug.WriteLine("Playing: WAV");
                    //simpleSound = new SoundPlayer(dataStream);
                    //simpleSound.Play();

                    FileStream fsW = new FileStream("test.wav", FileMode.Create, FileAccess.Write);
                    BinaryWriter bwW = new BinaryWriter(fsW);
                    bwW.Write(audio.GetPlayableByteStream());
                    bwW.Close();
                    fsW.Close();

                    Process.Start("test.wav");

                    break;
                case AudioType.Mp3:
                default:
                    Debug.WriteLine("Playing: MP3");

                    FileStream fs = new FileStream("test.mp3", FileMode.Create, FileAccess.Write);
                    BinaryWriter bw = new BinaryWriter(fs);
                    bw.Write(audio.GetPlayableByteStream());
                    bw.Close();
                    fs.Close();

                    Process.Start("test.mp3");

                    //Mp3FileReader mp3 = new Mp3FileReader(dataStream);
                    //WaveStream pcm = WaveFormatConversionStream.CreatePcmStream(mp3);
                    //simpleSound = new SoundPlayer(pcm);
                    //File.WriteAllBytes("test.mp3", audio.getPlayableByteStream());
                    break;
            }
        }
    }
}