﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using KotorStudio.Models.AuroraInterfaces;

namespace KotorStudio.Models.AuroraParsers
{
   

    public class GffObject : IGffObject
    {

        public struct GffHeader
        {
            public string FileType;
            public string FileVersion;
            public int StructOffset;
            public int StructCount;
            public int FieldOffset;
            public int FieldCount;
            public int LabelOffset;
            public int LabelCount;
            public int FieldDataOffset;
            public int FieldDataCount;
            public int FieldIndicesOffset;
            public int FieldIndicesCount;
            public int ListIndicesOffset;
            public int ListIndicesCount;
        }

        public struct GffStruct
        {
            public int Type;
            public int DataOrDataOffset;
            public int FieldCount;
        }

        public struct GffField
        {
            public int Type;
            public int LabelIndex;
            public byte[] DataOrDataOffset;
            public string Value; //Holds the Fields value in text form Type will be used to return it to its right form as in INT Float Double etc.
            public byte[] ComplexData;
            public int Index;

            public GffField(int type, int label, byte[] data, string val, int index)
            {
                Type = type;
                LabelIndex = label;
                DataOrDataOffset = data;
                Value = val;
                Index = index;
                ComplexData = new byte[0];
            }

            public string ReadLabel()
            {
                return GetLabel(LabelIndex);
            }

            public string ReadValue()
            {
                switch (Type)
                {
                    case (int)DataTypes.CExoLocString:

                        Stream stream = new MemoryStream(ComplexData);

                        BinaryReader cExoLocReader = new BinaryReader(stream);
                        //Console.WriteLine("Len: " + CExoLocReader.BaseStream.Length + " Bytes: " + ComplexData.Length);
                        int stringRef = cExoLocReader.ReadInt32();
                        int stringCount = cExoLocReader.ReadInt32();
                        for (int j = 0; j != stringCount; j++)
                        {
                            int _stringId = cExoLocReader.ReadInt32();
                            int stringLen = cExoLocReader.ReadInt32();
                            byte[] localString = cExoLocReader.ReadBytes(stringLen);
                            int stringId = _stringId;
                            bool feminine = (0x1 & stringId) == 0x1;
                            int languageId = stringId >> 1;
                            string gender = "Male";
                            if (feminine)
                            {
                                gender = "Female";
                            }

                            return System.Text.Encoding.UTF8.GetString(localString);
                        }

                        //Loop through
                        cExoLocReader.Close();
                        cExoLocReader.Dispose();
                        break;
                }

                return string.Empty;
            }

            public string RetVal()
            {
                return string.Empty;
            }
        }

        public enum DataTypes
        {
            Byte = 0,
            Char = 1,
            Word = 2,
            Short = 3,
            Dword = 4,
            Int = 5,
            Dword64 = 6,
            Int64 = 7,
            Float = 8,
            Double = 9,
            CExoString = 10,
            ResRef = 11,
            CExoLocString = 12,
            Void = 13,
            Struct = 14,
            List = 15
        }

        public enum LanguageIDs
        {
            English = 0,
            French = 1,
            German = 2,
            Italian = 3,
            Spanish = 4,
            Polish = 5,
            Korean = 128,
            [Description("Chinese Traditional")]
            ChineseTraditional = 129,
            [Description("Chinese Simplified")]
            ChineseSimplified = 130,
            Japanese = 131
        }

        private AuroraFile _file;
        private BinaryReader _reader;
        static GffHeader _header;
        static List<GffStruct> _structArray = new List<GffStruct>();
        static List<GffField> _listArray = new List<GffField>();
        static List<String> _labelArray = new List<String>();
        static List<GffField> _fieldArray = new List<GffField>(); //Array of all the fields
        static List<int[]> _listIndiciesArray = new List<int[]>();
        public TreeNode TreeData = new TreeNode();
        private byte[] _byteBuffer;
        private bool _debug = false;

        public GffObject(AuroraFile file)
        {
            _file = file;
            _header = new GffHeader();
            _structArray = new List<GffStruct>();
            _listArray = new List<GffField>();
            _labelArray = new List<String>();
            _fieldArray = new List<GffField>(); //Array of all the fields
            TreeData = new TreeNode();
        }

        //Opens the file
        public void Open()
        {
            _file.Open();
            _reader = _file.GetReader();
            _reader.BaseStream.Position = 0;
        }

        public string GetFileType()
        {
            return _header.FileType;
        }

        public string GetFileVersion()
        {
            return _header.FileVersion;
        }

        public void Seek(int bytes)
        {
            _reader.BaseStream.Position = bytes;
        }

        //Reads the binary data
        public void Read()
        {

            Open();

            if (_reader != null)
            {
                Seek(56);
                _byteBuffer = _reader.ReadBytes((int)_reader.BaseStream.Length - 56);

                Seek(0);

                //Get HEADER Data

                _header = ReadHeader();

                //END HEADER

                _reader.BaseStream.Position = _header.StructOffset;

                for (int i = 0; i < _header.StructCount; i++)
                {

                    GffStruct tmp = new GffStruct();

                    tmp.Type = _reader.ReadInt32();
                    tmp.DataOrDataOffset = _reader.ReadInt32();
                    tmp.FieldCount = _reader.ReadInt32();

                    _structArray.Add(tmp);

                }


                //Start Labels

                //Place all the labels in the label array

                long originalPos = _reader.BaseStream.Position;

                _reader.BaseStream.Position = _header.LabelOffset;

                for (int i = 0; i < _header.LabelCount; i++)
                {
                    string str = new string(_reader.ReadChars(16)).Replace("\0", string.Empty); ;
                    _labelArray.Add(str);
                }

                _reader.BaseStream.Position = originalPos;

                //End Labels


                //Start Fields

                for (int i = 0; i < _header.FieldCount; i++)
                {

                    int type = _reader.ReadInt32();
                    int label = _reader.ReadInt32();

                    byte[] data = _reader.ReadBytes(4); // Field data is 4Bytes long longer data is stored elsewhere but this data is used to find it

                    int index = i;

                    GffField newField = new GffField(type, label, data, ReadFieldData(data, type), index);
                    if (_debug)
                    {
                        switch (type)
                        {
                            case (int)DataTypes.Byte:
                                Console.WriteLine("[Byte] Name: " + GetLabel(label) + " Value: " + BitConverter.ToInt32(data, 0));
                                break;
                            case (int)DataTypes.CExoLocString:
                                newField.ComplexData = GetCExoLocString(BitConverter.ToInt32(data, 0));
                                Console.WriteLine("[CExoLocString] Name: " + GetLabel(label) + " Value: Complex");
                                break;
                            case (int)DataTypes.CExoString:
                                Console.WriteLine("[CExoString] Name: " + GetLabel(label) + " Value: " + GetCExoString(BitConverter.ToInt32(data, 0)));
                                break;
                            case (int)DataTypes.Char:
                                Console.WriteLine("[Char] Name: " + GetLabel(label) + " Value: " + BitConverter.ToInt32(data, 0));
                                break;
                            case (int)DataTypes.Double:
                                Console.WriteLine("[Double] Name: " + GetLabel(label) + " Value: " + GetDouble(BitConverter.ToInt32(data, 0)));
                                break;
                            case (int)DataTypes.Dword:
                                Console.WriteLine("[DWORD] Name: " + GetLabel(label) + " Value: " + BitConverter.ToInt32(data, 0));
                                break;
                            case (int)DataTypes.Dword64:
                                Console.WriteLine("[DWORD64] Name: " + GetLabel(label) + " Value: " + GetDword64(BitConverter.ToInt32(data, 0)));
                                break;
                            case (int)DataTypes.Float:
                                Console.WriteLine("[Float] Name: " + GetLabel(label) + " Value: " + BitConverter.ToSingle(data, 0));
                                break;
                            case (int)DataTypes.Int:
                                Console.WriteLine("[Int] Name: " + GetLabel(label) + " Value: " + BitConverter.ToInt32(data, 0));
                                break;
                            case (int)DataTypes.Int64:
                                Console.WriteLine("[Int64] Name: " + GetLabel(label) + " Value: " + BitConverter.ToInt32(data, 0));
                                break;
                            case (int)DataTypes.List:
                                Console.WriteLine("List: " + BitConverter.ToInt32(data, 0)); // Offset in bytes realative to the List Indices Array Start Byte
                                break;
                            case (int)DataTypes.ResRef:
                                Console.WriteLine("[ResRef] Name: " + GetLabel(label) + " Value: " + GetResRef(BitConverter.ToInt32(data, 0)));
                                break;
                            case (int)DataTypes.Short:
                                Console.WriteLine("[Short] Name: " + GetLabel(label) + " Value: " + BitConverter.ToInt32(data, 0));
                                break;
                            case (int)DataTypes.Struct:
                                Console.WriteLine("Struct: " + BitConverter.ToInt32(data, 0)); //Index of the struct
                                break;
                            case (int)DataTypes.Void:
                                Console.WriteLine("[Void] Name: " + GetLabel(label) + " Value: " + BitConverter.ToInt32(data, 0));
                                break;
                            case (int)DataTypes.Word:
                                Console.WriteLine("[Word] Name: " + GetLabel(label) + " Value: " + BitConverter.ToInt32(data, 0));
                                break;
                        }
                    }
                    else
                    {
                        switch (type)
                        {
                            case (int)DataTypes.CExoLocString:
                                newField.ComplexData = GetCExoLocString(BitConverter.ToInt32(data, 0));
                                break;
                        }
                    }

                    _fieldArray.Add(newField);

                }

                //EOF

                //PrintableData();

                _file.Close(); //Close the file because we are done reading data...

            }
            else
            {

            }

        }

        public void SaveGff()
        {
            //Test out the save function by writing out the header
            //FileStream fs = new FileStream("header.tmp", FileMode.Create);
            //BinaryWriter br = new BinaryWriter(fs);

            //Create buffers
            byte[] structArray = new byte[0];
            byte[] fieldArray = new byte[0];
            byte[] fieldDataBlock = new byte[0]; //This byte array holds all the Field data that is bigger that 32Byte Dwords

            char[] fileType = _header.FileType.ToCharArray(0, _header.FileType.Length);
            char[] fileVersion = _header.FileVersion.ToCharArray(0, _header.FileVersion.Length);

            /*br.Write(FileType);
            br.Write(FileVersion);
            br.Write((UInt32)Header.StructOffset);
            br.Write((UInt32)Header.StructCount);
            br.Write((UInt32)Header.FieldOffset);
            br.Write((UInt32)Header.FieldCount);
            br.Write((UInt32)Header.LabelOffset);
            br.Write((UInt32)Header.LabelCount);
            br.Write((UInt32)Header.FieldDataOffset);
            br.Write((UInt32)Header.FieldDataCount);
            br.Write((UInt32)Header.FieldIndicesOffset);
            br.Write((UInt32)Header.FieldIndicesCount);
            br.Write((UInt32)Header.ListIndicesOffset);
            br.Write((UInt32)Header.ListIndicesCount);*/

            //Create the Struct Array
            for (int i = 0; i != _structArray.Count - 1; i++)
            {
                GffStruct _Struct = _structArray[i];

                byte[] structBuffer = new byte[12];
                Buffer.BlockCopy(new int[_Struct.Type, _Struct.DataOrDataOffset, _Struct.FieldCount], 0, structBuffer, 0, 12);

                Buffer.BlockCopy(structBuffer, 0, structArray, 0, structArray.Length + 12);
            }

            //Create the Field Array
            for (int i = 0; i != _fieldArray.Count - 1; i++)
            {
                GffField field = _fieldArray[i];

                byte[] fieldBuffer = new byte[12];

                int dataOrDataOffset = BitConverter.ToInt32(field.DataOrDataOffset, 0);

                //Check to see if data is complex
                switch (field.Type)
                {

                    //Complex data must be stored in the FieldDataBlock and a reference to the byte offset stored in the field struct
                    case (int)DataTypes.Dword64:
                        dataOrDataOffset = fieldDataBlock.Length; //Record the FieldDataBlock current offset

                        UInt64 dword = UInt64.Parse(field.Value); //Convert the data string back to a DWORD
                        byte[] dwordBytes = BitConverter.GetBytes(dword); //Convert the UINT64 to an 8Byte Array

                        Buffer.BlockCopy(dwordBytes, 0, fieldDataBlock, fieldDataBlock.Length, 12); //Add the DWORD Bytes to the FIELDDATABLOCK

                        break;
                    case (int)DataTypes.Int64:

                        dataOrDataOffset = fieldDataBlock.Length; //Record the FieldDataBlock current offset

                        Int64 int64 = Int64.Parse(field.Value); //Convert the data string back to a INT64
                        byte[] int64Bytes = BitConverter.GetBytes(int64); //Convert the INT64 to an 8Byte Array

                        Buffer.BlockCopy(int64Bytes, 0, fieldDataBlock, fieldDataBlock.Length, 12); //Add the INT64 Bytes to the FIELDDATABLOCK

                        break;
                    case (int)DataTypes.Double:

                        dataOrDataOffset = fieldDataBlock.Length; //Record the FieldDataBlock current offset

                        Double DOUBLE = Double.Parse(field.Value); //Convert the data string back to a Double
                        byte[] doubleBytes = BitConverter.GetBytes(DOUBLE); //Convert the Double to an 8Byte Array

                        Buffer.BlockCopy(doubleBytes, 0, fieldDataBlock, fieldDataBlock.Length, 12); //Add the Double Bytes to the FIELDDATABLOCK

                        break;
                    case (int)DataTypes.CExoString:

                        dataOrDataOffset = fieldDataBlock.Length; //Record the FieldDataBlock current offset

                        string cExoString = field.Value;

                        byte[] cExoStringBytes = System.Text.Encoding.UTF8.GetBytes(cExoString); //Get the bytes of the CExoString

                        byte[] cExoStringsize = BitConverter.GetBytes((UInt32)cExoStringBytes.Length); //Get the byte size of the CExoString 4 Bytes long

                        byte[] nCExoStringBytes = new byte[cExoStringsize.Length + cExoStringBytes.Length];

                        Buffer.BlockCopy(cExoStringsize, 0, nCExoStringBytes, 0, cExoStringsize.Length);
                        Buffer.BlockCopy(cExoStringBytes, 0, nCExoStringBytes, cExoStringsize.Length, cExoStringBytes.Length);

                        Buffer.BlockCopy(nCExoStringBytes, 0, fieldDataBlock, fieldDataBlock.Length, nCExoStringBytes.Length); //Add the RESREF Bytes to the FIELDDATABLOCK

                        break;
                    case (int)DataTypes.ResRef:

                        dataOrDataOffset = fieldDataBlock.Length; //Record the FieldDataBlock current offset

                        string str = field.Value;
                        str = str.Substring(0, 16);

                        byte[] resrefBytes = System.Text.Encoding.UTF8.GetBytes(str); //Get the bytes of the ResRef

                        byte size = Convert.ToByte(resrefBytes.Length); //Get the byte size of the ResRef


                        byte[] nResrefBytes = new byte[resrefBytes.Length + 1];
                        resrefBytes.CopyTo(nResrefBytes, 1);
                        nResrefBytes[0] = size; //Prepend the size byte to the RESREF Array
                        resrefBytes = nResrefBytes;


                        Buffer.BlockCopy(resrefBytes, 0, fieldDataBlock, fieldDataBlock.Length, resrefBytes.Length); //Add the RESREF Bytes to the FIELDDATABLOCK





                        break;
                    case (int)DataTypes.CExoLocString:

                        dataOrDataOffset = fieldDataBlock.Length; //Record the FieldDataBlock current offset

                        Buffer.BlockCopy(field.ComplexData, 0, fieldDataBlock, fieldDataBlock.Length, field.ComplexData.Length); //Add the CExoLocString Bytes to the FIELDDATABLOCK

                        break;
                    case (int)DataTypes.Void:

                        dataOrDataOffset = fieldDataBlock.Length; //Record the FieldDataBlock current offset

                        break;
                        /*case (int)DataTypes.Struct:

                        break;
                        case (int)DataTypes.List:

                        break;*/
                }

                Buffer.BlockCopy(new int[field.Type, field.LabelIndex, dataOrDataOffset], 0, fieldBuffer, 0, 12); //Fill the Field Buffer with its data

                Buffer.BlockCopy(fieldBuffer, 0, fieldArray, fieldArray.Length, 12); //Add the Field Buffer to the Field Array
            }

            //Create the Labels array


            //Create the ListIndices


            //Test rest of file
            //br.Write(ByteBuffer);


            //br.Close();

        }

        //Reads and sets the files header data
        public GffHeader ReadHeader()
        {

            GffHeader tmp = new GffHeader();

            tmp.FileType = new string(_reader.ReadChars(4));
            tmp.FileVersion = new string(_reader.ReadChars(4));

            tmp.StructOffset = _reader.ReadInt32();
            tmp.StructCount = _reader.ReadInt32();
            tmp.FieldOffset = _reader.ReadInt32();
            tmp.FieldCount = _reader.ReadInt32();
            tmp.LabelOffset = _reader.ReadInt32();
            tmp.LabelCount = _reader.ReadInt32();
            tmp.FieldDataOffset = _reader.ReadInt32();
            tmp.FieldDataCount = _reader.ReadInt32();
            tmp.FieldIndicesOffset = _reader.ReadInt32();
            tmp.FieldIndicesCount = _reader.ReadInt32();
            tmp.ListIndicesOffset = _reader.ReadInt32();
            tmp.ListIndicesCount = _reader.ReadInt32();

            return tmp;
        }

        //Loops through all the retrieved data to see if it can create the data tree correctly. If so that means all the data was gathered correctly
        public void PrintableData()
        {
            _file.Open();
            _reader = _file.GetReader();
            Console.WriteLine("\nPrinting Data:\n");
            //Start with the main struct and iterate tree view style following next child objects before proceding to the next element

            TreeData = new TreeNode("[STRUCT ID: -1]");
            TreeData.Tag = new GffField(14, 0, null, "STRUCT", -1);
            Console.WriteLine("[-] Struct[0]: \tChildren: " + _structArray[0].FieldCount + "\t Fields: " + _structArray[0].DataOrDataOffset);
            //Next loop through child elements of the main struct which will be the field because a struct has no direct child structs


            List<GffField> fields = GetStructFields(_structArray[0].DataOrDataOffset, _structArray[0].FieldCount);
            PrintFields(fields, 0, TreeData);




            for (int i = 1; i != _structArray.Count; i++)
            {
                //Console.WriteLine(" [+] Struct["+i+"]: \tChildren: " + StructArray[i].FieldCount+"\t Fields: " + StructArray[i].DataOrDataOffset);
            }
            _file.Close();

        }

        public void PrintFields(List<GffField> fieldList, int indent, TreeNode node)
        {
           
            for (int i = 0; i != fieldList.Count; i++)
            {

                GffField field = fieldList[i];

                string lbl = field.ReadLabel();
                int type = field.Type;

                //Console.WriteLine(_type);

                TreeNode newNode = new TreeNode("" + lbl.ToString() + " Type: Value: ");

                newNode.Text = GetLabel(field.LabelIndex) + " [Type: " + (DataTypes)type + "] Value: " + field.Value + " ";

                newNode.Tag = field;

                //NewNode.Text = (lbl + " [Type: " + ((DataTypes)_type) + "] Value: " + ReadFieldData(_Field.DataOrDataOffset, _type));

                if (type == (int)DataTypes.Struct)
                {
                    Console.WriteLine(ParseIndent(indent) + " -" + lbl + " : STRUCT");
                }
                else if (type == (int)DataTypes.CExoLocString)
                {

                    byte[] complexData = field.ComplexData;

                    Stream stream = new MemoryStream(complexData);

                    BinaryReader cExoLocReader = new BinaryReader(stream);
                    Console.WriteLine("Len: " + cExoLocReader.BaseStream.Length + " Bytes: " + complexData.Length);
                    int stringRef = cExoLocReader.ReadInt32();
                    int stringCount = cExoLocReader.ReadInt32();

                    for (int j = 0; j != stringCount; j++)
                    {

                        int _stringId = cExoLocReader.ReadInt32();
                        int stringLen = cExoLocReader.ReadInt32();

                        byte[] localString = cExoLocReader.ReadBytes(stringLen);

                        int stringId = _stringId;
                        bool feminine = (0x1 & stringId) == 0x1;
                        int languageId = stringId >> 1;

                        string gender = "Male";

                        if (feminine)
                        {
                            gender = "Female";
                        }

                        TreeNode newSubString = new TreeNode("[LOCALSTRING] String Id: " + stringId + " (" + (LanguageIDs)languageId + " " + gender + ") " + System.Text.Encoding.UTF8.GetString(localString));
                        newSubString.Tag = new GffField((int)DataTypes.CExoLocString, 0, null, System.Text.Encoding.UTF8.GetString(localString), 0);
                        newNode.Nodes.Add(newSubString);

                    }

                    //Loop through


                    cExoLocReader.Close();
                    cExoLocReader.Dispose();

                }
                else if (type == (int)DataTypes.List)
                {

                    int[] listStructArray = GetListElements(BitConverter.ToInt32(field.DataOrDataOffset, 0));

                    //Console.WriteLine(ParseIndent(indent) + " -" + lbl + " : LIST #" + listStructArray.Length);

                    if (listStructArray.Length != 0)
                    {

                        int indentH = indent++;

                        for (int j = 0; j != listStructArray.Length; j++)
                        {

                            TreeNode newStruct = new TreeNode("[STRUCT ID: " + listStructArray[j] + "]");
                            newStruct.Tag = new GffField(14, 0, null, "STRUCT", listStructArray[j]);
                            newNode.Nodes.Add(newStruct);

                            GffStruct _struct = _structArray[listStructArray[j]];


                            List<GffField> nfields = GetStructFields(_struct.DataOrDataOffset, _struct.FieldCount);
                            PrintFields(nfields, indentH, newStruct);




                        }

                    }

                }
                else
                {

                    //Console.WriteLine(ParseIndent(indent) + " -" + lbl + " : " + ReadFieldData(_Field.DataOrDataOffset, _type));
                    newNode.Text = lbl + " [Type: " + (DataTypes)type + "] Value: " + ReadFieldData(field.DataOrDataOffset, type);
                }

                node.Nodes.Add(newNode);
            }

        }

        public string ParseIndent(int i)
        {
            string str = "";

            if (i != 0)
            {
                for (int j = 0; j != i + 4; j++)
                {
                    str += " ";
                }
            }

            return str;
        }

        //Gets data from the FieldDataHeader
        public string GetResRef(int offset)
        {

            string resRef = "";

            long originalPos = _reader.BaseStream.Position;//Store the original position of the reader object

            _reader.BaseStream.Position = _header.FieldDataOffset + offset;

            int length = _reader.ReadByte();// Get the length of the string
            if (length != 0)
            {
                resRef = new string(_reader.ReadChars(length));
            }

            _reader.BaseStream.Position = originalPos;//Return the reader position to the original

            return resRef;
        }

        //Gets data from the FieldDataHeader
        public byte[] GetCExoLocString(int offset)
        {

            byte[] bytes;

            long originalPos = _reader.BaseStream.Position;//Store the original position of the reader object

            _reader.BaseStream.Position = _header.FieldDataOffset + offset;

            int length = _reader.ReadInt32();//Total Size of the GetCExoLocString

            bytes = _reader.ReadBytes(length);

            _reader.BaseStream.Position = originalPos;//Return the reader position to the original

            return bytes;
        }

        //Gets data from the FieldDataHeader
        public int GetCExoLocStringRef(int offset)
        {

            int stringRef;

            long originalPos = _reader.BaseStream.Position;//Store the original position of the reader object

            _reader.BaseStream.Position = _header.FieldDataOffset + offset;

            int length = _reader.ReadInt32();//Total Size of the GetCExoLocString

            stringRef = _reader.ReadInt32();

            _reader.BaseStream.Position = originalPos;//Return the reader position to the original

            return stringRef;
        }

        //Gets data from the FieldDataHeader
        public string GetCExoString(int offset)
        {

            string resRef = "";

            long originalPos = _reader.BaseStream.Position;//Store the original position of the reader object

            _reader.BaseStream.Position = _header.FieldDataOffset + offset;

            int length = _reader.ReadInt32();// Get the length of the string
            if (length != 0)
            {
                resRef = new string(_reader.ReadChars(length));
            }

            _reader.BaseStream.Position = originalPos;//Return the reader position to the original

            return resRef;
        }

        //Gets data from the FieldDataHeader
        public string GetDword64(int offset)
        {

            string dword64 = "";

            long originalPos = _reader.BaseStream.Position;//Store the original position of the reader object

            _reader.BaseStream.Position = _header.FieldDataOffset + offset;

            dword64 = _reader.ReadUInt64().ToString();

            _reader.BaseStream.Position = originalPos;//Return the reader position to the original

            return dword64;
        }

        //Gets data from the FieldDataHeader
        public string GetDouble(int offset)
        {
            long originalPos = _reader.BaseStream.Position;//Store the original position of the reader object

            _reader.BaseStream.Position = _header.FieldDataOffset + offset;

            var Double = BitConverter.ToDouble(_reader.ReadBytes(8), 0).ToString();

            _reader.BaseStream.Position = originalPos;//Return the reader position to the original

            return Double;
        }

        public static string GetLabel(int offset)
        {
            return _labelArray[offset];
        }

        public string ReadFieldData(byte[] dataOrDataOffset, int type)
        {
            string val = "";
            switch (type)
            {
                case (int)DataTypes.Byte:
                    val = BitConverter.ToInt32(dataOrDataOffset, 0).ToString();
                    break;
                case (int)DataTypes.CExoLocString:
                    val = GetCExoLocStringRef(BitConverter.ToInt32(dataOrDataOffset, 0)).ToString();
                    break;
                case (int)DataTypes.CExoString:
                    val = GetCExoString(BitConverter.ToInt32(dataOrDataOffset, 0));
                    break;
                case (int)DataTypes.Char:
                    val = BitConverter.ToInt32(dataOrDataOffset, 0).ToString();
                    break;
                case (int)DataTypes.Double:
                    val = GetDouble(BitConverter.ToInt32(dataOrDataOffset, 0)).ToString();
                    break;
                case (int)DataTypes.Dword:
                    val = BitConverter.ToInt32(dataOrDataOffset, 0).ToString();
                    break;
                case (int)DataTypes.Dword64:
                    val = GetDword64(BitConverter.ToInt32(dataOrDataOffset, 0)).ToString();
                    break;
                case (int)DataTypes.Float:
                    val = BitConverter.ToSingle(dataOrDataOffset, 0).ToString();
                    break;
                case (int)DataTypes.Int:
                    val = BitConverter.ToInt32(dataOrDataOffset, 0).ToString();
                    break;
                case (int)DataTypes.Int64:
                    val = BitConverter.ToInt32(dataOrDataOffset, 0).ToString();
                    break;
                case (int)DataTypes.List:
                    val = BitConverter.ToInt32(dataOrDataOffset, 0).ToString(); // Offset in bytes realative to the List Indices Array Start Byte
                    break;
                case (int)DataTypes.ResRef:
                    val = GetResRef(BitConverter.ToInt32(dataOrDataOffset, 0)).ToString();
                    break;
                case (int)DataTypes.Short:
                    val = BitConverter.ToInt32(dataOrDataOffset, 0).ToString();
                    break;
                case (int)DataTypes.Struct:
                    val = BitConverter.ToInt32(dataOrDataOffset, 0).ToString(); //Index of the struct
                    break;
                case (int)DataTypes.Void:
                    val = BitConverter.ToInt32(dataOrDataOffset, 0).ToString();
                    break;
                case (int)DataTypes.Word:
                    val = BitConverter.ToInt32(dataOrDataOffset, 0).ToString();
                    break;
            }

            return val;
        }

        public int[] GetListElements(int offset)
        {

            long originalPos = _reader.BaseStream.Position;//Store the original position of the reader object

            _reader.BaseStream.Position = _header.ListIndicesOffset + offset;

            int listSize = _reader.ReadInt32();//The first 4 bytes indicate the size of the array

            int[] list = new int[listSize];

            for (int i = 0; i != listSize; i++)
            {
                list[i] = _reader.ReadInt32();
            }

            _reader.BaseStream.Position = originalPos;//Return the reader position to the original

            return list;
        }

        public List<GffField> GetStructFields(int offset, int count)
        {
            List<GffField> fields = new List<GffField>();
            if (count > 1)
            {

                long originalPos = _reader.BaseStream.Position;

                Seek(_header.FieldIndicesOffset + offset);



                for (int i = 0; i != count; i++)
                {
                    int faindex = _reader.ReadInt32();

                    fields.Add(_fieldArray[faindex]);

                }

                Seek((int)originalPos);


            }
            else
            {

                fields.Add(_fieldArray[offset]);

            }

            return fields;
        }

        public bool LabelExitist(string label)
        {
            for (int i = 0; i != _labelArray.Count; i++)
            {
                if (_labelArray[i] == label)
                {
                    return true;
                }
            }

            return false;
        }

        public void UpdateLabel(int lblIndex, string value)
        {
            _labelArray[lblIndex] = value.Substring(0, 16);
        }

        public string RetLabel(int index)
        {
            return _labelArray[index];
        }

        public int CreateLabel(string value)
        {
            _labelArray.Add(value.Substring(0, 16));
            int index = _labelArray.IndexOf(value);
            return index;
        }

        public List<GffStruct> GetStructs()
        {
            return _structArray;
        }

        public List<GffField> GetFields()
        {
            return _fieldArray;
        }

        public GffField GetFieldByLabel(String label)
        {

            foreach(GffField field in _fieldArray)
            {
                if(field.ReadLabel() == label){
                    return field;
                }
            }

            return new GffField();

        }

    }
}
