﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using OpenTK;

namespace KotorStudio.Models.AuroraParsers
{
    public interface IMdlObject
    {
        void GetBoundingBox();
        void Read();
        MdlObject.NodeHeader LoadModelNode(long offset);
        void ReadMesh(MdlObject.NodeHeader parent);
        void MoveTo(float x, float y, float z);
        void MoveTo(Vector3 pos);
    }

    public class MdlObject : IMdlObject
    {

        static int _kNodeFlagHasHeader = 0x0001;
        static int _kNodeFlagHasLight = 0x0002;
        static int _kNodeFlagHasEmitter = 0x0004;
        static int _kNodeFlagHasReference = 0x0010;
        static int _kNodeFlagHasMesh = 0x0020;
        static int _kNodeFlagHasSkin = 0x0040;
        static int _kNodeFlagHasAnim = 0x0080;
        static int _kNodeFlagHasDangly = 0x0100;
        static int _kNodeFlagHasAabb = 0x0200;
        static int _kNodeFlagHasSaber = 0x0800; //2081

        static int _kClassFlagEffect = 0x01;
        static int _kClassFlagTile = 0x02;
        static int _kClassFlagCharacter = 0x04;
        static int _kClassFlagDoor = 0x08;
        static int _kClassFlagPlaceable = 0x20;
        static int _kClassFlagOther = 0x00;

        private AuroraFile _mdl;
        private AuroraFile _mdx;
        private BinaryReader _mdlReader;
        private BinaryReader _mdxReader;
        private List<MeshNode> _meshes = new List<MeshNode>();


        private FileHeader _fileHeader;
        private GeometryHeader _geometryHeader;
        private ModelHeader _modelHeader;

        private List<NodeHeader> Nodes = new List<NodeHeader>();

        private string[] _names;


        private Vector3 _position = new Vector3(0f);
        private Vector3 _scale = new Vector3(1.5f);
        private float _rotation = 0f;

        public MdlObject(AuroraFile mdl, AuroraFile mdx)
        {
            _mdl = mdl;
            _mdx = mdx;
        }


        public void GetBoundingBox()
        {
            
            float[] boundingMin = new float[3], boundingMax = new float[3];

            boundingMin[0] = _mdlReader.ReadSingle();
            boundingMin[1] = _mdlReader.ReadSingle();
            boundingMin[2] = _mdlReader.ReadSingle();

            boundingMax[0] = _mdlReader.ReadSingle();
            boundingMax[1] = _mdlReader.ReadSingle();
            boundingMax[2] = _mdlReader.ReadSingle();
            
        }

        public void Read()
        {

            _mdl.Open();
            _mdlReader = _mdl.GetReader();

            _mdx.Open();
            _mdxReader = _mdx.GetReader();

            /*
             * File Header
             */

            _fileHeader.FlagBinary = _mdlReader.ReadUInt32();

            if (_fileHeader.FlagBinary != 0)
                throw new Exception("Unsupported KotOR ASCII MDL");

            _fileHeader.ModelDataSize = _mdlReader.ReadUInt32();
            _fileHeader.RawDataSize = _mdlReader.ReadUInt32();

            _fileHeader.ModelDataOffset = 12;
            _fileHeader.RawDataOffset = _fileHeader.ModelDataOffset + _fileHeader.ModelDataSize;

            /*
             * Geometry Header
             */

            _geometryHeader.Unknown1 = _mdlReader.ReadUInt32(); //4Byte Function pointer
            _geometryHeader.Unknown2 = _mdlReader.ReadUInt32(); //4Byte Function pointer

            _geometryHeader.ModelName = _mdlReader.ReadChars(32);
            _geometryHeader.RootNodeOffset = _mdlReader.ReadUInt32();
            _geometryHeader.NodeCount = _mdlReader.ReadUInt32();
            _geometryHeader.Unknown3 = _mdlReader.ReadBytes(24 + 4); // Unknown + Reference count
            _geometryHeader.GeometryType = _mdlReader.ReadSByte(); //Model Type

            _geometryHeader.Unknown4 = _mdlReader.ReadBytes(3); //Padding

            /*
             * Model Header
             */

            _modelHeader.Unknown1 = _mdlReader.ReadBytes(2); //Unknown
            _modelHeader.Classification = _mdlReader.ReadSByte();
            _modelHeader.Fogged = _mdlReader.ReadSByte();
            _modelHeader.Unknown2 = _mdlReader.ReadBytes(4); //Unkown

            AuroraFile.ReadArrayDef(_mdlReader, out _modelHeader.AnimationDataOffset, out _modelHeader.AnimationsCount);
            _modelHeader.AnimationsAllocated = _modelHeader.AnimationsCount;

            _modelHeader.ParentModelPointer = _mdlReader.ReadUInt32(); // Parent model pointer

            _modelHeader.BoundingMinX = _mdlReader.ReadSingle();
            _modelHeader.BoundingMinY = _mdlReader.ReadSingle();
            _modelHeader.BoundingMinZ = _mdlReader.ReadSingle();
            _modelHeader.BoundingMaxX = _mdlReader.ReadSingle();
            _modelHeader.BoundingMaxY = _mdlReader.ReadSingle();
            _modelHeader.BoundingMaxZ = _mdlReader.ReadSingle();
            _modelHeader.Radius = _mdlReader.ReadSingle();
            _modelHeader.Scale = _mdlReader.ReadSingle();
            _modelHeader.SuperModelName = _mdlReader.ReadChars(32);

            /*
             * Names Array Header
             */

            _mdlReader.BaseStream.Position += 4; // Root node pointer again

            _mdlReader.BaseStream.Position += 12; // Unknown

            UInt32 nameOffset, nameCount;
            AuroraFile.ReadArrayDef(_mdlReader, out nameOffset, out nameCount);

            UInt32[] nameOffsets = new UInt32[nameCount];
            AuroraFile.ReadArray(_mdlReader, _fileHeader.ModelDataOffset + nameOffset, nameCount, ref nameOffsets);

            //System.Diagnostics.Debug.WriteLine("Strings Count: " + nameCount);

            AuroraFile.ReadStrings(_mdlReader, nameOffsets, _fileHeader.ModelDataOffset, out _names);

            /*
             * Animation Header
             */




            //START: TEST - Loading Root Node
            long tmpPos = _mdlReader.BaseStream.Position;
            long nodeOffset = _fileHeader.ModelDataOffset + _geometryHeader.RootNodeOffset;

            LoadModelNode(nodeOffset);

            _mdlReader.BaseStream.Position = tmpPos;
            //END:   TEST - Loading Root Node


            UInt32[] animOffsets = new UInt32[_modelHeader.AnimationsCount];
            AuroraFile.ReadArray(_mdlReader, _fileHeader.ModelDataOffset + _modelHeader.AnimationDataOffset, _modelHeader.AnimationsCount, ref animOffsets);

            for (int i = 0; i!=_modelHeader.AnimationsCount; i++)
            {
                //newState(ctx);
                UInt32 offset = animOffsets[i];
                //readAnim(ctx, fileHeader.ModelDataOffset + offset);

                //addState(ctx);
            }


            Debug.WriteLine("model name: " + new string(_geometryHeader.ModelName));
            Debug.WriteLine("SuperModel name: " + new string(_modelHeader.SuperModelName));

            Debug.WriteLine("Root Node Offset: " + _geometryHeader.RootNodeOffset);
            Debug.WriteLine("Node Count: " + _geometryHeader.NodeCount);

            _mdl.Close();
            _mdx.Close();

        }

        public NodeHeader LoadModelNode(long offset)
        {
            
            NodeHeader node = new NodeHeader();
            Nodes.Add(node);
            _mdlReader.BaseStream.Position = offset;

            node.NodeType = _mdlReader.ReadUInt16();
            node.Supernode = _mdlReader.ReadUInt16();
            node.NodePosition = _mdlReader.ReadUInt16();
            

            if (node.NodePosition < _names.Length)
                node.Name = _names[node.NodePosition];


            // 2 4 4

            node.Unknown = _mdlReader.ReadUInt16();
            node.Unknown2 = _mdlReader.ReadBytes(4);

            node.ParentNodeLoc = _mdlReader.ReadUInt32(); //Parent node pointer

            node.PositionX = _mdlReader.ReadSingle();
            node.PositionY = _mdlReader.ReadSingle();
            node.PositionZ = _mdlReader.ReadSingle();

            node.Position = new Vector3(node.PositionX, node.PositionY, node.PositionZ);

            node.RotationW = (float)Utility.RadianToDegree(Math.Acos(_mdlReader.ReadSingle()) * 2.0f);
            node.RotationX = _mdlReader.ReadSingle();
            node.RotationY = _mdlReader.ReadSingle();
            node.RotationZ = _mdlReader.ReadSingle();

            //Debug.WriteLine(node.RotationW + " : " + node.RotationX + " : " + node.RotationY + " : " + node.RotationZ);

            AuroraFile.ReadArrayDef(_mdlReader, out node.ChildNodesOffset, out node.ChildNodesCount);

            UInt32[] children = new UInt32[node.ChildNodesCount];
            AuroraFile.ReadArray(_mdlReader, _fileHeader.ModelDataOffset + node.ChildNodesOffset, node.ChildNodesCount, ref children);

            AuroraFile.ReadArrayDef(_mdlReader, out node.ControllerKeyOffset, out node.ControllerKeyCount);

            AuroraFile.ReadArrayDef(_mdlReader, out node.ControllerDataOffset, out node.ControllerDataCount);

            float[] controllerData = new float[node.ControllerDataCount];
            AuroraFile.ReadArray(_mdlReader, _fileHeader.ModelDataOffset + node.ControllerDataOffset, node.ControllerDataCount, ref controllerData);

            //readNodeControllers(mdlReader, fileHeader.ModelDataOffset + node.ControllerKeyOffset, node.ControllerKeyCount, controllerData);

            Debug.WriteLine("Name: "+ node.Name+ " +Node Type: " + node.NodeType + " " + _kClassFlagPlaceable);

                //if ((node.NodeType & 0xFC00) != 0)
                //throw new Exception("Unknown node flags " + node.NodeType);

            if ((node.NodeType & _kNodeFlagHasLight) == _kNodeFlagHasLight)
            {
                // TODO: Light
                //ctx.mdl->skip(0x5C);
                _mdlReader.BaseStream.Position += 0x5C;
            }

            if ((node.NodeType & _kNodeFlagHasEmitter) == _kNodeFlagHasEmitter)
            {
                // TODO: Emitter
                //ctx.mdl->skip(0xD8);
                _mdlReader.BaseStream.Position += 0xD8;
            }

            if ((node.NodeType & _kNodeFlagHasReference) == _kNodeFlagHasReference)
            {
                // TODO: Reference
                //ctx.mdl->skip(0x44);
                _mdlReader.BaseStream.Position += 0x44;
            }

            if ((node.NodeType & _kNodeFlagHasMesh) == _kNodeFlagHasMesh)
            {
                ReadMesh(node);
            }

            if ((node.NodeType & _kNodeFlagHasSkin) == _kNodeFlagHasSkin)
            {
                // TODO: Skin
                //ctx.mdl->skip(0x64);
                _mdlReader.BaseStream.Position += 0x64;
            }

            if ((node.NodeType & _kNodeFlagHasAnim) == _kNodeFlagHasAnim)
            {
                // TODO: Anim
                //ctx.mdl->skip(0x38);
                _mdlReader.BaseStream.Position += 0x38;
            }

            if ((node.NodeType & _kNodeFlagHasDangly) == _kNodeFlagHasDangly)
            {
                // TODO: Dangly
                //ctx.mdl->skip(0x18);
                _mdlReader.BaseStream.Position += 0x18;
            }

            if ((node.NodeType & _kNodeFlagHasAabb) == _kNodeFlagHasAabb)
            {
                // TODO: AABB
                //ctx.mdl->skip(0x4);
                _mdlReader.BaseStream.Position += 0x4;
            }

            for (int i = 0; i != children.Length; i++)
            {

                long nodeOffset = _fileHeader.ModelDataOffset + children[i];
                //Debug.WriteLine("Child Offset: " + nodeOffset);
                NodeHeader child = LoadModelNode(nodeOffset);
                child.SetParent(node);

                //child.position = child.position + node.position;
                
                
            }
            return node;
        }

        public void ReadMesh(NodeHeader parent)
        {
            MeshNode mesh = new MeshNode();
            mesh.SetParent(parent);
            
            //mesh.Unknown1 = mdlReader.ReadUInt32(); //Function Pointer
            //mesh.Unknown2 = mdlReader.ReadUInt32(); //Function Pointer

            _mdlReader.BaseStream.Position += 8;

            AuroraFile.ReadArrayDef(_mdlReader, out mesh.FaceArrayOffset, out mesh.FaceArrayCount);
            

            mesh.BoundingBoxMinX = _mdlReader.ReadSingle();
            mesh.BoundingBoxMinY = _mdlReader.ReadSingle();
            mesh.BoundingBoxMinZ = _mdlReader.ReadSingle();

            mesh.BoundingBoxMaxX = _mdlReader.ReadSingle();
            mesh.BoundingBoxMaxY = _mdlReader.ReadSingle();
            mesh.BoundingBoxMaxZ = _mdlReader.ReadSingle();

            mesh.Radius = _mdlReader.ReadSingle();

            mesh.PointsAverageX = _mdlReader.ReadSingle();
            mesh.PointsAverageY = _mdlReader.ReadSingle();
            mesh.PointsAverageZ = _mdlReader.ReadSingle();

            mesh.DiffuseR = _mdlReader.ReadSingle();
            mesh.DiffuseG = _mdlReader.ReadSingle();
            mesh.DiffuseB = _mdlReader.ReadSingle();

            mesh.AmbientR = _mdlReader.ReadSingle();
            mesh.AmbientG = _mdlReader.ReadSingle();
            mesh.AmbientB = _mdlReader.ReadSingle();

            mesh.TransparencyHint = _mdlReader.ReadUInt32();

            bool hasTransparencyHint = true;
            bool transparencyHint = mesh.TransparencyHint != 0;

            mesh.TextureMap1 = _mdlReader.ReadChars(32);
            mesh.TextureMap2 = _mdlReader.ReadChars(32);

            //Debug.WriteLine("Tex1: "+new string(mesh.TextureMap1));
            //Debug.WriteLine("Tex2: " + new string(mesh.TextureMap2));

            _mdlReader.BaseStream.Position += 24;
            _mdlReader.BaseStream.Position += 12; // Vertex Indicies Counts

            AuroraFile.ReadArrayDef(_mdlReader, out mesh.VertexLocArrayOffset, out mesh.VertexLocArrayCount);

            if (mesh.VertexLocArrayCount > 1)
                throw new Exception("Face offsets offsets count wrong "+ mesh.VertexLocArrayCount);

            _mdlReader.BaseStream.Position += 12; // Unknown

            _mdlReader.BaseStream.Position += 24 + 16; // Unknown

            mesh.MdxStructSize = _mdlReader.ReadUInt32();

            _mdlReader.BaseStream.Position += 8; // Unknown

            mesh.MdxVertexNormalsOffset = _mdlReader.ReadUInt32();

            _mdlReader.BaseStream.Position += 4; // Unknown

            //mesh.UV
            UInt32[] offUv = new UInt32[2];
            offUv[0] = _mdlReader.ReadUInt32();
            offUv[1] = _mdlReader.ReadUInt32();

            _mdlReader.BaseStream.Position += 24; // Skip 24 Bytes

            mesh.VerticiesCount = _mdlReader.ReadUInt16();
            mesh.TextureCount = _mdlReader.ReadUInt16();

            _mdlReader.BaseStream.Position += 2; // Skip 2 Bytes

            mesh.FlagShadow = _mdlReader.ReadByte();
            bool shadow = mesh.FlagShadow == 1;
            mesh.FlagRender = _mdlReader.ReadByte();
            bool render = mesh.FlagRender == 1;

            _mdlReader.BaseStream.Position += 10; //Skip 10 Bytes

            if (Global.Game == Global.Games.KOTOR2) //Skipping these bytes will also let placeables work
                _mdlReader.BaseStream.Position += 8; //Skip 8 Bytes

            mesh.MdxNodeDataOffset = _mdlReader.ReadUInt32();
            mesh.VertexCoordinatesOffset = _mdlReader.ReadUInt32();

            //Debug.WriteLine("MDX Offset: " + mesh.MDXNodeDataOffset + " MDX Length: " + mdxReader.BaseStream.Length);

            //Placeable & Some Items MDX Offset Fix
            if (mesh.MdxNodeDataOffset > _mdxReader.BaseStream.Length)
            {
                //mdlReader.BaseStream.Position -= 2;
                //mesh.MDXNodeDataOffset = mdlReader.ReadUInt32();
            }

            //Head Items MDX Offset Fix
            if (mesh.MdxNodeDataOffset > _mdxReader.BaseStream.Length)
            {
                _mdlReader.BaseStream.Position -= 6;
                mesh.MdxNodeDataOffset = _mdlReader.ReadUInt32();
            }

            //Debug.WriteLine("MDX Offset: " + mesh.MDXNodeDataOffset + " MDX Length: " + mdxReader.BaseStream.Length);

            if (mesh.VertexLocArrayCount < 1 || mesh.VerticiesCount == 0 || mesh.FaceArrayCount == 0)
                return;

            long endPos = _mdlReader.BaseStream.Position;

            if (mesh.TextureCount > 2)
            {
                //warning("Model_KotOR::readMesh(): textureCount > 2 (%d)", textureCount);
                mesh.TextureCount = 2;
            }

            String tMap1 = "";
            for(int i = 0; i!=mesh.TextureMap1.Length; i++)
            {
                if( (int)mesh.TextureMap1[i] != 0)
                {
                    tMap1 = tMap1 + mesh.TextureMap1[i];
                }
                else
                {
                    break;
                }
            }

            //  We need to load the texture from file.
            //textureImage = Paloma.TargaImage.LoadTargaImage("I_datapad.tga");
            if (tMap1 != "" && tMap1 != "NULL")
            {
                //Load the texture file here
            }


            //Debug.WriteLine("MDX Size: "+mdxReader.BaseStream.Length);
            mesh.Tvectors = new List<Vector2>();
            for (UInt32 i = 0; i < mesh.VerticiesCount; i++)
            {
                // Position
                _mdxReader.BaseStream.Position = mesh.MdxNodeDataOffset + i * mesh.MdxStructSize;
                mesh.Vectors.Add(new Vector3(_mdxReader.ReadSingle(), _mdxReader.ReadSingle(), _mdxReader.ReadSingle()));


                // Normal
                mesh.Normals.Add(new Vector3(_mdxReader.ReadSingle(), _mdxReader.ReadSingle(), _mdxReader.ReadSingle()));
                
                // TexCoords
                for (UInt16 t = 0; t < 1; t++)
                {
                    if (offUv[t] != 0xFFFFFFFF)
                    {
                        _mdxReader.BaseStream.Position = mesh.MdxNodeDataOffset + i * mesh.MdxStructSize + offUv[t];
                        mesh.Tvectors.Add(new Vector2(_mdxReader.ReadSingle(), _mdxReader.ReadSingle()));
                    }
                    else {
                        //mesh.tvectors.Add(new vec2(0.0f));
                    }
                }
            }

            _mdlReader.BaseStream.Position = _fileHeader.ModelDataOffset + mesh.VertexLocArrayOffset;
            UInt32 offVerts = _mdlReader.ReadUInt32();

            _mdlReader.BaseStream.Position = _fileHeader.ModelDataOffset + offVerts;
            for (UInt32 i = 0; i < mesh.FaceArrayCount * 3; i++)
            {
                UInt16 index = _mdlReader.ReadUInt16();
                mesh.Faces.Add(mesh.Vectors[index]);
                try {
                    mesh.TexCords.Add(mesh.Tvectors[index]);
                    //mesh.Uvs.Add(new Vector2(mesh.Tvectors[index].x, mesh.Tvectors[index].y));
                }catch(Exception ex)
                {

                }
                //Debug.WriteLine(index);
            }
            _meshes.Add(mesh);
        }

        public void MoveTo(float x, float y, float z)
        {
            _position.X = x;
            _position.Y = y;
            _position.Z = z;
        }

        public void MoveTo(Vector3 pos)
        {
            _position = pos;
        }

        private struct FileHeader
        {
            public UInt32 FlagBinary;
            public UInt32 ModelDataSize;
            public UInt32 RawDataSize;
            public UInt32 ModelDataOffset;
            public UInt32 RawDataOffset;
        }

        private struct GeometryHeader
        {
            public long Unknown1;
            public long Unknown2;
            public char[] ModelName;
            public long RootNodeOffset;
            public long NodeCount;
            public byte[] Unknown3; //28Bytes Unknown
            public sbyte GeometryType;
            public byte[] Unknown4; //Padding? 3Bytes
        }

        private struct ModelHeader
        {
            public byte[] Unknown1;
            public sbyte Classification;
            public sbyte Fogged;
            public byte[] Unknown2;
            public UInt32 AnimationDataOffset;
            public UInt32 AnimationsCount;
            public UInt32 AnimationsAllocated;
            public UInt32 ParentModelPointer;
            public float BoundingMinX;
            public float BoundingMinY;
            public float BoundingMinZ;
            public float BoundingMaxX;
            public float BoundingMaxY;
            public float BoundingMaxZ;
            public float Radius;
            public float Scale;
            public char[] SuperModelName; //32Bytes NULL Terminated
        }

        public struct AnimationHeader {
            public long Length;
            public long TransTime;
            public char[] ModelName; //32Chars NULL terminated
            public long EventsOffset;
            public long EventsCount;
            public long EventsAllocated; //Duplicate of Previous
            public byte[] Unknown; //Unkown 4Bytes
        }

        public struct AnimationEvent
        {
            public float ActivationTime; //float
            public char[] Name; //32Chars NULL terminated *CCHARGIN event
        }

        public enum ControllerTypes
        {
            Position = 8,
            Orientation = 20,
            Scale = 36,
            Color = 76,
            Radius = 88,
            ShadowRadius = 96,
            VerticalDisplacement = 100,
            Multiplier = 140,
            AlphaEnd = 80,
            AlphaStart = 84,
            BirthRate = 88,
            BounceCo = 92,
            ColorEnd = 96,
            ColorStart = 108,
            CombineTime = 120,
            Drag = 124,
            Fps = 128,
            FrameEnd = 132,
            FrameStart = 136,
            Grav = 140,
            LifeExp = 144,
            Mass = 148,
            P2PBezier2 = 152,
            P2PBezier3 = 156,
            ParticleRot = 160,
            RandVel = 164,
            SizeStart = 168,
            SizeEnd = 172,
            SizeStartY = 176,
            SizeEndY = 180,
            Spread = 184,
            Threshold = 188,
            Velocity = 192,
            XSize = 196,
            YSize = 200,
            BlurLength = 204,
            LightningDelay = 208,
            LightningRadius = 212,
            LightningScale = 216,
            Detonate = 228,
            AlphaMid = 464,
            ColorMid = 468,
            PercentStart = 480,
            PercentMid = 481,
            PercentEnd = 482,
            SizeMid = 484,
            SizeMidY = 488,
            SelfIllumColor = 100,
            Alpha = 128
        }

        public struct Controller
        {
            public int Type;
            public UInt16 Unknown;
            public UInt16 ControllerDataRowCount;
            public UInt16 TimeKeyOffset;
            public UInt16 DataOffset;
            public char DataColumnCount;
            public byte[] Unknown2; //3Bytes
        }

        //80Bytes Total
        public class NodeHeader
        {
            public UInt16 NodeType; //2Byte Short
            public UInt16 Supernode; //2Byte Short
            public UInt16 NodePosition; //2Byte Short *CCHARGIN Node Number
            public string Name;
            public UInt16 Unknown; //2Byte Unknown
            public byte[] Unknown2; //4Byte Unknown
            public long ParentNodeLoc; //4Byte Long
            public float PositionX;
            public float PositionY;
            public float PositionZ;
            public float RotationW;
            public float RotationX;
            public float RotationY;
            public float RotationZ;

            //Child Nodes Array Pointers
            public UInt32 ChildNodesOffset; //4Byte Long
            public UInt32 ChildNodesCount; //4Byte Long
            public UInt32 ChildNodesAllocated; //4Byte Long //Duplicate of Previous

            //Node Controllers Array Pointers
            public UInt32 ControllerKeyOffset; //4Byte Long
            public UInt32 ControllerKeyCount; //4Byte Long
            public UInt32 ControllerKeyAllocated; //4Byte Long //Duplicate of Previous

            //Node Controllers Data Array Pointers
            public UInt32 ControllerDataOffset; //4Byte Long
            public UInt32 ControllerDataCount; //4Byte Long
            public UInt32 ControllerDataAllocated; //4Byte Long //Duplicate of Previous

            public Vector3 Position;

            private NodeHeader Parent;

            internal void SetParent(NodeHeader node)
            {
                Parent = node;
            }
        }


        private class MeshNode
        {
            public long Unknown1;
            public long Unknown2;
            public UInt32 FaceArrayOffset;
            public UInt32 FaceArrayCount;
            public UInt32 FaceArrayAllocated;
            public float BoundingBoxMinX;
            public float BoundingBoxMinY;
            public float BoundingBoxMinZ;
            public float BoundingBoxMaxX;
            public float BoundingBoxMaxY;
            public float BoundingBoxMaxZ;
            public float Radius;
            public float PointsAverageX;
            public float PointsAverageY;
            public float PointsAverageZ;
            public float DiffuseR;
            public float DiffuseG;
            public float DiffuseB;
            public float AmbientR;
            public float AmbientG;
            public float AmbientB;
            public UInt32 TransparencyHint;
            public char[] TextureMap1;
            public char[] TextureMap2;
            public byte[] Unknown4;         //24Bytes
            public UInt32 VertexNumArrayOffset;
            public UInt32 VertexNumArrayCount;
            public UInt32 VertexNumArrayAllocated;
            public UInt32 VertexLocArrayOffset;
            public UInt32 VertexLocArrayCount;
            public UInt32 VertexLocArrayAllocated;
            public UInt32 UnknownArrayOffset;
            public UInt32 UnknownArrayCount;
            public UInt32 UnknownArrayAllocated;
            public long Unknown5;
            public long Unknown6;
            public long Unknown7;
            public long Unknown8;
            public long Unknown9;
            public long Unknown10;
            public byte[] Unkown11; //16Bytes
            public long MdxStructSize;
            public long Unknown12;//unknown (has something to do with textures)
            public long Unknown13; //Always 0 (NULL maybe?)
            public long MdxVertexNormalsOffset; //Always 12
            public long Unknown14; //Always -1
            public long MdxuvCoordinatesOffset;
            public byte[] Unkown15; //28Bytes - unknown (each value always -1)
            public UInt16 VerticiesCount;
            public UInt16 TextureCount;
            public short Unknown16;
            public short FlagShadow; //(value of 256 = cast shadow)
            public short FlagRender; //(value of 256 = render this node)
            public short Unknown17;
            public long Unknown18; //8Bytes
            public long MdxNodeDataOffset;
            public long VertexCoordinatesOffset;

            public List<Vector3> Vectors = new List<Vector3>();
            public List<Vector3> Normals = new List<Vector3>();
            public List<Vector2> Tvectors = new List<Vector2>();
            public List<Vector3> Faces = new List<Vector3>();
            public List<Vector2> TexCords = new List<Vector2>();
            public List<Vector2> Uvs = new List<Vector2>();

            //  Storage the texture itself.
            //public Bitmap textureImage;
            private NodeHeader Parent;

            public void SetParent(NodeHeader p)
            {
                Parent = p;
            }

        }


    }
}
