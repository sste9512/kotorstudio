﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using KotorStudio.Models.AuroraInterfaces;

namespace KotorStudio.Models.AuroraParsers
{
    class _2DaObject : I_2daObject
    {

        private AuroraFile _file;
        private BinaryReader _reader;

        private DataTable _data;

        public _2DaObject(AuroraFile file)
        {
            _file = file;
            _data = new DataTable();
        }

        public void Read()
        {

            _file.Open();
            _reader = _file.GetReader();

            char[] fileType = _reader.ReadChars(4);
            char[] fileVersion = _reader.ReadChars(4);

            _reader.BaseStream.Position += 1; //10 = Newline (Skip)

            char[] delimiterChars = { ' ', '\t' };

            string str = "";
            char ch;
            while ((ch = _reader.ReadChar()) != 0)
                str = str + ch;
            DataColumn column = new DataColumn();
            //DataRow row;
            column.DataType = Type.GetType("System.Int32");
            column.ColumnName = "(Row Label)";
            column.ReadOnly = true;
            column.Unique = true;
            // Add the Column to the DataColumnCollection.
            _data.Columns.Add(column);
            string[] columns = str.Split(delimiterChars);
            foreach (string name in columns)
            {
                column = new DataColumn
                {
                    DataType = Type.GetType("System.String"), ColumnName = name, ReadOnly = true, Unique = false
                };
                // Add the Column to the DataColumnCollection.
                _data.Columns.Add(column);
            }

            UInt32 columnCount = (UInt32)columns.Length - 1;
            UInt32 rowCount = _reader.ReadUInt32();

            string[] rows = new string[rowCount];

            for (int i = 0; i!=rowCount; i++)
            {
                string rowIndex = "";
                char c;

                while ((c = _reader.ReadChar()) != 9)
                    rowIndex = rowIndex + c;

                rows[i] = rowIndex;
            }

            List<int> dataOffsets = new List<int>();
            UInt32 cellCount = columnCount * rowCount;
            uint[] offsets = new uint[cellCount];

            for (int i = 0; i < cellCount; i++)
            {
                offsets[i] = _reader.ReadUInt16();
            }

            _reader.BaseStream.Position += 2;
            uint dataOffset = (uint)_reader.BaseStream.Position;

            for (int i = 0; i < rowCount; i++)
            {

                DataRow row = _data.NewRow();
                row["(Row Label)"] = i;

                for (int j = 0; j < columnCount; j++)
                {
                    uint offset = dataOffset + offsets[i * columnCount + j];

                    try
                    {
                        _reader.BaseStream.Position = offset;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception();
                    }

                    string token = "";
                    char c;

                    while((c = _reader.ReadChar()) != 0)
                        token = token + c;

                    if(token == "") 
                        token = "****";
                    
                    Debug.WriteLine(columns[j] + " : " + token);
                    row[columns[j]] = token;
                }

                _data.Rows.Add(row);

            }

            _file.Close();

        }

        public DataTable GetTable()
        {
            return _data;
        }

        public DataRowCollection GetRows()
        {
            return _data.Rows;
        }

    }
}
