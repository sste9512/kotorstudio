﻿using System.IO;

namespace KotorStudio.Models.AuroraParsers
{
    public class AreObject : IAreObject
    {
        AuroraFile _file;
        BinaryReader _reader;

        public AreObject(AuroraFile file)
        {
            _file = file;
        }

        public void Read()
        {
            _file.Open();
            _reader = _file.GetReader();
            _file.Close();
        }

    }
}
