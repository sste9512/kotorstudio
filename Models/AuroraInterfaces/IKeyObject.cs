﻿using System;
using KotorStudio.Models.AuroraParsers;

namespace KotorStudio.Models.AuroraInterfaces
{
    public interface IKeyObject
    {
        void Read();
        KeyObject.KeyTable FindFileKey(String resRef, UInt32 resourceType);
    }
}