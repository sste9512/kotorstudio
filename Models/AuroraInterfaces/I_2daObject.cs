﻿using System.Data;

namespace KotorStudio.Models.AuroraInterfaces
{
    internal interface I_2daObject
    {
        void Read();
        DataTable GetTable();
        DataRowCollection GetRows();
    }
}