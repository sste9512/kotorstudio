using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KotorStudio.Models.AuroraParsers;

namespace KotorStudio.Models.AuroraInterfaces
{
    public interface IErfObject
    {
        void Open();
        void Seek(int bytes);
        AuroraFile GetFile();
        void Read();
        ErfObject.ErfHeader ReadHeader();
        List<ErfObject.ErfKey> GetKeys();
        List<ErfObject.ErfResource> GetResources();
        byte[] GetRawResource(ErfObject.ErfResource resource);
        Task<byte[]> GetRawResourceAsync(ErfObject.ErfResource resource);
        void WriteToFile(string fileName, byte[] rawResourceBytes);
        ErfObject.ErfResource GetResourceByKey(String key, int restype);
    }
}