using System;
using System.Collections.Generic;
using System.Windows.Forms;
using KotorStudio.Models.AuroraParsers;

namespace KotorStudio.Models.AuroraInterfaces
{
    public interface IGffObject
    {
        void Open();
        string GetFileType();
        string GetFileVersion();
        void Seek(int bytes);
        void Read();
        void SaveGff();
        GffObject.GffHeader ReadHeader();
        void PrintableData();
        void PrintFields(List<GffObject.GffField> fieldList, int indent, TreeNode node);
        string ParseIndent(int i);
        string GetResRef(int offset);
        byte[] GetCExoLocString(int offset);
        int GetCExoLocStringRef(int offset);
        string GetCExoString(int offset);
        string GetDword64(int offset);
        string GetDouble(int offset);
        string ReadFieldData(byte[] dataOrDataOffset, int type);
        int[] GetListElements(int offset);
        List<GffObject.GffField> GetStructFields(int offset, int count);
        bool LabelExitist(string label);
        void UpdateLabel(int lblIndex, string value);
        string RetLabel(int index);
        int CreateLabel(string value);
        List<GffObject.GffStruct> GetStructs();
        List<GffObject.GffField> GetFields();
        GffObject.GffField GetFieldByLabel(String label);
    }

}