﻿using System;
using System.Collections.Generic;
using KotorStudio.Models.AuroraParsers;

namespace KotorStudio.Models.AuroraInterfaces
{
    public interface IBiffObject
    {
        void Read();
        BiffObject.VResourceHeader FindFileById(UInt32 id);
        AuroraFile ExtractVResource(BiffObject.VResourceHeader fileHeader, KeyObject.KeyTable key);
        AuroraFile ExtractFile(String filename, UInt32 restype);
        String GetFilename();
        List<BiffObject.VResourceHeader> GetResources();
    }
}