﻿using KotorStudio.Models.AuroraParsers;

namespace KotorStudio.Models.AuroraInterfaces
{
    public interface IWaveObject
    {
        byte[] GetPlayableByteStream();
        new WaveObject.AudioType GetType();
    }
}