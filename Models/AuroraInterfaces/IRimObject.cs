﻿using System;
using System.Collections.Generic;
using KotorStudio.Models.AuroraParsers;

namespace KotorStudio.Models.AuroraInterfaces
{
    public interface IRimObject
    {
        void Read();
        byte[] GetRawResource(RimObject.RimKey key);
        RimObject.RimKey GetResourceByKey(String key, int restype);
        AuroraFile GetFile();
        List<RimObject.RimKey> GetRimKeyList();
    }
}