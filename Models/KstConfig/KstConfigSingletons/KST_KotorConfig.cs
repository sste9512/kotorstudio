﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace KotorStudio.Models.KstConfig.KstConfigSingletons
{
    public class KstKotorConfig
    {
        private KstKotorConfig()
        {
        }

        public string KotorPath { get; set; }
        public string Kotor2Path { get; set; }
        public string DefaultProjectPath { get; set; }
        public bool NeedsPathSetup { get; }
        public static KstKotorConfig Instance => _instance ?? (_instance = new KstKotorConfig());
        private static KstKotorConfig _instance = new KstKotorConfig();


        public async void SaveAllToConfigFile()
        {
            await Task.Factory.StartNew(() =>
            {
                using (var sw = new StreamWriter(DefaultProjectPath))
                {
                    sw.WriteLine("KotorPath = " + KotorPath);
                    sw.WriteLine("KotorPath = " + Kotor2Path);
                    sw.WriteLine("DefaultProjectPath = " + DefaultProjectPath);
                }
            });
        }

        public async void SaveAllToConfigFile(Action action)
        {
            await Task.Factory.StartNew(() =>
            {
                using (var sw = new StreamWriter(DefaultProjectPath))
                {
                    sw.WriteLine("KotorPath = " + KotorPath);
                    sw.WriteLine("KotorPath = " + Kotor2Path);
                    sw.WriteLine("DefaultProjectPath = " + DefaultProjectPath);
                }
            });
        }


        public async void RetrieveAllSettingsFromConfig()
        {
            await Task.Factory.StartNew(() =>
            {
                if (!File.Exists(DefaultProjectPath)) return;
                var config = File.ReadAllLines(DefaultProjectPath);
                KotorPath = config[0].Split('=')[1].Trim();
                Kotor2Path = config[1].Split('=')[1].Trim();
                DefaultProjectPath = config[2].Split('=')[1].Trim();
            });
        }
    }
}