using System;
using System.Diagnostics;

namespace XoreosToolController
{
    public static class UnerfArguments
    {
        public const string DisplayMeta = "i";
        public const string ListArchiveContents = "l";
        public const string ListArchiveContentsWithPaths = "v";
        public const string ExtractFilesToCurrentDir = "e";
        public const string ExtractFilesToCurrentDirWFullName = "s";
    }
    
    //unerf i Shadowlords1.mod
    
    
    public class Unerf : ProcCommand
    {
        public Unerf(string exeLocation) : base(exeLocation)
        {
        }

        public Unerf(string exeLocation, Func<object, DataReceivedEventArgs> dataReceivedHandler,
                                         Func<object, DataReceivedEventArgs> errorReceivedHandler) : base(exeLocation)
        {
            _dataReceivedHandler = dataReceivedHandler;
            _errorReceivedHandler = errorReceivedHandler;
        }

        protected override void DataReceived(object sendingProcess, DataReceivedEventArgs e)
        {
            _dataReceivedHandler?.Invoke(sendingProcess);
        }

        protected override void ErrorReceived(object sendingProcess, DataReceivedEventArgs e)
        {
            if (_dataReceivedHandler != null)
            {
                _errorReceivedHandler.Invoke(sendingProcess);
            }
        }


        public void ExtractMeta()
        {
            
        }

        public void ListAllFilesInArchive(string pathToErf)
        {
            
        }

        public void ListAllFilesWithFullPath(string pathToErf)
        {
            
        }

        public void ExtractAllFilesWithFullPath(string pathToErf)
        {
            string command = "unerf s " + pathToErf;
            ExecuteWithArgs(command);
        }

        public void ExtractFileAreasFromArchive()
        {
            
        }
    } 
}