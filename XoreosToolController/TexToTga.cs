using System;
using System.Diagnostics;

namespace XoreosToolController
{
    public static class TexToTgaArguments
    {
        const string flip = "--flip";
        const string auto = "--auto";
        const string dds = "--dds";
        const string sbm = "--sbm";
        const string tpc = "--tpc";
        const string txb = "--txb";
        const string tga = "--tga";
    }


    public class TexToTga : ProcCommand
    {
        public TexToTga(string exeLocation) : base(exeLocation)
        {
        }

        public TexToTga(string exeLocation, Func<object, DataReceivedEventArgs> dataReceivedHandler,
            Func<object, DataReceivedEventArgs> errorReceivedHandler) : base(exeLocation)
        {
            _dataReceivedHandler = dataReceivedHandler;
            _errorReceivedHandler = errorReceivedHandler;
        }

        protected override void DataReceived(object sendingProcess, DataReceivedEventArgs e)
        {
            _dataReceivedHandler.Invoke(sendingProcess);
        }

        protected override void ErrorReceived(object sendingProcess, DataReceivedEventArgs e)
        {
            _errorReceivedHandler.Invoke(sendingProcess);
        }
    }
}