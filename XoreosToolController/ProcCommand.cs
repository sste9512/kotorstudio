using System;
using System.Diagnostics;
using System.Text;

namespace XoreosToolController
{
    public abstract class ProcCommand
    {
        
        private string _exeLocation { get; }

        protected Func<object, DataReceivedEventArgs> _dataReceivedHandler;
        protected Func<object, DataReceivedEventArgs> _errorReceivedHandler;

        protected abstract void DataReceived(object sendingProcess, DataReceivedEventArgs e);
        protected abstract void ErrorReceived(object sendingProcess, DataReceivedEventArgs e);

        protected ProcCommand(string exeLocation)
        {
            _exeLocation = exeLocation;
        }

        public void Execute()
        {
            //* Create your Process
            Process process = new Process();
            process.StartInfo.FileName = _exeLocation;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.Arguments = "/c DIR";
            //* Set your output and error (asynchronous) handlers
            process.OutputDataReceived += DataReceived;
            process.ErrorDataReceived += ErrorReceived;
            //* Start process and handlers
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();
        }

        public void ExecuteWithArgs(string argument)
        {
            //* Create your Process
            Process process = new Process();
            process.StartInfo.FileName = _exeLocation;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.Arguments = argument;
            //* Set your output and error (asynchronous) handlers
            process.OutputDataReceived += DataReceived;
            process.ErrorDataReceived += ErrorReceived;
            //* Start process and handlers
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();
        }
    }
}