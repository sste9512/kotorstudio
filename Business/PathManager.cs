using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using OpenTK.Graphics.ES11;

namespace KotorStudio.Business
{
    public class PathManager : IPathManager
    {
        public string BaseDirectory { get; set; }

        private IEnumerable<FileInfo> _directoryFiles;
        private IEnumerable<FileInfo> _generatedProjectFiles;

        public PathManager()
        {
            _directoryFiles = new List<FileInfo>();
            _generatedProjectFiles = new List<FileInfo>();
            BaseDirectory = @"C:\ResourceHolder";
        }

        public PathManager SetBaseDirectory(Func<string,string> setBase)
        {
            setBase.Invoke(BaseDirectory);
            return this;
        }
        
        public string ReadFromConfigFile(FileInfo file)
        {
            return "something";
        }

        private bool GenerateDirectoryForResource(string directoryName)
        {
            string newResourceDirectory = directoryName;
            if (!Directory.Exists(newResourceDirectory))
            {
                Directory.CreateDirectory(newResourceDirectory);
                return true;
            }
            return false;
        }

        public void MapDirectoryFilesToResourceList<T>(IEnumerable<T> resourceList, Action<T> onWrite, Action onError, Action onCompleted)
        {
            foreach (var item in resourceList)
            {
                onWrite.Invoke(item);
                if (!GenerateDirectoryForResource(BaseDirectory + "\\" + item))
                {
                    onError.Invoke();
                    break;
                }
            }
            onCompleted.Invoke();
        }
    }
}