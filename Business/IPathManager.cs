using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace KotorStudio.Business
{
    public interface IPathManager
    {
        string ReadFromConfigFile(FileInfo file);
    }
}