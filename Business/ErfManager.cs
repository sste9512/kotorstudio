using System.Collections.Generic;
using KotorStudio.Models.AuroraParsers;
using KotorStudio.Models.LegacyERF;
using KotorStudio.Persistence;

namespace KotorStudio.Business
{
    public class ErfManager : IErfProvider
    {
        private ResourcePersistence<ErfObject> _persistence;
        private PathManager _pathManager;
      
        private readonly List<ErfKeyEntry> _initialList;
        private readonly List<ErfObject.ErfKey> _keyList;
        private readonly List<ErfObject.ErfResource> _resourceList;

        private readonly ErfObject _erfObject;
        
        public ErfManager()
        {
            
        }
        

    }
}