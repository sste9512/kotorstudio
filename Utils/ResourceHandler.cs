﻿using Microsoft.VisualBasic.CompilerServices;

namespace KotorStudio.Utils
{
    internal static class ResourceHandler
    {
        public static short GetIdForRsrcType(string type)
        {
            var sLeft = type;
            if (StringType.StrCmp(sLeft, "res", false) == 0)
                return 0;
            if (StringType.StrCmp(sLeft, "bmp", false) == 0)
                return 1;
            if (StringType.StrCmp(sLeft, "mve", false) == 0)
                return 2;
            if (StringType.StrCmp(sLeft, "tga", false) == 0)
                return 3;
            if (StringType.StrCmp(sLeft, "wav", false) == 0)
                return 4;
            if (StringType.StrCmp(sLeft, "plt", false) == 0)
                return 6;
            if (StringType.StrCmp(sLeft, "ini", false) == 0)
                return 7;
            if (StringType.StrCmp(sLeft, "mp3", false) == 0)
                return 8;
            if (StringType.StrCmp(sLeft, "mpg", false) == 0)
                return 9;
            if (StringType.StrCmp(sLeft, "txt", false) == 0)
                return 10;
            if (StringType.StrCmp(sLeft, "wma", false) == 0)
                return 11;
            if (StringType.StrCmp(sLeft, "wmv", false) == 0)
                return 12;
            if (StringType.StrCmp(sLeft, "xmv", false) == 0)
                return 13;
            if (StringType.StrCmp(sLeft, "plh", false) == 0)
                return 2000;
            if (StringType.StrCmp(sLeft, "tex", false) == 0)
                return 2001;
            if (StringType.StrCmp(sLeft, "mdl", false) == 0)
                return 2002;
            if (StringType.StrCmp(sLeft, "thg", false) == 0)
                return 2003;
            if (StringType.StrCmp(sLeft, "fnt", false) == 0)
                return 2005;
            if (StringType.StrCmp(sLeft, "lua", false) == 0)
                return 2007;
            if (StringType.StrCmp(sLeft, "slt", false) == 0)
                return 2008;
            if (StringType.StrCmp(sLeft, "nss", false) == 0)
                return 2009;
            if (StringType.StrCmp(sLeft, "ncs", false) == 0)
                return 2010;
            if (StringType.StrCmp(sLeft, "mod", false) == 0)
                return 2011;
            if (StringType.StrCmp(sLeft, "are", false) == 0)
                return 2012;
            if (StringType.StrCmp(sLeft, "set", false) == 0)
                return 2013;
            if (StringType.StrCmp(sLeft, "ifo", false) == 0)
                return 2014;
            if (StringType.StrCmp(sLeft, "bic", false) == 0)
                return 2015;
            if (StringType.StrCmp(sLeft, "wok", false) == 0)
                return 2016;
            if (StringType.StrCmp(sLeft, "2da", false) == 0)
                return 2017;
            if (StringType.StrCmp(sLeft, "tlk", false) == 0)
                return 2018;
            if (StringType.StrCmp(sLeft, "txi", false) == 0)
                return 2022;
            if (StringType.StrCmp(sLeft, "git", false) == 0)
                return 2023;
            if (StringType.StrCmp(sLeft, "bti", false) == 0)
                return 2024;
            if (StringType.StrCmp(sLeft, "uti", false) == 0)
                return 2025;
            if (StringType.StrCmp(sLeft, "btc", false) == 0)
                return 2026;
            if (StringType.StrCmp(sLeft, "utc", false) == 0)
                return 2027;
            if (StringType.StrCmp(sLeft, "dlg", false) == 0)
                return 2029;
            if (StringType.StrCmp(sLeft, "itp", false) == 0)
                return 2030;
            if (StringType.StrCmp(sLeft, "btt", false) == 0)
                return 2031;
            if (StringType.StrCmp(sLeft, "utt", false) == 0)
                return 2032;
            if (StringType.StrCmp(sLeft, "dds", false) == 0)
                return 2033;
            if (StringType.StrCmp(sLeft, "bts", false) == 0)
                return 2034;
            if (StringType.StrCmp(sLeft, "uts", false) == 0)
                return 2035;
            if (StringType.StrCmp(sLeft, "ltr", false) == 0)
                return 2036;
            if (StringType.StrCmp(sLeft, "gff", false) == 0)
                return 2037;
            if (StringType.StrCmp(sLeft, "fac", false) == 0)
                return 2038;
            if (StringType.StrCmp(sLeft, "bts", false) == 0)
                return 2039;
            if (StringType.StrCmp(sLeft, "ute", false) == 0)
                return 2040;
            if (StringType.StrCmp(sLeft, "btd", false) == 0)
                return 2041;
            if (StringType.StrCmp(sLeft, "utd", false) == 0)
                return 2042;
            if (StringType.StrCmp(sLeft, "btp", false) == 0)
                return 2043;
            if (StringType.StrCmp(sLeft, "utp", false) == 0)
                return 2044;
            if (StringType.StrCmp(sLeft, "dft", false) == 0)
                return 2045;
            if (StringType.StrCmp(sLeft, "gic", false) == 0)
                return 2046;
            if (StringType.StrCmp(sLeft, "gui", false) == 0)
                return 2047;
            if (StringType.StrCmp(sLeft, "css", false) == 0)
                return 2048;
            if (StringType.StrCmp(sLeft, "ccs", false) == 0)
                return 2049;
            if (StringType.StrCmp(sLeft, "btm", false) == 0)
                return 2050;
            if (StringType.StrCmp(sLeft, "utm", false) == 0)
                return 2051;
            if (StringType.StrCmp(sLeft, "dwk", false) == 0)
                return 2052;
            if (StringType.StrCmp(sLeft, "pwk", false) == 0)
                return 2053;
            if (StringType.StrCmp(sLeft, "btg", false) == 0)
                return 2054;
            if (StringType.StrCmp(sLeft, "utg", false) == 0)
                return 2055;
            if (StringType.StrCmp(sLeft, "jrl", false) == 0)
                return 2056;
            if (StringType.StrCmp(sLeft, "sav", false) == 0)
                return 2057;
            if (StringType.StrCmp(sLeft, "utw", false) == 0)
                return 2058;
            if (StringType.StrCmp(sLeft, "4pc", false) == 0)
                return 2059;
            if (StringType.StrCmp(sLeft, "ssf", false) == 0)
                return 2060;
            if (StringType.StrCmp(sLeft, "hak", false) == 0)
                return 2061;
            if (StringType.StrCmp(sLeft, "nwm", false) == 0)
                return 2062;
            if (StringType.StrCmp(sLeft, "bik", false) == 0)
                return 2063;
            if (StringType.StrCmp(sLeft, "ndb", false) == 0)
                return 2064;
            if (StringType.StrCmp(sLeft, "ptm", false) == 0)
                return 2065;
            if (StringType.StrCmp(sLeft, "ptt", false) == 0)
                return 2066;
            if (StringType.StrCmp(sLeft, "lyt", false) == 0)
                return 3000;
            if (StringType.StrCmp(sLeft, "vis", false) == 0)
                return 3001;
            if (StringType.StrCmp(sLeft, "rim", false) == 0)
                return 3002;
            if (StringType.StrCmp(sLeft, "pth", false) == 0)
                return 3003;
            if (StringType.StrCmp(sLeft, "lip", false) == 0)
                return 3004;
            if (StringType.StrCmp(sLeft, "bwm", false) == 0)
                return 3005;
            if (StringType.StrCmp(sLeft, "txb", false) == 0)
                return 3006;
            if (StringType.StrCmp(sLeft, "tpc", false) == 0)
                return 3007;
            if (StringType.StrCmp(sLeft, "mdx", false) == 0)
                return 3008;
            if (StringType.StrCmp(sLeft, "rsv", false) == 0)
                return 3009;
            if (StringType.StrCmp(sLeft, "sig", false) == 0)
                return 3010;
            if (StringType.StrCmp(sLeft, "xbx", false) == 0)
                return 3011;
            if (StringType.StrCmp(sLeft, "erf", false) == 0)
                return 9997;
            if (StringType.StrCmp(sLeft, "bif", false) == 0)
                return 9998;
            return StringType.StrCmp(sLeft, "key", false) == 0 ? (short) 9999 : (short) -1;
        }


        /*
         * Convienence Method for identifying game resources
         * Takes the Resource Id of the Model Classes
         */
        public static string GetRsrcTypeForId(int resourceId)
        {
            switch (resourceId)
            {
                case 0:
                    return "res";
                case 1:
                    return "bmp";
                case 2:
                    return "mve";
                case 3:
                    return "tga";
                case 4:
                    return "wav";
                case 6:
                    return "plt";
                case 7:
                    return "ini";
                case 8:
                    return "mp3";
                case 9:
                    return "mpg";
                case 10:
                    return "txt";
                case 11:
                    return "wma";
                case 12:
                    return "wmv";
                case 13:
                    return "xmv";
                case 2000:
                    return "plh";
                case 2001:
                    return "tex";
                case 2002:
                    return "mdl";
                case 2003:
                    return "thg";
                case 2005:
                    return "fnt";
                case 2007:
                    return "lua";
                case 2008:
                    return "slt";
                case 2009:
                    return "nss";
                case 2010:
                    return "ncs";
                case 2011:
                    return "mod";
                case 2012:
                    return "are";
                case 2013:
                    return "set";
                case 2014:
                    return "ifo";
                case 2015:
                    return "bic";
                case 2016:
                    return "wok";
                case 2017:
                    return "2da";
                case 2018:
                    return "tlk";
                case 2022:
                    return "txi";
                case 2023:
                    return "git";
                case 2024:
                    return "bti";
                case 2025:
                    return "uti";
                case 2026:
                    return "btc";
                case 2027:
                    return "utc";
                case 2029:
                    return "dlg";
                case 2030:
                    return "itp";
                case 2031:
                    return "btt";
                case 2032:
                    return "utt";
                case 2033:
                    return "dds";
                case 2034:
                    return "bts";
                case 2035:
                    return "uts";
                case 2036:
                    return "ltr";
                case 2037:
                    return "gff";
                case 2038:
                    return "fac";
                case 2039:
                    return "bts";
                case 2040:
                    return "ute";
                case 2041:
                    return "btd";
                case 2042:
                    return "utd";
                case 2043:
                    return "btp";
                case 2044:
                    return "utp";
                case 2045:
                    return "dft";
                case 2046:
                    return "gic";
                case 2047:
                    return "gui";
                case 2048:
                    return "css";
                case 2049:
                    return "ccs";
                case 2050:
                    return "btm";
                case 2051:
                    return "utm";
                case 2052:
                    return "dwk";
                case 2053:
                    return "pwk";
                case 2054:
                    return "btg";
                case 2055:
                    return "utg";
                case 2056:
                    return "jrl";
                case 2057:
                    return "sav";
                case 2058:
                    return "utw";
                case 2059:
                    return "4pc";
                case 2060:
                    return "ssf";
                case 2061:
                    return "hak";
                case 2062:
                    return "nwm";
                case 2063:
                    return "bik";
                case 2064:
                    return "ndb";
                case 2065:
                    return "ptm";
                case 2066:
                    return "ptt";
                case 3000:
                    return "lyt";
                case 3001:
                    return "vis";
                case 3002:
                    return "rim";
                case 3003:
                    return "pth";
                case 3004:
                    return "lip";
                case 3005:
                    return "bwm";
                case 3006:
                    return "txb";
                case 3007:
                    return "tpc";
                case 3008:
                    return "mdx";
                case 3009:
                    return "rsv";
                case 3010:
                    return "sig";
                case 3011:
                    return "xbx";
                case 9997:
                    return "erf";
                case 9998:
                    return "bif";
                case 9999:
                    return "key";
                default:
                    return "Unk (" + StringType.FromInteger(resourceId) + ")";
            }
        }


        private static string ParentDescForResType(int resType)
        {
            switch (resType)
            {
                case 3:
                    return "TGA Images";
                case 4:
                    return "WAV Files";
                case 2002:
                    return "Aurora Model";
                case 2009:
                    return "Script, Source";
                case 2010:
                    return "Script, Compiled";
                case 2011:
                    return "Module";
                case 2012:
                    return "Static Area Info";
                case 2014:
                    return "Module Info File";
                case 2016:
                    return "Walkmesh";
                case 2017:
                    return "2D Array";
                case 2022:
                    return "Extra Texture info";
                case 2023:
                    return "Dynamic Area Info";
                case 2025:
                    return "Blueprint, Item";
                case 2027:
                    return "Blueprint, Character";
                case 2029:
                    return "Dialog";
                case 2030:
                    return "Blueprint Palette File";
                case 2032:
                    return "Blueprint, Triggers";
                case 2035:
                    return "Blueprint, Sound";
                case 2036:
                    return "Letter-combo Probability File";
                case 2038:
                    return "Faction File";
                case 2040:
                    return "Blueprint, Encounter";
                case 2042:
                    return "Blueprint, Doors";
                case 2044:
                    return "Blueprint, Placeables";
                case 2047:
                    return "GUI Elements";
                case 2051:
                    return "Blueprint, Merchant";
                case 2052:
                    return "Door Walkmesh";
                case 2053:
                    return "Placeable Object Walkmesh";
                case 2055:
                    return "Blueprint, ";
                case 2056:
                    return "Journal File";
                case 2058:
                    return "Blueprint, Waypoint";
                case 2060:
                    return "Sound Sets";
                case 3000:
                    return "Layouts";
                case 3003:
                    return "Paths";
                case 3007:
                    return "PC Textures";
                case 3008:
                    return "Aurora Model Extension";
                default:
                    return GetRsrcTypeForId(resType);
            }
        }
    }
}