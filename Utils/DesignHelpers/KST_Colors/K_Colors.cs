﻿using System.Windows.Media;

namespace KotorStudio.Utils.DesignHelpers.KST_Colors
{
    //Design Convenience Colors
    public static class KColors
    {
        public static readonly SolidColorBrush FlatLightBlack =
            new SolidColorBrush((Color) ColorConverter.ConvertFromString(value: "#FF323131"));
    }
}