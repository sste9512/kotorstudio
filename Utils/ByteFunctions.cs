﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace KotorStudio.Utils
{
    internal static class ByteFunctions
    {
        //  Note: for custom classes add[Serializable] attribute to enable serialization

        public static byte[] ObjectToByteArray(object obj)
        {
            if (obj == null)
                return null;
            var bf = new BinaryFormatter();
            var ms = new MemoryStream();
            bf.Serialize(ms, obj);
            ms.Close();
            return ms.ToArray();
        }

        public static object ByteArrayToObject(byte[] arrBytes)
        {
            var memStream = new MemoryStream();
            var binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = binForm.Deserialize(memStream);
            memStream.Close();
            return obj;
        }
    }
}