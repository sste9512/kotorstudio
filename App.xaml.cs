﻿using System;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Effects;
using Autofac;

using KotorStudio.Models.AuroraInterfaces;
using KotorStudio.Models.AuroraParsers;
using KotorStudio.Models.KstConfig.KstConfigSingletons;
using KotorStudio.Performance;
using MainWindow = KotorStudio.UI.KstMainWindow.MainWindow;


namespace KotorStudio
{
    public partial class App : Application
    {
        public static IContainer Container;
       
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

           
            RenderOptions.ProcessRenderMode = RenderMode.Default; 
            if (KstKotorConfig.Instance.DefaultProjectPath != null) return;
            KstKotorConfig.Instance.DefaultProjectPath = AppDomain.CurrentDomain.BaseDirectory.Replace(@"\", @"/") + "config.txt";
            KstKotorConfig.Instance.RetrieveAllSettingsFromConfig();
  
           
            var builder = new ContainerBuilder();
            builder.RegisterType<ErfObject>().As<IErfObject>();
            builder.RegisterType<GffObject>().As<IGffObject>();
            builder.RegisterType<_2DaObject>().As<I_2daObject>();
            builder.RegisterType<BiffObject>().As<IBiffObject>();
            Container = builder.Build();
 
           
            
            Cache.GetInstance();
            
            var mainWindow = new MainWindow();
            mainWindow.Show();
            
        }


        protected override void OnSessionEnding(SessionEndingCancelEventArgs e)
        {
            base.OnSessionEnding(e);
            KstKotorConfig.Instance.SaveAllToConfigFile();
        }


        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
            KstKotorConfig.Instance.SaveAllToConfigFile();
        }
    }
}