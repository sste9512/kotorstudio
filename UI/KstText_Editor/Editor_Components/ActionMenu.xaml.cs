﻿using System.Windows.Controls;

namespace KotorStudio.UI.KstText_Editor.Editor_Components
{
    public partial class ActionMenu : UserControl
    {
        private readonly int[] _fonts;
        private RichTextBox _textBox;

        public ActionMenu(RichTextBox textBox)
        {
             InitializeComponent();
            _textBox = textBox;
            _fonts = new[] {16, 16, 12, 20, 22};
           
        }

        public ActionMenu()
        {
            _fonts = new[] {16, 16, 12, 20, 22};
        }


        public void ChooseFont(RichTextBox textBox, int selecedIndex)
        {
            textBox.FontSize = _fonts[selecedIndex];
        }
    }
}