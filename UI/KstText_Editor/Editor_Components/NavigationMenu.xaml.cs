﻿using System.Windows;
using System.Windows.Controls;

namespace KotorStudio.UI.KstText_Editor.Editor_Components
{
    
    public partial class NavigationMenu : UserControl
    {
        public NavigationMenu()
        {
            InitializeComponent();
        }

        private void SaveAsMenuItem_Click(object sender, RoutedEventArgs e)
        {
        }

        private void SaveMenuItem_Click(object sender, RoutedEventArgs e)
        {
        }

        private void ExitMenuItem_Click(object sender, RoutedEventArgs e)
        {
        }

        private void CloseCurrentTabMenuItem_Click(object sender, RoutedEventArgs e)
        {
        }

        private void CloseAllTabsMenuItem_Click(object sender, RoutedEventArgs e)
        {
        }
    }
}