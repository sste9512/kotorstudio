using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace KotorStudio.UI.KstText_Editor
{
    public partial class KstTextEditor : UserControl
    {
        public KstTextEditor()
        {
            InitializeComponent();
        }

        public KstTextEditor(string filePath)
        {
             InitializeComponent();
             string[] lines = File.ReadAllLines(filePath);
            foreach (var line in lines)
            {
               TextEditor.AppendText(line + "\n");
            }
            
        }
        private void TextBoxFocusableChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            
        }
    }
}
