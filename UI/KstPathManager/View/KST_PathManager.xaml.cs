﻿using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using KotorStudio.Models.KstConfig.KstConfigSingletons;

namespace KotorStudio.UI.KstPathManager.View
{
    public partial class KstPathManager : Window
    {
        public string Kotor2Path = "";
         public KstPathManager()
        {
            InitializeComponent();
            kotorPathLabel.Content = KstKotorConfig.Instance.KotorPath;
            kotorTSLPathLabel.Content = KstKotorConfig.Instance.Kotor2Path;
        }


        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


        public void Save(object sender, RoutedEventArgs e)
        {
        }


        private void Cancel(object sender, RoutedEventArgs e)
        {
            Close();
        }


        private void FindKotorPath(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            var result = dialog.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK) return;
            KstKotorConfig.Instance.KotorPath = dialog.SelectedPath;
            kotorPathLabel.Content = " ---> " + dialog.SelectedPath;
        }


        private void FindKotor2Path(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            var result = dialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                KstKotorConfig.Instance.Kotor2Path = dialog.SelectedPath;
                kotorTSLPathLabel.Content = " -> " + dialog.SelectedPath;
            }

            ;
        }


        private void RootWindow_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}