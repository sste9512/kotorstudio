using System.Windows;
using System.Windows.Forms;
using KotorStudio.Models.KstConfig.KstConfigSingletons;

namespace KotorStudio.UI.KstPathManager.View
{
    public partial class PathManager : Window
    {
        public PathManager()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            SizeToContent = SizeToContent.WidthAndHeight;
        }

        private void FindKotorPathOnClick(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            var result = dialog.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK) return;
            KstKotorConfig.Instance.KotorPath = dialog.SelectedPath;
            //kotorPathLabel.Content = " ---> " + dialog.SelectedPath;
        }

        private void FindKotorTslPathOnClick(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            var result = dialog.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK) return;
            KstKotorConfig.Instance.KotorPath = dialog.SelectedPath;
            //kotorPathLabel.Content = " ---> " + dialog.SelectedPath;
        }
    }
}
