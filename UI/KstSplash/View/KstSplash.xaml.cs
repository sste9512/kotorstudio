﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows;

namespace KotorStudio.UI.KstSplash.View
{
    public partial class KstSplash : Window
    {
        private CancellationTokenSource _cts = new CancellationTokenSource();
        private List<string> _directories = new List<string>();

        public KstSplash()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            MainWindowLoadingProgress.Value = 0;


            var worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            // worker.DoWork += worker_DoWork;
            //  worker.ProgressChanged += worker_ProgressChanged;
            //   worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.RunWorkerAsync(10000);
        }
    }
}