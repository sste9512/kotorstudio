using System.Collections.Generic;
using System.IO;
using KotorStudio.Models.LegacyERF;


namespace KotorStudio.UI.KstErfEditor
{
    public static class ErfEditorFactory
    {
      
        
        
        public static void ShowErfEditor(List<ErfKeyEntry> list, List<byte[]> resourceList, string fileName)
        {
            var editor = new ErfEditor(list, resourceList, fileName, string.Empty);
            editor.Show();
        }

       
        
        public static void ShowErfEditor(string filePath, string itemName)
        {
            using (var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                var erfParser = new ErfParser(fs);
                var num2 = checked(erfParser.EntryCount - 1);
                var index = 0;

                var refList = new List<ErfKeyEntry>();
                var resourceList = new List<byte[]>();

                while (index <= num2)
                {
                    //CREATES RESOURCE FILE AS BYTE[], USING THE CLS ERF OBJECT CREATED EARLIER
                    var erfResource = erfParser.GetErfResource(index);
                    resourceList.Add(erfResource);

                    //CREATES A KEY HHERE AND CONVERTS A KEYENTRY WITH SPECIFIED INDEX INTO A KEYENTRY OBJECT
                    var keyEntry = (ErfKeyEntry) erfParser.KeyEntryList[index];
                    refList.Add(keyEntry);

                    //WRITES THE FILES TO THE OUTPUT PATH
                    ++index;
                }

                new ErfEditor(refList, resourceList, itemName, string.Empty).Show();
            }
        }
    }
}