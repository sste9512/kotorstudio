﻿using System.Windows.Controls;

namespace KotorStudio.ERF_Editor.EditorComponents
{
    /// <summary>
    ///     Interaction logic for NavigationMenu.xaml
    /// </summary>
    public partial class NavigationMenu : UserControl
    {
        public NavigationMenu()
        {
            InitializeComponent();
        }
    }
}