﻿using System.Windows.Controls;

namespace KotorStudio.ERF_Editor.EditorComponents
{
    /// <summary>
    ///     Interaction logic for ERFTabControl.xaml
    /// </summary>
    public partial class ERFTabControl : UserControl
    {
        public ERFTabControl()
        {
            InitializeComponent();
        }
    }
}