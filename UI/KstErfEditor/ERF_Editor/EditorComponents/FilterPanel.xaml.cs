﻿using System.Windows;
using System.Windows.Controls;

namespace KotorStudio.ERF_Editor.EditorComponents
{
    /// <summary>
    ///     Interaction logic for FilterPanel.xaml
    /// </summary>
    public partial class FilterPanel : UserControl
    {
        public FilterPanel()
        {
            InitializeComponent();
        }


        //Switch the view to list form
        private void ButtonBase_OnClickList(object sender, RoutedEventArgs e)
        {
        }

        //switch the view to grid form
        private void ButtonBase_OnClickGrid(object sender, RoutedEventArgs e)
        {
        }
    }
}