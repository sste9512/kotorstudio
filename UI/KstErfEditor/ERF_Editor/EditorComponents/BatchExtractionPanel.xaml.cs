﻿using System;
using System.Windows;
using System.Windows.Forms;
using UserControl = System.Windows.Controls.UserControl;

namespace KotorStudio.ERF_Editor.EditorComponents
{
    /// <summary>
    ///     This class provides all of the functions needed for batch extraction of ERF -> TPC Resources
    /// </summary>
    public partial class BatchExtractionPanel : UserControl
    {
        private readonly string defaultOutputDir =
            AppDomain.CurrentDomain.BaseDirectory.Replace(@"\", @"/") + "config.txt";

        private string customOutputDir = "";

        private string implementedDir = "";

        public BatchExtractionPanel()
        {
            InitializeComponent();
        }


        private void OpenOutputDirectory(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            var result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                customOutputDir = dialog.SelectedPath;
                OutputDirLabel.Content = " -> " + dialog.SelectedPath; // update the directory label 
            }
        }


        public string getCustomOutputDir()
        {
            return customOutputDir;
        }


        private void ExtractToTGAEvent(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }


        private void ExtractToPNGEvent(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void DefaultCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            implementedDir = defaultOutputDir;
            CustomOutputCheckBox.IsChecked = false;
        }

        private void CustomOutputCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            implementedDir = customOutputDir;
            DefaultCheckBox.IsChecked = false;
        }
    }
}