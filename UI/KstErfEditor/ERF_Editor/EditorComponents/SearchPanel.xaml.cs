﻿using System.Windows.Controls;
using System.Windows.Input;

namespace KotorStudio.ERF_Editor.EditorComponents
{
    /// <summary>
    ///     TODO:
    ///     # 1 create react extension functions that update the ui and filter the tpc results
    /// </summary>
    public partial class SearchPanel : UserControl
    {
        public SearchPanel()
        {
            InitializeComponent();
        }

        private void SearchBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
        }


        private void SearchBox_OnMouseEnter(object sender, MouseEventArgs e)
        {
            var box = sender as TextBox;
            if (box.Text == "Search Resources Here") box.Text = "";
        }


        private void SearchBox_OnMouseLeave(object sender, MouseEventArgs e)
        {
            var box = sender as TextBox;
            if (box.Text == "") box.Text = "Search Resources Here";
        }
    }
}