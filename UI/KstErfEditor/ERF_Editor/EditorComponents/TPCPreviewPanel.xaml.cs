﻿using System.Windows.Controls;
using KotorStudio.Models.ERF;

namespace KotorStudio.ERF_Editor.EditorComponents
{
    /// TODO:
    /// #1 ADD PROCESS THAT TAKES A PREVIEW PICTURE OF THE TPC AND ADDS IT TO THE IMAGE
    /// #2 MAKE SURE THAT DATA BINDED UI IS FUNCTIONING PROPERLY AND ALL OF THE LABELS SCALE PROPERLY
    public partial class TPCPreviewPanel : UserControl
    {
        private readonly ErfKeyEntry selectedResource;


        public TPCPreviewPanel()
        {
            InitializeComponent();
        }


        public TPCPreviewPanel(ErfKeyEntry erfKeyEntry)
        {
            InitializeComponent();
            selectedResource = erfKeyEntry;
            DataContext = selectedResource;
        }
    }
}