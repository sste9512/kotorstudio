﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using KotorStudio.KST_Colors;
using KotorStudio.Models.ERF;
using KotorStudio.XMLHelpers;
using Newtonsoft.Json;

namespace KotorStudio.ERF_Editor.ViewModel
{
    internal class ERF_ContextMenu
    {
        public static ContextMenu generateMenu(ErfKeyEntry erfKey)
        {
            //INIT CXT MENU HERE
            var menu = new ContextMenu();
            menu.Background = KColors.FlatLightBlack;


            //TO JSON ITEM HERE
            var copyJsonItem = new MenuItem();
            copyJsonItem.Foreground = Brushes.White;
            copyJsonItem.Header = "Copy as Json";
            copyJsonItem.Click += (s, e) => { Clipboard.SetText(JsonConvert.SerializeObject(erfKey)); };
            menu.Items.Add(copyJsonItem);


            //TO XML ITEM HERE
            var item = new MenuItem();
            item.Foreground = Brushes.White;
            item.Header = "Copy as XML";
            item.Click += (s, e) => { Clipboard.SetText(XmlUtils.ToXml(erfKey)); };
            menu.Items.Add(item);


            var seperator = new Separator();
            seperator.Foreground = Brushes.White;
            menu.Items.Add(seperator);


            //WRITE TO XML FILE HERE
            var ERFtoXMLFile = new MenuItem();
            ERFtoXMLFile.Foreground = Brushes.White;
            ERFtoXMLFile.Header = "Write to  XML File";
            ERFtoXMLFile.Click += (s, e) =>
            {
                var sw = new StreamWriter("C:/Users/steve/Desktop/kotor_tpcs/" + erfKey.ResourceName + ".txt", true);
                sw.WriteLine(XmlUtils.ToXml(erfKey));
                sw.Close();
            };
            menu.Items.Add(ERFtoXMLFile);


            return menu;
        }
    }
}