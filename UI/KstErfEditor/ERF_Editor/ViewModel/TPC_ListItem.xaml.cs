﻿using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using KotorStudio.Models.ERF;

namespace KotorStudio.ERF_Editor.ViewModel
{
    /// <summary>
    ///     Interaction logic for TPC_ListItem.xaml
    /// </summary>
    public partial class TPC_ListItem : UserControl
    {
        public TPC_ListItem()
        {
            InitializeComponent();
        }

        public TPC_ListItem(ErfKeyEntry entry)
        {
            InitializeComponent();
            resourceName.Content = entry.ResourceName;
            itemIndexLabel.Content = entry.Index;
            var menu = ERF_ContextMenu.generateMenu(entry);
            ContextMenu = menu;
            MouseRightButtonDown += (s, e) => { ContextMenu.IsOpen = true; };
        }


        private void UserControl_MouseEnter(object sender, MouseEventArgs e)
        {
            listBorder.BorderBrush = Brushes.CadetBlue;
        }

        private void UserControl_MouseLeave(object sender, MouseEventArgs e)
        {
            listBorder.BorderBrush = Brushes.Transparent;
        }
    }
}