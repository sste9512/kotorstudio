﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Xml.Serialization;
using KotorStudio.ERF_Editor.ViewModel;
using KotorStudio.KST_Text_Editor;
using KotorStudio.Models.ERF;
using Newtonsoft.Json;

namespace KotorStudio.ERF_Editor
{
    public partial class ERF_Editor : Window
    {
        internal string batchOutputDirectory = "";

        internal List<ErfKeyEntry>
            intialList = new List<ErfKeyEntry>(); // List of ERFKeyEntries, and main datasource being passed around

        internal List<byte[]> resourceList = new List<byte[]>(); // List of actual bytes contatined by the ERFS

        internal IDisposable
            subscription; // RX disposeable, stored on heap to be disposed on Window Closing Event (IMPORTANT)

        internal List<TabControl> tabs = new List<TabControl>(); // Container for the tabs produced


        /*
         * =======================================================================
         *
         *  Main Constructor Here
         *
         *=======================================================================
         */

        public ERF_Editor(List<ErfKeyEntry> list, List<byte[]> resourceList, string fileName)
        {
            InitializeComponent();
            SizeToContent = SizeToContent.WidthAndHeight;
            WindowStartupLocation = WindowStartupLocation.CenterScreen;

            intialList = list;
            this.resourceList = resourceList;
            ErfWindowtitle.Content = "ERF Editor ->" + new DirectoryInfo(fileName).Name;
            populateListWithKeyEntries(intialList, resourceList, ListBox);
            IndeterminateProgressBar.Visibility = Visibility.Hidden;

            NavigationMenu.WriteToJSONItem.Click += (s, e) => { writeAllToFile(); };
            NavigationMenu.ShowDataGrid.Click += (s, e) => { ERFGrid.ERFGrid.ShowERFGrid(intialList); };
            NavigationMenu.WriteToXMLItem.Click += (s, e) =>
            {
                IndeterminateProgressBar.Visibility = Visibility.Visible;
                var serializer = new XmlSerializer(typeof(List<ErfKeyEntry>));
                TextWriter textWriter = new StreamWriter(@"C:/Users/steve/Desktop/kotor_tpcs/trialXMLS.xml");
                serializer.Serialize(textWriter, intialList);
                textWriter.Close();
            };
        }


        /*
          * =======================================================================
          *
          *  Write to JSON File Here
          *
          *=======================================================================
          */

        private void writeAllToFile()
        {
            IEnumerable<ErfKeyEntry> list = intialList; //Convert erfkeyentry list to enumerable for processing
            var sw = new StreamWriter("C:/Users/steve/Desktop/kotor_tpcs/trial.txt", true);

            //Init observable, converted from enumerable list
            var source = list.ToObservable()
                .SubscribeOn(NewThreadScheduler.Default) // do processing work on background
                .ObserveOn(DispatcherScheduler.Current); // act on main thread
            subscription = source.Subscribe(
                x => sw.WriteLine(JsonConvert.SerializeObject(x) + Environment.NewLine), //write json value to file
                ex => Console.WriteLine("OnError: {0}", ex.Message), // error handling
                () => sw.Close()); // close streamwriter resources
        }


        /*
        * =======================================================================
        *
        *    Populates the listbox with the values of the erfentries
         *   The TPC_ListItem initialises itself and maps to presentation state
         *   Establishes double click listener to link events to the ERF Editor
        *
        *=======================================================================
        */

        private void populateListWithKeyEntries(List<ErfKeyEntry> listEntries, List<byte[]> resourceList, ListBox box)
        {
            var index = 0;

            foreach (var entry in listEntries)
            {
                var item = new TPC_ListItem(entry);
                Debug.WriteLine(entry.ResourceName);

                item.MouseDoubleClick += (s, e) =>
                {
                    new KstTextEditor(JsonConvert.SerializeObject(entry)).Show(); // Open KST Text Editor Here
                    ErfKeyEntry.WriteToFile("", entry);
                    //tpc.tpc t = new tpc.tpc();

                    //add function to speak to the tpc preview control here
                };

                box.Items.Add(item);
                index += 1;
            }
        }


        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("This button works");
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
        }

        private void window_Closing(object sender, CancelEventArgs e)
        {
            if (subscription != null) subscription.Dispose();
        }
    }
}