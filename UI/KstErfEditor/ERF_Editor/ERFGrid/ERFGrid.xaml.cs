﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using KotorStudio.Models.ERF;

namespace KotorStudio.ERF_Editor.ERFGrid
{
    /// <summary>
    /// Interaction logic for ERFGrid.xaml
    /// </summary>
    public partial class ERFGrid : Window
    {
        public List<ErfKeyEntry> list;

        public ERFGrid(List<ErfKeyEntry> list)
        {
            InitializeComponent();
            this.list = list;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            dataGrid1.ItemsSource = list;
        }


        public static void ShowERFGrid(List<ErfKeyEntry> erf)
        {
            new ERFGrid(erf).Show();
        }

    }
}
