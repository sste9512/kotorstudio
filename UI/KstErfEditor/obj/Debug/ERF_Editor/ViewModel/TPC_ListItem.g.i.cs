﻿#pragma checksum "..\..\..\..\ERF_Editor\ViewModel\TPC_ListItem.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "F922C5A723D1CA97E5D8356A3F02F9F8F09706E0"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using KotorStudio.ERF_Editor.ViewModel;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace KotorStudio.ERF_Editor.ViewModel {
    
    
    /// <summary>
    /// TPC_ListItem
    /// </summary>
    public partial class TPC_ListItem : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 9 "..\..\..\..\ERF_Editor\ViewModel\TPC_ListItem.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border listBorder;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\..\..\ERF_Editor\ViewModel\TPC_ListItem.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label itemIndexLabel;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\..\ERF_Editor\ViewModel\TPC_ListItem.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label resourceName;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\..\ERF_Editor\ViewModel\TPC_ListItem.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label resourceOffset;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\..\ERF_Editor\ViewModel\TPC_ListItem.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label resourceType;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ERF_Editor;component/erf_editor/viewmodel/tpc_listitem.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\ERF_Editor\ViewModel\TPC_ListItem.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 7 "..\..\..\..\ERF_Editor\ViewModel\TPC_ListItem.xaml"
            ((KotorStudio.ERF_Editor.ViewModel.TPC_ListItem)(target)).MouseEnter += new System.Windows.Input.MouseEventHandler(this.UserControl_MouseEnter);
            
            #line default
            #line hidden
            
            #line 8 "..\..\..\..\ERF_Editor\ViewModel\TPC_ListItem.xaml"
            ((KotorStudio.ERF_Editor.ViewModel.TPC_ListItem)(target)).MouseLeave += new System.Windows.Input.MouseEventHandler(this.UserControl_MouseLeave);
            
            #line default
            #line hidden
            return;
            case 2:
            this.listBorder = ((System.Windows.Controls.Border)(target));
            return;
            case 3:
            this.itemIndexLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.resourceName = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.resourceOffset = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.resourceType = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

