﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Serialization;
using KotorStudio.Models.LegacyERF;
using KotorStudio.UI.KstErfEditor.EditorComponents.TemplateControls;
using KotorStudio.UI.KstText_Editor;
using Newtonsoft.Json;

namespace KotorStudio.UI.KstErfEditor
{
    public partial class ErfEditor : Window
    {
        private readonly List<ErfKeyEntry> _intialList; 

        private string _batchOutputDirectory;

        private List<byte[]> _resourceList;

        private IDisposable _subscription; 

        internal List<TabControl> Tabs = new List<TabControl>();

       

        
        
        
        
        public ErfEditor(List<ErfKeyEntry> list, List<byte[]> resourceList, string fileName,
            string batchOutputDirectory)
        {
            InitializeComponent();
            SizeToContent = SizeToContent.Width;
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            _intialList = list;
            _resourceList = resourceList;
            _batchOutputDirectory = batchOutputDirectory;
           
            PopulateListWithKeyEntries(_intialList, resourceList, ListBox);
           

            NavigationMenu.WriteToJsonItem.Click += (s, e) => { WriteAllToFile(); };
            NavigationMenu.ShowDataGrid.Click += (s, e) =>
            {
                //ErfGrid.ShowErfGrid(_intialList);
            };
            NavigationMenu.WriteToXmlItem.Click += (s, e) =>
            {
               
                var serializer = new XmlSerializer(typeof(List<ErfKeyEntry>));
                TextWriter textWriter = new StreamWriter(@"C:/Users/steve/Desktop/kotor_tpcs/trialXMLS.xml");
                serializer.Serialize(textWriter, _intialList);
                textWriter.Close();
            };
            SearchPanel.SearchButton.Click += (s, e) =>
            {
                  
                
            };



        }


       

        private void WriteAllToFile()
        {
            IEnumerable<ErfKeyEntry> list = _intialList; //Convert erfkeyentry list to enumerable for processing
            var sw = new StreamWriter("C:/Users/steve/Desktop/kotor_tpcs/trial.txt", true);

            //Init observable, converted from enumerable list
            var source = list.ToObservable()
                .SubscribeOn(NewThreadScheduler.Default) // do processing work on background
                .ObserveOn(DispatcherScheduler.Current); // act on main thread
            _subscription = source.Subscribe(
                x => sw.WriteLine(JsonConvert.SerializeObject(x) + Environment.NewLine), //write json value to file
                ex => Console.WriteLine(@"OnError: {0}", ex.Message), // error handling
                () => sw.Close()); // close streamwriter resources
        }


        /*
        * ======================================================================
        *
        *   Populates the listbox with the values of the erfentries
        *   The TPC_ListItem initialises itself and maps to presentation state
        *   Establishes double click listener to link events to the ERF Editor
        *
        *=======================================================================
        */

        private void PopulateListWithKeyEntries(List<ErfKeyEntry> listEntries, List<byte[]> resourceList, ListBox box)
        {
            foreach (var entry in listEntries)
            {
                var item = new TpcListItem(entry);
                Debug.WriteLine(entry.ResourceName);

                item.MouseDoubleClick += (s, e) =>
                {
                   // new KstTextEditor(JsonConvert.SerializeObject(entry)).Show(); // Open KST Text Editor Here
                    //ErfKeyEntry.WriteToFile(entry, "");
                };

                box.Items.Add(item);
            }
        }


      

        
    
       
         private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void window_Closing(object sender, CancelEventArgs e)
        {
            _subscription?.Dispose();
        }

        private void Window_OnClosed(object sender, EventArgs e)
        {
            
        }

     
        

     
    }

   

   
}