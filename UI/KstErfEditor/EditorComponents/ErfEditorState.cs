using System;
using System.IO;
using System.Linq;

namespace KotorStudio.UI.KstErfEditor.EditorComponents
{
    public class ErfEditorState
    {
        public bool HasInitialisedFiles { get; set; }
        public bool HasSetupRepository { get; set; }
        public string baseDirectory { get; set; }
        public FileInfo[] ErfStorageDirectories { get; set; }

        public ErfEditorState SetFilePath(string baseDir)
        {
            baseDirectory = baseDir;
            return this;
        }

        public ErfEditorState ValidateExistingStorageDirectory(string fileName)
        {
            var baseAppDirectory = AppDomain.CurrentDomain.BaseDirectory.Replace(@"\", @"/") + "/" + "Erfs";
            if (!Directory.Exists(baseAppDirectory))
            {
                Directory.CreateDirectory(baseAppDirectory);
            }

            var files = Directory.GetDirectories(AppDomain.CurrentDomain.BaseDirectory.Replace(@"\", @"/"));
            if (!files.ToList().Contains(baseAppDirectory + "\\" + fileName))
            {
                Directory.CreateDirectory(baseAppDirectory + "/" + new DirectoryInfo(fileName).Name);
            }

            return this;
        }
          
        
    }
}