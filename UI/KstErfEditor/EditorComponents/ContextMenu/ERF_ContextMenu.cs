﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using KotorStudio.Models.LegacyERF;
using KotorStudio.Utils.DesignHelpers.KST_Colors;
using KotorStudio.Utils.XMLHelpers;
using Newtonsoft.Json;


namespace KotorStudio.UI.KstErfEditor.EditorComponents.ContextMenu
{
    internal static class ErfContextMenu
    {
        public static System.Windows.Controls.ContextMenu GenerateMenu(ErfKeyEntry erfKey)
        {
            if (erfKey == null) throw new ArgumentNullException(nameof(erfKey));
            //INIT CXT MENU HERE
            var menu = new System.Windows.Controls.ContextMenu {Background = KColors.FlatLightBlack};


            //TO JSON ITEM HERE
            var copyJsonItem = new MenuItem {Foreground = Brushes.White, Header = "Copy as Json"};
            //  copyJsonItem.Click += (s, e) => { Clipboard.SetText(JsonConvert.SerializeObject(erfKey)); };
            menu.Items.Add(copyJsonItem);


            //TO XML ITEM HERE
            var copyAsXmlItem = new MenuItem {Foreground = Brushes.White, Header = "Copy as XML"};
            copyAsXmlItem.Click += (s, e) => { Clipboard.SetText(XmlUtils.ToXml(erfKey)); };
            menu.Items.Add(copyAsXmlItem);


            var separator = new Separator();
            separator.Foreground = Brushes.White;
            menu.Items.Add(separator);


            //WRITE TO XML FILE HERE
            var erFtoXmlFile = new MenuItem {Foreground = Brushes.White, Header = "Write to  XML File"};
            erFtoXmlFile.Click += (s, e) =>
            {
                var sw = new StreamWriter("C:/Users/steve/Desktop/kotor_tpcs/" + erfKey.ResourceName + ".txt", true);
                sw.WriteLine(XmlUtils.ToXml(erfKey));
                sw.Close();
            };
            menu.Items.Add(erFtoXmlFile);

            //WRITE TO JSON FILE HERE
            var erFtoJsonFile = new MenuItem {Foreground = Brushes.White, Header = "Write to  JSON File"};
            erFtoJsonFile.Click += async (s, e) =>
            {
                await Task.Factory.StartNew(() =>
                {
                    using (var sw = new StreamWriter("C:/Users/steve/Desktop/kotor_tpcs/" + erfKey.ResourceName + ".txt", true))
                    {
                        sw.WriteLine(JsonConvert.SerializeObject(erfKey));
                    }
                });
            };
            menu.Items.Add(erFtoJsonFile);


            return menu;
        }
    }
}