﻿using System.Drawing;
using System.IO;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using KotorStudio.Models.AuroraParsers;
using KotorStudio.Models.LegacyERF;
using KotorStudio.UI.KstErfEditor.EditorComponents.ContextMenu;
using KotorStudio.Utils;
using Brushes = System.Windows.Media.Brushes;
using ResourceHandler = KSTModels.Utils.ResourceHandler;

namespace KotorStudio.UI.KstErfEditor.EditorComponents.TemplateControls
{
    public partial class TpcListItem : UserControl
    {
        public TpcListItem()
        {
            InitializeComponent();
            DataContext = this;
        }

        public TpcListItem(ErfObject.ErfKey key)
        {
            InitializeComponent();
            ResourceName.Content = StringUtils.CleanInput(new string(key.ResRef));
            ItemIndexLabel.Content = key.ResId;
            ResourceType.Text = ResourceHandler.GetRsrcTypeForId(key.ResType);
        }

        BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        public TpcListItem(ErfKeyEntry entry)
        {
            InitializeComponent();
            ResourceName.Content = entry.ResourceName;
            ItemIndexLabel.Content = entry.Index;
            var menu = ErfContextMenu.GenerateMenu(entry);
            ContextMenu = menu;
            MouseRightButtonDown += (s, e) =>
            {
                var contextMenu = ContextMenu;
                if (contextMenu != null) contextMenu.IsOpen = true;
            };
        }

        public void IntitializeImageFromDb(ErfKeyEntry entry)
        {
            //Call Database from here 
            //Use Entity to find 
        }


        private void UserControl_MouseEnter(object sender, MouseEventArgs e)
        {
            ListBorder.BorderBrush = Brushes.CadetBlue;
        }


        private void UserControl_MouseLeave(object sender, MouseEventArgs e)
        {
            ListBorder.BorderBrush = Brushes.Transparent;
        }

        private void UserControl_OnContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
        }

        private void UserControl_OnContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
        }
    }
}