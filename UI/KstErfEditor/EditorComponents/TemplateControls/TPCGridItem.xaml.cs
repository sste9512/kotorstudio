using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using KotorStudio.Models.AuroraParsers;
using KotorStudio.Models.LegacyERF;
using KotorStudio.UI.KstErfEditor.EditorComponents.ContextMenu;
using KotorStudio.Utils;
using ResourceHandler = KSTModels.Utils.ResourceHandler;

namespace KotorStudio.UI.KstErfEditor.EditorComponents.TemplateControls
{
    public partial class TpcGridItem : UserControl
    {
        private ErfKeyEntry _keyEntry;

        private ErfObject.ErfKey erfKey;
        private ErfObject.ErfResource erfResource;

        public TpcGridItem()
        {
            InitializeComponent();
        }

        public TpcGridItem(ErfObject.ErfKey erfKey, ErfObject.ErfResource erfResource)
        {
            InitializeComponent();
            this.erfKey = erfKey;
            this.erfResource = erfResource;
            ResourceName.Text = StringUtils.CleanInput(new string(erfKey.ResRef));
            ResType.Text = ResourceHandler.GetRsrcTypeForId(erfKey.ResType);
        }

        public TpcGridItem(ErfKeyEntry entry, byte[] resource)
        {
            InitializeComponent();
            _keyEntry = entry;
            MapToObject();
            ContextMenu = ErfContextMenu.GenerateMenu(entry);
            MouseRightButtonDown += RightButtonDown;
        }


        public TpcGridItem(ErfKeyEntry entry)
        {
            InitializeComponent();
            _keyEntry = entry;
            MapToObject();
            ContextMenu = ErfContextMenu.GenerateMenu(entry);
            MouseRightButtonDown += RightButtonDown;
        }

        public void MapToObject()
        {
            ResourceName.Text = _keyEntry.ResourceName;
            ResType.Text = _keyEntry._resTypeStr;
        }


        private void RightButtonDown(object sender, MouseButtonEventArgs e)
        {
            var contextMenu = ContextMenu;
            if (contextMenu != null) contextMenu.IsOpen = true;
        }

        private void PreviewTPC_OnClick(object sender, RoutedEventArgs e)
        {
        }

        private void OnClick_WriteToDatabase(object sender, RoutedEventArgs e)
        {
        }

        private void OnClick_UploadToGoogleDrive(object sender, RoutedEventArgs e)
        {
        }
    }
}