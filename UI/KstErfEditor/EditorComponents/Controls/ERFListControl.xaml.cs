using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Controls;
using KotorStudio.Models.AuroraParsers;
using KotorStudio.Models.LegacyERF;
using KotorStudio.UI.KstErfEditor.EditorComponents.TemplateControls;
using KotorStudio.UI.KstText_Editor;
using Newtonsoft.Json;

namespace KotorStudio.UI.KstErfEditor.EditorComponents.Controls
{
    public partial class ErfListControl : UserControl
    {
        
        
        public ErfListControl()
        {
            InitializeComponent();
        }
       
        
        
        
        public void PopulateListWithKeyEntries(List<ErfObject.ErfKey> keyEntries, List<ErfObject.ErfResource> resourceEntries)
        {
            foreach (var entry in keyEntries)
            {
                var item = new TpcListItem(entry);
               

                item.MouseDoubleClick += (s, e) =>
                {
                   // new KstTextEditor(JsonConvert.SerializeObject(entry)).Show(); // Open KST Text Editor Here
                    //ErfKeyEntry.WriteToFile(entry);
                };

                ListBox.Items.Add(item);
            }
              
        }
    
      
        
        
        public void PopulateListWithKeyEntries(List<ErfKeyEntry> listEntries, List<byte[]> resourceList)
        {
            foreach (var entry in listEntries)
            {
                var item = new TpcListItem(entry);
                Debug.WriteLine(entry.ResourceName);

                item.MouseDoubleClick += (s, e) =>
                {
                   // new KstTextEditor(JsonConvert.SerializeObject(entry)).Show(); // Open KST Text Editor Here
                   // ErfKeyEntry.WriteToFile(entry, "");
                };

                ListBox.Items.Add(item);
            }
        }
    }
}
