using System.Collections.Generic;
using System.Windows.Controls;
using KotorStudio.Models.AuroraParsers;
using KotorStudio.UI.KstErfEditor.EditorComponents.TemplateControls;
using KotorStudio.UI.KstMainWindow.Commands;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace KotorStudio.UI.KstErfEditor.EditorComponents.Controls
{
    public partial class ExtractionPanelControl : UserControl
    {

        private ConcreteWindowCommand _command;
        private List<ErfObject.ErfResource> _erfResources;
        
        public ExtractionPanelControl()
        {
            InitializeComponent();
        }

        public ExtractionPanelControl(WindowReceiver receiver, List<ErfObject.ErfResource> erfResources)
        {
             InitializeComponent();
            _command = new ConcreteWindowCommand(receiver);
            _erfResources = erfResources;
        }

        private void fillFilePanel(ref List<ErfObject.ErfResource> _erfResources)
        {
            for (int i = 0; i < 20; i++)
            {
                FileExtractionPanel.Children.Add(new ExtractionPanelItems());
            }
        }
        
        private void fillDatabasePanel(ref List<ErfObject.ErfResource> _erfResources)
        {
            
        }
        
        private void fillGoogleDrivePanel(ref List<ErfObject.ErfResource> _erfResources)
        {
            
        }
    }
}
