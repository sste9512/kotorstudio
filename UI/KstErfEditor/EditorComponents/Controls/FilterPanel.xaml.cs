﻿using System.Windows;
using System.Windows.Controls;

namespace KotorStudio.UI.KstErfEditor.EditorComponents.Controls
{
    public partial class FilterPanel : UserControl
    {
        private bool IsList { get; set; }
        
        public FilterPanel()
        {
            InitializeComponent();
            
        }

        private void ButtonBase_OnClickList(object sender, RoutedEventArgs e)
        {
            if (!IsList)
            {
                //ConvertToListHere
                //Possibly add transition?
            }
        }

        private void ButtonBase_OnClickGrid(object sender, RoutedEventArgs e)
        {
            if (IsList)
            {
                //ConvertTOGridHere
                //Possibly add transition
            }
        }
    }
}