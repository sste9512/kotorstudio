using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using KotorStudio.Models.AuroraParsers;
using KotorStudio.UI.KstErfEditor.EditorComponents.TemplateControls;

namespace KotorStudio.UI.KstErfEditor.EditorComponents.Controls
{
    public partial class ErfBuilder : UserControl
    {
        public List<ErfObject.ErfResource> ListToBuild { get; set; }

        public ErfBuilder()
        {
            InitializeComponent();
            for (int i = 0; i < 30; i++)
            {
                builderList.Items.Add(new ErfBuilderListItem());
            }
        }

        private void BuilderList_OnDrop(object sender, DragEventArgs e)
        {
        }

        private void ClearList()
        {
            ListToBuild.ForEach(x => { ListToBuild.Remove(x); });
        }
    }
}