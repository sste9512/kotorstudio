using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Controls;
using KotorStudio.Models.AuroraParsers;
using KotorStudio.Performance;
using KotorStudio.UI.KstErfEditor.EditorComponents.TemplateControls;

namespace KotorStudio.UI.KstErfEditor.EditorComponents.Controls
{
    public partial class ErfGrid : UserControl
    {

        private Cache gridCache;
        
        
        public ErfGrid()
        {
            InitializeComponent();
            gridCache = new Cache();
        }

     
        public void PopulateGridWithKeyEntries(List<ErfObject.ErfKey> listEntries, List<ErfObject.ErfResource> resourceList)
        {
            //if cache does not contain this particular list of resources then add each to cache while building
            if (gridCache.GetItem(listEntries[0].ToString()) == null)
            {
                for (int i = 0; i < listEntries.Count; i++)
                {
                     
                    var item = new TpcGridItem(listEntries[i], resourceList[i]);
                    gridCache.Add(listEntries[i].ToString(), item);
                    item.MouseDoubleClick += (s, e) =>{};
                    ErfWrapPanel.Children.Add(item);
                }
            }
            else
            {
                for (int i = 0; i < listEntries.Count; i++)
                {
                    //var item = new TpcGridItem(listEntries[i], resourceList[i]);
                   // item.MouseDoubleClick += (s, e) =>
                  //  {
                        //   new KstTextEditor(JsonConvert.SerializeObject(listEntries[i])).Show();
                 //   };
                    ErfWrapPanel.Children.Add((TpcGridItem)gridCache.GetItem(listEntries[i].ToString()));
                }
            }
          
        }

    }
}
