using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.VisualStyles;
using Autofac;
using KotorStudio.Business;
using KotorStudio.Models.AuroraInterfaces;
using KotorStudio.Models.AuroraParsers;
using KotorStudio.Models.KstConfig.KstConfigSingletons;
using KotorStudio.Models.LegacyERF;
using KotorStudio.UI.KstMainWindow.Commands;
using KotorStudio.UI.KstMainWindow.Interfaces;
using KotorStudio.Utils;


namespace KotorStudio.UI.KstErfEditor.EditorComponents.Controls
{
    [DebuggerDisplay("Init Erf Editor")]
    public partial class ErfEditor : UserControl, IWindowReceiver, IDisposable
    {
        private readonly List<ErfKeyEntry> _initialList;
        private readonly List<ErfObject.ErfKey> _keyList;
        private readonly List<ErfObject.ErfResource> _resourceList;

        private IDisposable _subscription;
        private UserControl _tpcContainer;

        private IErfObject erfObj;
        
        private ErfObject _erfObject;
        private List<byte[]> _resourceDataList;

        private ErfEditorState State { get; }
        private WindowReceiver _receiver;

        private PathManager _pathManager;
        
        
        public ErfEditor()
        {
            InitializeComponent();
        }

        private async Task<ErfObject> erfCompiler(string filePath)
        {
            return await Task.Factory.StartNew(() =>
            {
                var erf = new ErfObject(filePath);
                return erf;
            });
        }
        public ErfEditor(string filePath)
        {
            InitializeComponent();
            _erfObject = new ErfObject(filePath);
            _keyList = _erfObject.GetKeys();
            _resourceList = _erfObject.GetResources();
            _resourceDataList = new List<byte[]>();
            _pathManager = new PathManager();

            State = new ErfEditorState().SetFilePath(filePath).ValidateExistingStorageDirectory(filePath);
            
            foreach (var res in _erfObject.GetResources())
            {
                var resData = _erfObject.GetRawResource(res);
                Console.WriteLine(resData);
                _resourceDataList.Add(resData);
            }
            WriteAllToFile();
            

            ErfListControl.PopulateListWithKeyEntries(_keyList, _resourceList);
            FilterPanel.GridConversionBtn.Click += OnClickGridConversion;
            FilterPanel.ListConversionBtn.Click += OnClickListConversion;
            _receiver = new WindowReceiver();
            _receiver.SetWindowReceiver(this);

          //  _pathManager.SetBaseDirectory(s => @"C:\ResourceHolder");
            _pathManager.MapDirectoryFilesToResourceList(_keyList.ToList(),
                                                         key => { Console.WriteLine(key.ResRef); },
                                                         () => { Console.WriteLine("this messed up"); },
                                                         () => { Console.WriteLine("This has Completed"); });
        }

        
        private void OnClickGridConversion(Object sender, RoutedEventArgs e)
        {
            ErfLayoutContainer.Children.RemoveAt(0);
            var erfGrid = new ErfGrid();
            erfGrid.PopulateGridWithKeyEntries(_keyList, _resourceList);
            ErfLayoutContainer.Children.Add(erfGrid);
        }

        private void OnClickListConversion(Object sender, RoutedEventArgs e)
        {
            ErfLayoutContainer.Children.RemoveAt(0);
            var erfList = new ErfListControl();
            erfList.PopulateListWithKeyEntries(_keyList, _resourceList);
            ErfLayoutContainer.Children.Add(erfList);
        }

        private void WriteAllToFile()
        {
            for (int i = 0; i < _resourceDataList.Count; i++)
            {
                var title = new String(_keyList[i].ResRef);
                File.WriteAllBytes(@"C:\TPCS" + "\\" + StringUtils.CleanInput(title) + ".tpc", _resourceDataList[i]);
            }
        }


        public void Dispose()
        {
            _subscription?.Dispose();
        }

        public void OnReceive(object sender, int commandType)
        {
            
        }
    }
}