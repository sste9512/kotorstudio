﻿using System.Windows.Controls;
using System.Windows.Input;

namespace KotorStudio.UI.KstErfEditor.EditorComponents.Controls
{
    public partial class SearchPanel : UserControl
    {
        public SearchPanel()
        {
            InitializeComponent();
        }

        private void SearchBox_OnMouseEnter(object sender, MouseEventArgs e)
        {
            var box = sender as TextBox;
            if (box != null && box.Text == "Search Resources Here") box.Text = "";
        }

        private void SearchBox_OnMouseLeave(object sender, MouseEventArgs e)
        {
            var box = sender as TextBox;
            if (box.Text == "") box.Text = "Search Resources Here";
        }
    }
}