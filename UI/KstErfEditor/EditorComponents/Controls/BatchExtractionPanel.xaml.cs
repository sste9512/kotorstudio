﻿using System;
using System.Windows;
using System.Windows.Forms;
using UserControl = System.Windows.Controls.UserControl;

namespace KotorStudio.UI.KstErfEditor.EditorComponents.Controls
{
    public partial class BatchExtractionPanel : UserControl
    {
        private readonly string _defaultOutputDir =
            AppDomain.CurrentDomain.BaseDirectory.Replace(@"\", @"/") + "config.txt";

        private string _customOutputDir = "";

        private string _implementedDir = "";

        public BatchExtractionPanel()
        {
            InitializeComponent();
        }


        private void OpenOutputDirectory(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            var result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                _customOutputDir = dialog.SelectedPath;
                OutputDirLabel.Content = " -> " + dialog.SelectedPath; // update the directory label 
            }
        }


        public string GetCustomOutputDir()
        {
            return _customOutputDir;
        }


        private void ExtractToTgaEvent(object sender, RoutedEventArgs e)
        {
            
        }


        private void ExtractToPngEvent(object sender, RoutedEventArgs e)
        {
           
        }

        private void DefaultCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            _implementedDir = _defaultOutputDir;
            CustomOutputCheckBox.IsChecked = false;
        }

        private void CustomOutputCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            _implementedDir = _customOutputDir;
            DefaultCheckBox.IsChecked = false;
        }
    }
}