﻿using System.Windows.Controls;
using KotorStudio.Models.LegacyERF;

namespace KotorStudio.UI.KstErfEditor.EditorComponents.Controls
{
    public partial class TpcPreviewPanel : UserControl
    {
        private readonly ErfKeyEntry _selectedResource;

        public TpcPreviewPanel()
        {
            InitializeComponent();
        }

        public TpcPreviewPanel(ErfKeyEntry erfKeyEntry)
        {
            InitializeComponent();
            _selectedResource = erfKeyEntry;
            DataContext = _selectedResource;
        }
    }
}