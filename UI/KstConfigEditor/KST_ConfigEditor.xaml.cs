﻿using System;
using System.Windows;
using System.Windows.Input;

namespace KotorStudio.UI.KstConfigEditor
{
    public partial class KstConfigEditor : Window
    {
        public KstConfigEditor()
        {
            InitializeComponent();
            SizeToContent = SizeToContent.WidthAndHeight;
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }

        public static void CallConfigEditor()
        {
            new KstConfigEditor().Show();
        }


        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


        private void MaxButton_Click(object sender, RoutedEventArgs e)
        {
            
        }


        private void MinButton_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }


        private void Window_ContentRendered(object sender, EventArgs e)
        {
        }


        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}