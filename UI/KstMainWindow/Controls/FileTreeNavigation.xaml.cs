using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace KotorStudio.UI.KstMainWindow.Controls
{
    public partial class FileTreeNavigation : UserControl
    {
        public FileTreeNavigation()
        {
            InitializeComponent();
        }


        private void TabItem_OnMouseEnter(object sender, MouseEventArgs e)
        {
            Border.BorderBrush = Brushes.Azure;
        }

        private void TabItem_OnMouseLeave(object sender, MouseEventArgs e)
        {
            Border.BorderBrush = Brushes.Transparent;
        }

        private void TabItem_OnMouseEnter1(object sender, MouseEventArgs e)
        {
            Border.BorderBrush = Brushes.Azure;
        }

        private void TabItem_OnMouseLeave1(object sender, MouseEventArgs e)
        {
            Border.BorderBrush = Brushes.Transparent;
        }
    }
}