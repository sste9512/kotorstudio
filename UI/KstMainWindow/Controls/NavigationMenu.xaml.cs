﻿using System.Windows;
using System.Windows.Controls;
using KotorStudio.UI.KstPathManager.View;
using KotorStudio.UI.KstText_Editor;

namespace KotorStudio.UI.KstMainWindow.Controls
{
    public partial class NavigationMenu : UserControl
    {
        public NavigationMenu()
        {
            InitializeComponent();
        }

        private void OpenPathManager(object sender, RoutedEventArgs e)
        {
            var pathMan = new PathManager();
            pathMan.Show();
        }

        private void MenuItem_OnClick(object sender, RoutedEventArgs e)
        {
           // var textEditor = new KstTextEditor();
           // textEditor.Show();
        }

        private void ShowEditorSettings_Click(object sender, RoutedEventArgs e)
        {
            KstConfigEditor.KstConfigEditor.CallConfigEditor();
        }
    }
}