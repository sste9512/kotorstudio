using System;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace KotorStudio.UI.KstMainWindow.Controls
{
    public partial class TabHeader : UserControl
    {
       
        private Guid guid { get; set; }
        
        public TabHeader()
        {
            InitializeComponent();
        }

        public TabHeader(string title, Guid guid)
        {
            InitializeComponent();
            TitleLabel.Content = title;
            this.guid = guid;
        }
        
        public TabHeader(Uri imageSource, string title, Guid guid)
        {
            InitializeComponent();
            Image.Source = new BitmapImage(imageSource);
            TitleLabel.Content = title;
            this.guid = guid;
        }

        
        private void UserControl_OnMouseEnter(object sender, MouseEventArgs e)
        {
            BottomRect.Fill = Brushes.LightBlue;
        }

        private void UserControl_OnMouseLeave(object sender, MouseEventArgs e)
        {
            BottomRect.Fill = Brushes.Transparent;
        }

        private void CloseButton_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            
        }
    }
}