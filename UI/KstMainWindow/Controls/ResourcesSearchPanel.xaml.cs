﻿using System.Windows.Controls;
using System.Windows.Input;

namespace KotorStudio.UI.KstMainWindow.Controls
{
    public partial class ResourcesSearchPanel : UserControl
    {
        public ResourcesSearchPanel()
        {
            InitializeComponent();
            searchBox.MouseEnter += MouseEnter;
            searchBox.MouseLeave += MouseLeave;
            searchBox.KeyDown += OnKeyDownHandler;
        }


        private new void MouseEnter(object sender, MouseEventArgs e)
        {
            if (!searchBox.Text.Equals("")) searchBox.Text = "";
        }


        private new void MouseLeave(object sender, MouseEventArgs e)
        {
            if (!searchBox.Text.Equals("")) searchBox.Text = "Search Resources Here";
        }


        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return) searchBox.Text = "You Entered: " + searchBox.Text;
        }
    }
}