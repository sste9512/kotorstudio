﻿using System.Windows;
using System.Windows.Controls;
using KotorStudio.Controls.KstTreeView.ViewModel;
using KotorStudio.UI.KstErfEditor.EditorComponents.Controls;
using KotorStudio.UI.KstMainWindow.Commands;
using KotorStudio.UI.KstMainWindow.Controls;
using KotorStudio.UI.KstMainWindow.Interfaces;
using KotorStudio.UI.KstRIMEditor;
using KotorStudio.UI.KstText_Editor;

namespace KotorStudio.UI.KstMainWindow
{
    public partial class MainWindow : Window, IWindowReceiver
    {
       
        private WindowReceiver Receiver { get; }
        public MainWindow()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            SizeToContent = SizeToContent.Height;
            Receiver = new WindowReceiver();
            Receiver.SetWindowReceiver(this);
            PopulateTree();
        }


        public void OnReceive(object sender, int commandType)
        {
            if (sender is KstTreeViewItem node)
            {
                var name = node.ItemName;

                if (name.Contains(".ini"))
                {
                    var tabItem = new TabItem {Content = new KstTextEditor(node.FilePath), Header = new TabHeader()};
                    ContentPresentController.TabControl.Items.Add(tabItem);
                    ContentPresentController.TabControl.SelectedIndex = ContentPresentController.TabControl.Items.Count;
                    tabItem.IsSelected = true;
                }
                else if (name.Contains(".tlk"))
                {
                }
                else if (name.Contains(".dll"))
                {
                }
                else if (name.Contains(".2da"))
                {
                }
                else if (name.Contains(".key"))
                {
                }
                else if (name.Contains(".tga"))
                {
                }
                else if (name.Contains(".bif"))
                {
                }
                else if (name.Contains(".rim"))
                {
                    var tabItem = new TabItem {Content = new RimEditor(node.FilePath), Header = new TabHeader()};
                    ContentPresentController.TabControl.Items.Add(tabItem);
                    ContentPresentController.TabControl.SelectedIndex = ContentPresentController.TabControl.Items.Count;
                    tabItem.IsSelected = true;
                }
                else if (name.Contains(".erf"))
                {
                    var tabItem = new TabItem {Content = new ErfEditor(node.FilePath), Header = new TabHeader()};
                    ContentPresentController.TabControl.Items.Add(tabItem);
                    ContentPresentController.TabControl.SelectedIndex = ContentPresentController.TabControl.Items.Count;
                    tabItem.IsSelected = true;
                }
            }
        }
    }
}