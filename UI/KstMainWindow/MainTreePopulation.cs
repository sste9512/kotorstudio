﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Controls;
using KotorStudio.Controls.KstTreeView.ViewModel;

namespace KotorStudio.UI.KstMainWindow
{
    public partial class MainWindow
    {
        private void PopulateTree()
        {
            string[] directories = InitTopNode(@"E:\Steam\steamapps\common\Knights of the Old Republic II");

            foreach (string path in directories)
            {
                if (Directory.Exists(path))
                {
                    TreeViewItem item = new TreeViewItem();
                    item.Header = new KstTreeViewItem(path, Receiver);
                    FileTreeNavigation.KotorTreeView.Items.Add(item);
                    TraverseItem(item, InitTopNode(path));
                }
                else if (File.Exists(path))
                {
                    TreeViewItem item = new TreeViewItem();
                    item.Header = new KstTreeViewItem(path, Receiver);
                    FileTreeNavigation.KotorTreeView.Items.Add(item);
                }
                else
                {
                    Console.WriteLine(@"{0} is not a valid file or directory.", path);
                }
            }
        }

        private string[] InitTopNode(string root)
        {
            var array1 = Directory.GetFiles(root);
            var array2 = Directory.GetDirectories(root);
            var list = new List<string>();
            list.AddRange(array2);
            list.AddRange(array1);
            return list.ToArray();
        }

        private void TraverseItem(TreeViewItem item, string[] allPathsInRoot)
        {
            foreach (var path in allPathsInRoot)
                if (File.Exists(path))
                {
                    var newItem = new TreeViewItem();
                    newItem.Header = new KstTreeViewItem(path, Receiver);
                    item.Items.Add(newItem);
                }
                else if (Directory.Exists(path))
                {
                    var newItem = new TreeViewItem();
                    newItem.Header = new KstTreeViewItem(path, Receiver);
                    item.Items.Add(newItem);
                    TraverseItem(newItem, InitTopNode(path));
                }
                else
                {
                    Console.WriteLine(@"{0} is not a valid file or directory.", path);
                }
        }
    }
}