using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using KotorStudio.UI.KstMainWindow.Commands;

namespace KotorStudio.UI.KstMainWindow.Presenter
{
    public class MainWindowPresenter
    {
        public List<TabItem> TabRepo { get; set; }
        public WindowReceiver Receiver { get; set; }
        public Window Window { get; set; }

        public MainWindowPresenter(Window window)
        {
                     
        }
        
    }
}