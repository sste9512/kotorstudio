using KotorStudio.UI.KstMainWindow.Interfaces;

namespace KotorStudio.UI.KstMainWindow.Commands
{

    public abstract class WindowCommand
    {
        protected readonly WindowReceiver Receiver;

        protected WindowCommand(WindowReceiver receiver)
        {
            Receiver = receiver;
        }

        public abstract void Execute(object sender, int commandType);
    }

    public class ConcreteWindowCommand : WindowCommand
    {
        private IWindowCommand _windowCommand;
        
        public ConcreteWindowCommand(WindowReceiver receiver) : base(receiver)
        {
        }
        
        public override void Execute(object sender, int commandType)
        {
            Receiver.Action(sender, commandType);
        }
    }


}