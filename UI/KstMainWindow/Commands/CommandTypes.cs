namespace KotorStudio.UI.KstMainWindow.Commands
{
    public enum CommandTypes
    {
        TreeToTabCommand,
        RemoveAllTabsCommand
    }
}