using KotorStudio.UI.KstMainWindow.Interfaces;

namespace KotorStudio.UI.KstMainWindow.Commands
{
    public class WindowReceiver
    {
        private IWindowReceiver _receiver;

        public void SetWindowReceiver(IWindowReceiver receiver)
        {
            _receiver = receiver;
        }

        public void Action(object sender, int commandType)
        {
            _receiver?.OnReceive(sender, commandType);
        }
    }
}