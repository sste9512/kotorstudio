namespace KotorStudio.UI.KstMainWindow.Interfaces
{
    public interface IWindowReceiver
    {
        void OnReceive(object sender, int commandType);
    }
}