namespace KotorStudio.UI.KstMainWindow.Interfaces
{
    public interface IWindowCommand
    {
        void Action(object sender, int commandType);
    }
}