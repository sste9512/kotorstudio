using System.Windows.Controls;
using System.Windows.Media;
using KotorStudio.Models;
using KotorStudio.Models.AuroraParsers;
using KSTModels.Utils;

namespace KotorStudio.UI.KstRIMEditor
{
    public partial class RimEditor : UserControl
    {
        private string filePath { get; set; }
        private RimObject rimObject { get; set; }
                
        public RimEditor()
        {
            InitializeComponent();
             
        }
        
        public RimEditor(string filePath)
        {
            InitializeComponent();
            this.filePath = filePath;
            var auroraFile = new AuroraFile(filePath);
            rimObject = new RimObject(auroraFile);
            rimObject.Read();
            foreach (var rimkey in rimObject.GetRimKeyList())
            {
                var label = new Label();
                label.Content = "Resource Name: " + new string(rimkey.ResRef);
                label.FontSize = 14;
                label.Foreground = Brushes.White;
                RIMPanel.Children.Add(createNewLabel(ResourceHandler.GetRsrcTypeForId(rimkey.ResType)));
               
                
                RIMPanel.Children.Add(label);
            }
        }

        public Label createNewLabel(string key)
        {
            var label = new Label();
            label.Content = key;
            label.FontSize = 14;
            label.Foreground = Brushes.White;
            return label;
        }
        
    }
}
