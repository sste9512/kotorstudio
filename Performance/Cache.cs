using System.Collections.Generic;

namespace KotorStudio.Performance
{
    public class Cache
    {
        Dictionary<string, object> _centralCache = new Dictionary<string, object>();

        public void Add(string key, object Value)
        {
            if (!_centralCache.ContainsKey(key))
            {
                _centralCache.Add(key, Value);
            }
        }

        public object GetItem(string key)
        {
            if (_centralCache.ContainsKey(key))
            {
                return _centralCache[key];
            }

            return null;
        }


        static Cache cache = null;

        public static Cache GetInstance()
        {
            if (cache == null)
            {
                return new Cache();
            }

            return cache;
        }
    }
}