﻿using System.IO;
using System.Xml.Serialization;

namespace KSTUi.XMLHelpers
{
    public static class XmlUtils
    {
        public static string ToXml(object obj)
        {
            var stringwriter = new StringWriter();
            var serializer = new XmlSerializer(obj.GetType());
            serializer.Serialize(stringwriter, obj);
            return stringwriter.ToString();
        }

        /* public static object LoadFromXMLString(string xmlText)
        {
            var stringReader = new System.IO.StringReader(xmlText);
            var serializer = new XmlSerializer(typeof(YourClass));
            return serializer.Deserialize(stringReader) as YourClass;
        }
        */
    }
}