﻿#pragma checksum "..\..\..\..\..\KstErfEditor\EditorComponents\Controls\ErfEditor.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "EAEF5819DA7B60AD1E8FD67771E98B54FCF14695"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using KSTUi.KST_ErfEditor.EditorComponents.Controls;
using KSTUi.KstErfEditor.EditorComponents.Controls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace KSTUi.KstErfEditor.EditorComponents.Controls {
    
    
    /// <summary>
    /// ErfEditor
    /// </summary>
    public partial class ErfEditor : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 15 "..\..\..\..\..\KstErfEditor\EditorComponents\Controls\ErfEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal KSTUi.KST_ErfEditor.EditorComponents.Controls.SearchPanel SearchPanel;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\..\..\KstErfEditor\EditorComponents\Controls\ErfEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal KSTUi.KST_ErfEditor.EditorComponents.Controls.FilterPanel FilterPanel;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\..\..\KstErfEditor\EditorComponents\Controls\ErfEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal KSTUi.KstErfEditor.EditorComponents.Controls.ErfBuilder ErfBuilder;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\..\..\KstErfEditor\EditorComponents\Controls\ErfEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel ErfLayoutContainer;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\..\..\..\KstErfEditor\EditorComponents\Controls\ErfEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal KSTUi.KST_ErfEditor.EditorComponents.Controls.ErfListControl ErfListControl;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/KSTUi;component/ksterfeditor/editorcomponents/controls/erfeditor.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\KstErfEditor\EditorComponents\Controls\ErfEditor.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.SearchPanel = ((KSTUi.KST_ErfEditor.EditorComponents.Controls.SearchPanel)(target));
            return;
            case 2:
            this.FilterPanel = ((KSTUi.KST_ErfEditor.EditorComponents.Controls.FilterPanel)(target));
            return;
            case 3:
            this.ErfBuilder = ((KSTUi.KstErfEditor.EditorComponents.Controls.ErfBuilder)(target));
            return;
            case 4:
            this.ErfLayoutContainer = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 5:
            this.ErfListControl = ((KSTUi.KST_ErfEditor.EditorComponents.Controls.ErfListControl)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

