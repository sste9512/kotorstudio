﻿using System;
using System.IO;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using KotorStudio.UI.KstMainWindow.Commands;

namespace KotorStudio.Controls.KstTreeView.ViewModel
{
    public partial class KstTreeViewItem : UserControl
    {
        public string FilePath { get; set; }
        public string ItemName { get; set; }
        private ConcreteWindowCommand Command { get; set; }

        public KstTreeViewItem()
        {
            InitializeComponent();
        }

        public KstTreeViewItem(string path)
        {
            InitializeComponent();
            FilePath = path.Replace(@"\\", "/");
            ItemName = "";
            SetUpTreeViewItem();
        }

        public KstTreeViewItem(string path, WindowReceiver receiver)
        {
            InitializeComponent();
            FilePath = path.Replace(@"\\", "/");
            ItemName = "";
            SetUpTreeViewItem();
            Command = new ConcreteWindowCommand(receiver);
        }

        private void SetUpTreeViewItem()
        {
            SetupImageView();
            ItemName = new DirectoryInfo(FilePath).Name;
            TreeviewText.Content = ItemName;
            MouseEnter += KstTreeViewItemMouseEnter;
            MouseLeave += KstTreeViewItemMouseLeave;
            MouseDoubleClick += KstTreeViewItemDoubleClick;
        }

        private void SetupImageView()
        {
            if (File.GetAttributes(FilePath).HasFlag(FileAttributes.Directory))
            {
                TreeviewImage.Source = new BitmapImage(new Uri("pack://application:,,,/Resources/folder_icon.png"));
            }
            else
            {
                if (FilePath.Contains(".key"))
                {
                    TreeviewImage.Source = new BitmapImage(new Uri("pack://application:,,,/Resources/key_file.png"));
                }
                else if (FilePath.Contains(".zip"))
                {
                    TreeviewImage.Source = new BitmapImage(new Uri("pack://application:,,,/Resources/zip_file.png"));
                }
                else if (FilePath.Contains(".pdf"))
                {
                    TreeviewImage.Source = new BitmapImage(new Uri("pack://application:,,,/Resources/pdf_file.png"));
                }
                else if (FilePath.Contains(".rtf") || FilePath.Contains(".txt"))
                {
                    TreeviewImage.Source = new BitmapImage(new Uri("pack://application:,,,/Resources/rtf_file.png"));
                }
                else if (FilePath.Contains(".dll"))
                {
                    TreeviewImage.Source = new BitmapImage(new Uri("pack://application:,,,/Resources/dll_file.png"));
                }
                else if (FilePath.Contains(".tlk"))
                {
                    TreeviewImage.Source = new BitmapImage(new Uri("pack://application:,,,/Resources/tlk_file.png"));
                }
                else if (FilePath.Contains(".2da"))
                {
                }
                else if (FilePath.Contains(".tga"))
                {
                    TreeviewImage.Source = new BitmapImage(new Uri("pack://application:,,,/Resources/tga_file.png"));
                }
                else if (FilePath.Contains(".gui"))
                {
                    TreeviewImage.Source = new BitmapImage(new Uri("pack://application:,,,/Resources/gui_file.png"));
                }
                else if (FilePath.Contains(".MDL"))
                {
                    TreeviewImage.Source = new BitmapImage(new Uri("pack://application:,,,/Resources/MDL_file.png"));
                }
                else if (FilePath.Contains(".wav"))
                {
                    TreeviewImage.Source = new BitmapImage(new Uri("pack://application:,,,/Resources/wav_file.png"));
                }
                else if (FilePath.Contains(".bik"))
                {
                    TreeviewImage.Source = new BitmapImage(new Uri("pack://application:,,,/Resources/bik_file.png"));
                }
                else if (FilePath.Contains(".ini"))
                {
                    TreeviewImage.Source = new BitmapImage(new Uri("pack://application:,,,/Resources/ini_gears.png"));
                }
                else if (FilePath.Contains(".exe"))
                {
                    TreeviewImage.Source = new BitmapImage(new Uri("pack://application:,,,/Resources/exe_file.png"));
                }
            }
        }

        private void KstTreeViewItemMouseEnter(object sender, MouseEventArgs e)
        {
        }

        private void KstTreeViewItemMouseLeave(object sender, MouseEventArgs e)
        {
            Background = Brushes.Transparent;
        }

        private void KstTreeViewItemDoubleClick(object sender, MouseButtonEventArgs e)
        {
               Command.Execute(sender, 0);
        }
    }
}