﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using KotorStudio.Controls.KstTreeView.ViewModel;

namespace KotorStudio.Controls.KstTreeView.View
{
    public partial class KstTreeView : UserControl
    {
        private string _topDirectory;

        public void PopulateTree()
        {
            string[] directories = InitTopNode("D:/SteamGames/steamapps/common/Knights of the Old Republic II");
            Console.WriteLine("finished loading directories");

            foreach (string path in directories)
            {
                if (Directory.Exists(path))
                {
                    TreeViewItem item = new TreeViewItem();
                    item.Header = new KstTreeViewItem(path);

                    Treeviewdirectory.Items.Add(item);
                    TraverseItem(item, InitTopNode(path));
                }
                else if (File.Exists(path))
                {
                    TreeViewItem item = new TreeViewItem();
                    item.Header = new KstTreeViewItem(path);
                    Treeviewdirectory.Items.Add(item);
                }
                else
                {
                    Console.WriteLine(@"{0} is not a valid file or directory.", path);
                }
            }
        }

        public KstTreeView()
        {
            InitializeComponent();
           
        }

        private string[] InitTopNode(string root)
        {
            var array1 = Directory.GetFiles(root);
            var array2 = Directory.GetDirectories(root);
            var list = new List<string>();
            list.AddRange(array2);
            list.AddRange(array1);
            return list.ToArray();
        }


        private static async Task<string[]> StreamValues(string root)
        {
            return await Task<string[]>.Factory.StartNew(() =>
            {
                var array1 = Directory.GetFiles(root);
                var array2 = Directory.GetDirectories(root);
                var list = new List<string>();
                list.AddRange(array2);
                list.AddRange(array1);
                return list.ToArray();
            });
        }

        public List<TreeViewItem> TraverseTreeAndGetNodes(TreeView tree, string[] allPathsInRoot)
        {
            var items = new List<TreeViewItem>();
            foreach (var path in allPathsInRoot)
                if (Directory.Exists(path))
                {
                    var item = new TreeViewItem();
                    item.Header = new KstTreeViewItem(path);
                    // KST_TreeViewItem item = new KST_TreeViewItem(path);
                    items.Add(item);
                    TraverseItem(item, InitTopNode(path));
                }
                else if (File.Exists(path))
                {
                    var item = new TreeViewItem();
                    item.Header = new KstTreeViewItem(path);
                    items.Add(item);
                }
                else
                {
                    Console.WriteLine(@"{0} is not a valid file or directory.", path);
                }

            return items;
        }


        public void TraverseTree(TreeView tree, string[] allPathsInRoot)
        {
            foreach (var path in allPathsInRoot)
                if (Directory.Exists(path))
                {
                    var item = new TreeViewItem();
                    item.Header = new KstTreeViewItem(path);
                    tree.Items.Add(item);
                    TraverseItem(item, InitTopNode(path));
                }
                else if (File.Exists(path))
                {
                    var item = new TreeViewItem();
                    item.Header = new KstTreeViewItem(path);
                    tree.Items.Add(item);
                }
                else
                {
                    Console.WriteLine(@"{0} is not a valid file or directory.", path);
                }
        }


        private void TraverseItem(TreeViewItem item, string[] allPathsInRoot)
        {
            foreach (var path in allPathsInRoot)
                if (File.Exists(path))
                {
                    var newItem = new TreeViewItem();
                    newItem.Header = new KstTreeViewItem(path);
                    item.Items.Add(newItem);
                }
                else if (Directory.Exists(path))
                {
                    var newItem = new TreeViewItem();
                    newItem.Header = new KstTreeViewItem(path);
                    item.Items.Add(newItem);
                    TraverseItem(newItem, InitTopNode(path));
                }
                else
                {
                    Console.WriteLine(@"{0} is not a valid file or directory.", path);
                }
        }


        private void KST_TreeView_OnLoaded(object sender, RoutedEventArgs e)
        {
            var directories = StreamValues("D:/SteamGames/steamapps/common/Knights of the Old Republic II");


            foreach (var path in directories.Result)
                if (Directory.Exists(path))
                {
                    var item = new TreeViewItem();
                    item.Header = new KstTreeViewItem(path);

                    Treeviewdirectory.Items.Add(item);
                    TraverseItem(item, InitTopNode(path));
                }
                else if (File.Exists(path))
                {
                    var item = new TreeViewItem();
                    item.Header = new KstTreeViewItem(path);
                    Treeviewdirectory.Items.Add(item);
                }
                else
                {
                    Console.WriteLine(@"{0} is not a valid file or directory.", path);
                }
        }
    }
}