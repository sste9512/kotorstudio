using System.Data.Entity;

namespace KotorStudio.Persistence
{
    public interface IResourcePersistence<T>
    {
        void SaveAsJson(T t, string filePath);
        void SaveToDb(T t, DbContext dbContext);
    }
}