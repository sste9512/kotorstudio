using System.Data.Entity;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace KotorStudio.Persistence
{
    public class ResourcePersistence<T> : IResourcePersistence<T>
    {
       
        public void SaveAsJson(T t, string filePath)
        {
             File.WriteAllText(filePath,JsonConvert.SerializeObject(t));
        }

        public void SaveToDb(T t, DbContext dbContext)
        {
            
        }

        public async void SaveAsJsonAsync(T t, string filePath)
        {
            await Task.Factory.StartNew(() =>
            {
                 File.WriteAllText(filePath, JsonConvert.SerializeObject(t));
            });
        } 
        
        /*
        public void SaveToGoogleDrive(T t, GoogleDriveContext Context)
        {
            
        }
        */
    }
}