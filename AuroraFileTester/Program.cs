﻿using System;
using System.Text.RegularExpressions;
using KotorStudio.Models;
using KotorStudio.Models.AuroraParsers;
using KSTModels.Utils;

namespace AuroraFileTester
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            /*
            var erfFilePath = @"E:\Steam\steamapps\common\Knights of the Old Republic II\TexturePacks\swpc_tex_tpc.erf";
            var erf = new ErfObject(erfFilePath);
         
            Console.WriteLine(erf._header.EntryCount);
            Console.WriteLine(erf._header.OffsetToKeyList);
            Console.WriteLine(erf._header.OffsetToLocalizedString);
            Console.WriteLine(erf._header.OffsetToResourceList);
          
            foreach (var res in erf.GetKeys())
            {
                Console.WriteLine(res.ResRef);
                Console.WriteLine(res.ResId);
                Console.WriteLine(res.ResType);
                Console.WriteLine("---------------------");
             
            }

            foreach (var res in erf.GetResources())
            {
                Console.WriteLine(res.OffsetToResource);
                Console.WriteLine(res.ResourceSize);
                Console.WriteLine("---------------------");
            }
            //Write All the tpcs from the Erf to the specified file
            for (int i = 0; i < erf.GetKeys().Count; i++)
            {
                erf.WriteToFile(@"C:\TPCS\" + CleanInput(new string(erf.GetKeys()[i].ResRef)), erf.GetRawResource(erf.GetResources()[i]));
            }
          */


            var filePath = @"E:\Steam\steamapps\common\Knights of the Old Republic II\data\Textures.bif";
            var aurorafile = new AuroraFile(filePath);
            var biff = new BiffObject(aurorafile);
            biff.Read();
            foreach (var res in biff.GetResources())
            {
                Console.WriteLine(res.Id + " Id");
                Console.WriteLine(res.Offset + " Offset");
                Console.WriteLine(res.FileSize + " FileSize");
                Console.WriteLine(ResourceHandler.GetRsrcTypeForId((int) res.ResourceType) + " ResourceType");
                Console.WriteLine("-----------------------");
            }
        }


        static string CleanInput(string strIn)
        {
            try
            {
                return Regex.Replace(strIn, @"[^\w\.@-]", "", RegexOptions.None, TimeSpan.FromSeconds(1.5));
            }
            catch (RegexMatchTimeoutException)
            {
                return String.Empty;
            }
        }
    }
}