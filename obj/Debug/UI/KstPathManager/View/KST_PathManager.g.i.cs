﻿#pragma checksum "..\..\..\..\..\UI\KstPathManager\View\KST_PathManager.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "62869CEBDF5608D7C915CEDB165B5875F88836A9"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace KotorStudio.UI.KstPathManager.View {
    
    
    /// <summary>
    /// KstPathManager
    /// </summary>
    public partial class KstPathManager : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 31 "..\..\..\..\..\UI\KstPathManager\View\KST_PathManager.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DockPanel RootWindow;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\..\..\UI\KstPathManager\View\KST_PathManager.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DockPanel TitleBar;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\..\..\UI\KstPathManager\View\KST_PathManager.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CloseButton;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\..\..\UI\KstPathManager\View\KST_PathManager.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button MaxButton;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\..\..\..\UI\KstPathManager\View\KST_PathManager.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button MinButton;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\..\..\UI\KstPathManager\View\KST_PathManager.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label erfWindowtitle;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\..\..\..\UI\KstPathManager\View\KST_PathManager.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label kotorPathLabel;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\..\..\..\UI\KstPathManager\View\KST_PathManager.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label kotorTSLPathLabel;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/KotorStudio;component/ui/kstpathmanager/view/kst_pathmanager.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\UI\KstPathManager\View\KST_PathManager.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.RootWindow = ((System.Windows.Controls.DockPanel)(target));
            
            #line 34 "..\..\..\..\..\UI\KstPathManager\View\KST_PathManager.xaml"
            this.RootWindow.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.RootWindow_OnMouseDown);
            
            #line default
            #line hidden
            return;
            case 2:
            this.TitleBar = ((System.Windows.Controls.DockPanel)(target));
            return;
            case 3:
            this.CloseButton = ((System.Windows.Controls.Button)(target));
            
            #line 43 "..\..\..\..\..\UI\KstPathManager\View\KST_PathManager.xaml"
            this.CloseButton.Click += new System.Windows.RoutedEventHandler(this.CloseButton_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.MaxButton = ((System.Windows.Controls.Button)(target));
            return;
            case 5:
            this.MinButton = ((System.Windows.Controls.Button)(target));
            return;
            case 6:
            this.erfWindowtitle = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.kotorPathLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            
            #line 84 "..\..\..\..\..\UI\KstPathManager\View\KST_PathManager.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.FindKotorPath);
            
            #line default
            #line hidden
            return;
            case 9:
            this.kotorTSLPathLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            
            #line 97 "..\..\..\..\..\UI\KstPathManager\View\KST_PathManager.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.FindKotor2Path);
            
            #line default
            #line hidden
            return;
            case 11:
            
            #line 105 "..\..\..\..\..\UI\KstPathManager\View\KST_PathManager.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Save);
            
            #line default
            #line hidden
            return;
            case 12:
            
            #line 152 "..\..\..\..\..\UI\KstPathManager\View\KST_PathManager.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Cancel);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

