﻿#pragma checksum "..\..\..\..\..\ERF_Editor\EditorComponents\Controls\TPCPreviewPanel.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "247C547AD9AB1D95617A46C062484836459908C1"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.MahApps;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace KotorStudio.ERF_Editor.EditorComponents.Controls {
    
    
    /// <summary>
    /// TpcPreviewPanel
    /// </summary>
    public partial class TpcPreviewPanel : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 31 "..\..\..\..\..\ERF_Editor\EditorComponents\Controls\TPCPreviewPanel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label MemoryLabel;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\..\..\ERF_Editor\EditorComponents\Controls\TPCPreviewPanel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label FlagLabel;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\..\..\ERF_Editor\EditorComponents\Controls\TPCPreviewPanel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label CubeLabel;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\..\..\ERF_Editor\EditorComponents\Controls\TPCPreviewPanel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label OffsetLabel;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\..\..\ERF_Editor\EditorComponents\Controls\TPCPreviewPanel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label NameLabel;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\..\..\ERF_Editor\EditorComponents\Controls\TPCPreviewPanel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label SomethingLabel;
        
        #line default
        #line hidden
        
        
        #line 200 "..\..\..\..\..\ERF_Editor\EditorComponents\Controls\TPCPreviewPanel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image TgaViewer;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/KotorStudio;component/erf_editor/editorcomponents/controls/tpcpreviewpanel.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\ERF_Editor\EditorComponents\Controls\TPCPreviewPanel.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.MemoryLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.FlagLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.CubeLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.OffsetLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.NameLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.SomethingLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.TgaViewer = ((System.Windows.Controls.Image)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

